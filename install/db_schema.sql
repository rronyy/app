/*
SQLyog Community v12.4.2 (32 bit)
MySQL - 5.6.17 : Database - logistics
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `_icons` */

DROP TABLE IF EXISTS `_icons`;

CREATE TABLE `_icons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(64) NOT NULL,
  `icon` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=767 DEFAULT CHARSET=latin1;

/*Data for the table `_icons` */

LOCK TABLES `_icons` WRITE;

insert  into `_icons`(`id`,`theme`,`icon`) values 
(1,'sbadmin2','fa fa-glass'),
(2,'sbadmin2','fa fa-music'),
(3,'sbadmin2','fa fa-search'),
(4,'sbadmin2','fa fa-envelope-o'),
(5,'sbadmin2','fa fa-heart'),
(6,'sbadmin2','fa fa-star'),
(7,'sbadmin2','fa fa-star-o'),
(8,'sbadmin2','fa fa-user'),
(9,'sbadmin2','fa fa-film'),
(10,'sbadmin2','fa fa-th-large'),
(11,'sbadmin2','fa fa-th'),
(12,'sbadmin2','fa fa-th-list'),
(13,'sbadmin2','fa fa-check'),
(14,'sbadmin2','fa fa-times'),
(15,'sbadmin2','fa fa-search-plus'),
(16,'sbadmin2','fa fa-search-minus'),
(17,'sbadmin2','fa fa-power-off'),
(18,'sbadmin2','fa fa-signal'),
(19,'sbadmin2','fa fa-gear'),
(20,'sbadmin2','fa fa-cog'),
(21,'sbadmin2','fa fa-trash-o'),
(22,'sbadmin2','fa fa-home'),
(23,'sbadmin2','fa fa-file-o'),
(24,'sbadmin2','fa fa-clock-o'),
(25,'sbadmin2','fa fa-road'),
(26,'sbadmin2','fa fa-download'),
(27,'sbadmin2','fa fa-arrow-circle-o-down'),
(28,'sbadmin2','fa fa-arrow-circle-o-up'),
(29,'sbadmin2','fa fa-inbox'),
(30,'sbadmin2','fa fa-play-circle-o'),
(31,'sbadmin2','fa fa-rotate-right'),
(32,'sbadmin2','fa fa-repeat'),
(33,'sbadmin2','fa fa-refresh'),
(34,'sbadmin2','fa fa-list-alt'),
(35,'sbadmin2','fa fa-lock'),
(36,'sbadmin2','fa fa-flag'),
(37,'sbadmin2','fa fa-headphones'),
(38,'sbadmin2','fa fa-volume-off'),
(39,'sbadmin2','fa fa-volume-down'),
(40,'sbadmin2','fa fa-volume-up'),
(41,'sbadmin2','fa fa-qrcode'),
(42,'sbadmin2','fa fa-barcode'),
(43,'sbadmin2','fa fa-tag'),
(44,'sbadmin2','fa fa-tags'),
(45,'sbadmin2','fa fa-book'),
(46,'sbadmin2','fa fa-bookmark'),
(47,'sbadmin2','fa fa-print'),
(48,'sbadmin2','fa fa-camera'),
(49,'sbadmin2','fa fa-font'),
(50,'sbadmin2','fa fa-bold'),
(51,'sbadmin2','fa fa-italic'),
(52,'sbadmin2','fa fa-text-height'),
(53,'sbadmin2','fa fa-text-width'),
(54,'sbadmin2','fa fa-align-left'),
(55,'sbadmin2','fa fa-align-center'),
(56,'sbadmin2','fa fa-align-right'),
(57,'sbadmin2','fa fa-align-justify'),
(58,'sbadmin2','fa fa-list'),
(59,'sbadmin2','fa fa-dedent'),
(60,'sbadmin2','fa fa-outdent'),
(61,'sbadmin2','fa fa-indent'),
(62,'sbadmin2','fa fa-video-camera'),
(63,'sbadmin2','fa fa-photo'),
(64,'sbadmin2','fa fa-image'),
(65,'sbadmin2','fa fa-picture-o'),
(66,'sbadmin2','fa fa-pencil'),
(67,'sbadmin2','fa fa-map-marker'),
(68,'sbadmin2','fa fa-adjust'),
(69,'sbadmin2','fa fa-tint'),
(70,'sbadmin2','fa fa-edit'),
(71,'sbadmin2','fa fa-pencil-square-o'),
(72,'sbadmin2','fa fa-share-square-o'),
(73,'sbadmin2','fa fa-check-square-o'),
(74,'sbadmin2','fa fa-arrows'),
(75,'sbadmin2','fa fa-step-backward'),
(76,'sbadmin2','fa fa-fast-backward'),
(77,'sbadmin2','fa fa-backward'),
(78,'sbadmin2','fa fa-play'),
(79,'sbadmin2','fa fa-pause'),
(80,'sbadmin2','fa fa-stop'),
(81,'sbadmin2','fa fa-forward'),
(82,'sbadmin2','fa fa-fast-forward'),
(83,'sbadmin2','fa fa-step-forward'),
(84,'sbadmin2','fa fa-eject'),
(85,'sbadmin2','fa fa-chevron-left'),
(86,'sbadmin2','fa fa-chevron-right'),
(87,'sbadmin2','fa fa-plus-circle'),
(88,'sbadmin2','fa fa-minus-circle'),
(89,'sbadmin2','fa fa-times-circle'),
(90,'sbadmin2','fa fa-check-circle'),
(91,'sbadmin2','fa fa-question-circle'),
(92,'sbadmin2','fa fa-info-circle'),
(93,'sbadmin2','fa fa-crosshairs'),
(94,'sbadmin2','fa fa-times-circle-o'),
(95,'sbadmin2','fa fa-check-circle-o'),
(96,'sbadmin2','fa fa-ban'),
(97,'sbadmin2','fa fa-arrow-left'),
(98,'sbadmin2','fa fa-arrow-right'),
(99,'sbadmin2','fa fa-arrow-up'),
(100,'sbadmin2','fa fa-arrow-down'),
(101,'sbadmin2','fa fa-mail-forward'),
(102,'sbadmin2','fa fa-share'),
(103,'sbadmin2','fa fa-expand'),
(104,'sbadmin2','fa fa-compress'),
(105,'sbadmin2','fa fa-plus'),
(106,'sbadmin2','fa fa-minus'),
(107,'sbadmin2','fa fa-asterisk'),
(108,'sbadmin2','fa fa-exclamation-circle'),
(109,'sbadmin2','fa fa-gift'),
(110,'sbadmin2','fa fa-leaf'),
(111,'sbadmin2','fa fa-fire'),
(112,'sbadmin2','fa fa-eye'),
(113,'sbadmin2','fa fa-eye-slash'),
(114,'sbadmin2','fa fa-warning'),
(115,'sbadmin2','fa fa-exclamation-triangle'),
(116,'sbadmin2','fa fa-plane'),
(117,'sbadmin2','fa fa-calendar'),
(118,'sbadmin2','fa fa-random'),
(119,'sbadmin2','fa fa-comment'),
(120,'sbadmin2','fa fa-magnet'),
(121,'sbadmin2','fa fa-chevron-up'),
(122,'sbadmin2','fa fa-chevron-down'),
(123,'sbadmin2','fa fa-retweet'),
(124,'sbadmin2','fa fa-shopping-cart'),
(125,'sbadmin2','fa fa-folder'),
(126,'sbadmin2','fa fa-folder-open'),
(127,'sbadmin2','fa fa-arrows-v'),
(128,'sbadmin2','fa fa-arrows-h'),
(129,'sbadmin2','fa fa-bar-chart-o'),
(130,'sbadmin2','fa fa-twitter-square'),
(131,'sbadmin2','fa fa-facebook-square'),
(132,'sbadmin2','fa fa-camera-retro'),
(133,'sbadmin2','fa fa-key'),
(134,'sbadmin2','fa fa-gears'),
(135,'sbadmin2','fa fa-cogs'),
(136,'sbadmin2','fa fa-comments'),
(137,'sbadmin2','fa fa-thumbs-o-up'),
(138,'sbadmin2','fa fa-thumbs-o-down'),
(139,'sbadmin2','fa fa-star-half'),
(140,'sbadmin2','fa fa-heart-o'),
(141,'sbadmin2','fa fa-sign-out'),
(142,'sbadmin2','fa fa-linkedin-square'),
(143,'sbadmin2','fa fa-thumb-tack'),
(144,'sbadmin2','fa fa-external-link'),
(145,'sbadmin2','fa fa-sign-in'),
(146,'sbadmin2','fa fa-trophy'),
(147,'sbadmin2','fa fa-github-square'),
(148,'sbadmin2','fa fa-upload'),
(149,'sbadmin2','fa fa-lemon-o'),
(150,'sbadmin2','fa fa-phone'),
(151,'sbadmin2','fa fa-square-o'),
(152,'sbadmin2','fa fa-bookmark-o'),
(153,'sbadmin2','fa fa-phone-square'),
(154,'sbadmin2','fa fa-twitter'),
(155,'sbadmin2','fa fa-facebook'),
(156,'sbadmin2','fa fa-github'),
(157,'sbadmin2','fa fa-unlock'),
(158,'sbadmin2','fa fa-credit-card'),
(159,'sbadmin2','fa fa-rss'),
(160,'sbadmin2','fa fa-hdd-o'),
(161,'sbadmin2','fa fa-bullhorn'),
(162,'sbadmin2','fa fa-bell'),
(163,'sbadmin2','fa fa-certificate'),
(164,'sbadmin2','fa fa-hand-o-right'),
(165,'sbadmin2','fa fa-hand-o-left'),
(166,'sbadmin2','fa fa-hand-o-up'),
(167,'sbadmin2','fa fa-hand-o-down'),
(168,'sbadmin2','fa fa-arrow-circle-left'),
(169,'sbadmin2','fa fa-arrow-circle-right'),
(170,'sbadmin2','fa fa-arrow-circle-up'),
(171,'sbadmin2','fa fa-arrow-circle-down'),
(172,'sbadmin2','fa fa-globe'),
(173,'sbadmin2','fa fa-wrench'),
(174,'sbadmin2','fa fa-tasks'),
(175,'sbadmin2','fa fa-filter'),
(176,'sbadmin2','fa fa-briefcase'),
(177,'sbadmin2','fa fa-arrows-alt'),
(178,'sbadmin2','fa fa-group'),
(179,'sbadmin2','fa fa-users'),
(180,'sbadmin2','fa fa-chain'),
(181,'sbadmin2','fa fa-link'),
(182,'sbadmin2','fa fa-cloud'),
(183,'sbadmin2','fa fa-flask'),
(184,'sbadmin2','fa fa-cut'),
(185,'sbadmin2','fa fa-scissors'),
(186,'sbadmin2','fa fa-copy'),
(187,'sbadmin2','fa fa-files-o'),
(188,'sbadmin2','fa fa-paperclip'),
(189,'sbadmin2','fa fa-save'),
(190,'sbadmin2','fa fa-floppy-o'),
(191,'sbadmin2','fa fa-square'),
(192,'sbadmin2','fa fa-navicon'),
(193,'sbadmin2','fa fa-reorder'),
(194,'sbadmin2','fa fa-bars'),
(195,'sbadmin2','fa fa-list-ul'),
(196,'sbadmin2','fa fa-list-ol'),
(197,'sbadmin2','fa fa-strikethrough'),
(198,'sbadmin2','fa fa-underline'),
(199,'sbadmin2','fa fa-table'),
(200,'sbadmin2','fa fa-magic'),
(201,'sbadmin2','fa fa-truck'),
(202,'sbadmin2','fa fa-pinterest'),
(203,'sbadmin2','fa fa-pinterest-square'),
(204,'sbadmin2','fa fa-google-plus-square'),
(205,'sbadmin2','fa fa-google-plus'),
(206,'sbadmin2','fa fa-money'),
(207,'sbadmin2','fa fa-caret-down'),
(208,'sbadmin2','fa fa-caret-up'),
(209,'sbadmin2','fa fa-caret-left'),
(210,'sbadmin2','fa fa-caret-right'),
(211,'sbadmin2','fa fa-columns'),
(212,'sbadmin2','fa fa-unsorted'),
(213,'sbadmin2','fa fa-sort'),
(214,'sbadmin2','fa fa-sort-down'),
(215,'sbadmin2','fa fa-sort-desc'),
(216,'sbadmin2','fa fa-sort-up'),
(217,'sbadmin2','fa fa-sort-asc'),
(218,'sbadmin2','fa fa-envelope'),
(219,'sbadmin2','fa fa-linkedin'),
(220,'sbadmin2','fa fa-rotate-left'),
(221,'sbadmin2','fa fa-undo'),
(222,'sbadmin2','fa fa-legal'),
(223,'sbadmin2','fa fa-gavel'),
(224,'sbadmin2','fa fa-dashboard'),
(225,'sbadmin2','fa fa-tachometer'),
(226,'sbadmin2','fa fa-comment-o'),
(227,'sbadmin2','fa fa-comments-o'),
(228,'sbadmin2','fa fa-flash'),
(229,'sbadmin2','fa fa-bolt'),
(230,'sbadmin2','fa fa-sitemap'),
(231,'sbadmin2','fa fa-umbrella'),
(232,'sbadmin2','fa fa-paste'),
(233,'sbadmin2','fa fa-clipboard'),
(234,'sbadmin2','fa fa-lightbulb-o'),
(235,'sbadmin2','fa fa-exchange'),
(236,'sbadmin2','fa fa-cloud-download'),
(237,'sbadmin2','fa fa-cloud-upload'),
(238,'sbadmin2','fa fa-user-md'),
(239,'sbadmin2','fa fa-stethoscope'),
(240,'sbadmin2','fa fa-suitcase'),
(241,'sbadmin2','fa fa-bell-o'),
(242,'sbadmin2','fa fa-coffee'),
(243,'sbadmin2','fa fa-cutlery'),
(244,'sbadmin2','fa fa-file-text-o'),
(245,'sbadmin2','fa fa-building-o'),
(246,'sbadmin2','fa fa-hospital-o'),
(247,'sbadmin2','fa fa-ambulance'),
(248,'sbadmin2','fa fa-medkit'),
(249,'sbadmin2','fa fa-fighter-jet'),
(250,'sbadmin2','fa fa-beer'),
(251,'sbadmin2','fa fa-h-square'),
(252,'sbadmin2','fa fa-plus-square'),
(253,'sbadmin2','fa fa-angle-double-left'),
(254,'sbadmin2','fa fa-angle-double-right'),
(255,'sbadmin2','fa fa-angle-double-up'),
(256,'sbadmin2','fa fa-angle-double-down'),
(257,'sbadmin2','fa fa-angle-left'),
(258,'sbadmin2','fa fa-angle-right'),
(259,'sbadmin2','fa fa-angle-up'),
(260,'sbadmin2','fa fa-angle-down'),
(261,'sbadmin2','fa fa-desktop'),
(262,'sbadmin2','fa fa-laptop'),
(263,'sbadmin2','fa fa-tablet'),
(264,'sbadmin2','fa fa-mobile-phone'),
(265,'sbadmin2','fa fa-mobile'),
(266,'sbadmin2','fa fa-circle-o'),
(267,'sbadmin2','fa fa-quote-left'),
(268,'sbadmin2','fa fa-quote-right'),
(269,'sbadmin2','fa fa-spinner'),
(270,'sbadmin2','fa fa-circle'),
(271,'sbadmin2','fa fa-mail-reply'),
(272,'sbadmin2','fa fa-reply'),
(273,'sbadmin2','fa fa-github-alt'),
(274,'sbadmin2','fa fa-folder-o'),
(275,'sbadmin2','fa fa-folder-open-o'),
(276,'sbadmin2','fa fa-smile-o'),
(277,'sbadmin2','fa fa-frown-o'),
(278,'sbadmin2','fa fa-meh-o'),
(279,'sbadmin2','fa fa-gamepad'),
(280,'sbadmin2','fa fa-keyboard-o'),
(281,'sbadmin2','fa fa-flag-o'),
(282,'sbadmin2','fa fa-flag-checkered'),
(283,'sbadmin2','fa fa-terminal'),
(284,'sbadmin2','fa fa-code'),
(285,'sbadmin2','fa fa-mail-reply-all'),
(286,'sbadmin2','fa fa-reply-all'),
(287,'sbadmin2','fa fa-star-half-empty'),
(288,'sbadmin2','fa fa-star-half-full'),
(289,'sbadmin2','fa fa-star-half-o'),
(290,'sbadmin2','fa fa-location-arrow'),
(291,'sbadmin2','fa fa-crop'),
(292,'sbadmin2','fa fa-code-fork'),
(293,'sbadmin2','fa fa-unlink'),
(294,'sbadmin2','fa fa-chain-broken'),
(295,'sbadmin2','fa fa-question'),
(296,'sbadmin2','fa fa-info'),
(297,'sbadmin2','fa fa-exclamation'),
(298,'sbadmin2','fa fa-superscript'),
(299,'sbadmin2','fa fa-subscript'),
(300,'sbadmin2','fa fa-eraser'),
(301,'sbadmin2','fa fa-puzzle-piece'),
(302,'sbadmin2','fa fa-microphone'),
(303,'sbadmin2','fa fa-microphone-slash'),
(304,'sbadmin2','fa fa-shield'),
(305,'sbadmin2','fa fa-calendar-o'),
(306,'sbadmin2','fa fa-fire-extinguisher'),
(307,'sbadmin2','fa fa-rocket'),
(308,'sbadmin2','fa fa-maxcdn'),
(309,'sbadmin2','fa fa-chevron-circle-left'),
(310,'sbadmin2','fa fa-chevron-circle-right'),
(311,'sbadmin2','fa fa-chevron-circle-up'),
(312,'sbadmin2','fa fa-chevron-circle-down'),
(313,'sbadmin2','fa fa-html5'),
(314,'sbadmin2','fa fa-css3'),
(315,'sbadmin2','fa fa-anchor'),
(316,'sbadmin2','fa fa-unlock-alt'),
(317,'sbadmin2','fa fa-bullseye'),
(318,'sbadmin2','fa fa-ellipsis-h'),
(319,'sbadmin2','fa fa-ellipsis-v'),
(320,'sbadmin2','fa fa-rss-square'),
(321,'sbadmin2','fa fa-play-circle'),
(322,'sbadmin2','fa fa-ticket'),
(323,'sbadmin2','fa fa-minus-square'),
(324,'sbadmin2','fa fa-minus-square-o'),
(325,'sbadmin2','fa fa-level-up'),
(326,'sbadmin2','fa fa-level-down'),
(327,'sbadmin2','fa fa-check-square'),
(328,'sbadmin2','fa fa-pencil-square'),
(329,'sbadmin2','fa fa-external-link-square'),
(330,'sbadmin2','fa fa-share-square'),
(331,'sbadmin2','fa fa-compass'),
(332,'sbadmin2','fa fa-toggle-down'),
(333,'sbadmin2','fa fa-caret-square-o-down'),
(334,'sbadmin2','fa fa-toggle-up'),
(335,'sbadmin2','fa fa-caret-square-o-up'),
(336,'sbadmin2','fa fa-toggle-right'),
(337,'sbadmin2','fa fa-caret-square-o-right'),
(338,'sbadmin2','fa fa-euro'),
(339,'sbadmin2','fa fa-eur'),
(340,'sbadmin2','fa fa-gbp'),
(341,'sbadmin2','fa fa-dollar'),
(342,'sbadmin2','fa fa-usd'),
(343,'sbadmin2','fa fa-rupee'),
(344,'sbadmin2','fa fa-inr'),
(345,'sbadmin2','fa fa-cny'),
(346,'sbadmin2','fa fa-rmb'),
(347,'sbadmin2','fa fa-yen'),
(348,'sbadmin2','fa fa-jpy'),
(349,'sbadmin2','fa fa-ruble'),
(350,'sbadmin2','fa fa-rouble'),
(351,'sbadmin2','fa fa-rub'),
(352,'sbadmin2','fa fa-won'),
(353,'sbadmin2','fa fa-krw'),
(354,'sbadmin2','fa fa-bitcoin'),
(355,'sbadmin2','fa fa-btc'),
(356,'sbadmin2','fa fa-file'),
(357,'sbadmin2','fa fa-file-text'),
(358,'sbadmin2','fa fa-sort-alpha-asc'),
(359,'sbadmin2','fa fa-sort-alpha-desc'),
(360,'sbadmin2','fa fa-sort-amount-asc'),
(361,'sbadmin2','fa fa-sort-amount-desc'),
(362,'sbadmin2','fa fa-sort-numeric-asc'),
(363,'sbadmin2','fa fa-sort-numeric-desc'),
(364,'sbadmin2','fa fa-thumbs-up'),
(365,'sbadmin2','fa fa-thumbs-down'),
(366,'sbadmin2','fa fa-youtube-square'),
(367,'sbadmin2','fa fa-youtube'),
(368,'sbadmin2','fa fa-xing'),
(369,'sbadmin2','fa fa-xing-square'),
(370,'sbadmin2','fa fa-youtube-play'),
(371,'sbadmin2','fa fa-dropbox'),
(372,'sbadmin2','fa fa-stack-overflow'),
(373,'sbadmin2','fa fa-instagram'),
(374,'sbadmin2','fa fa-flickr'),
(375,'sbadmin2','fa fa-adn'),
(376,'sbadmin2','fa fa-bitbucket'),
(377,'sbadmin2','fa fa-bitbucket-square'),
(378,'sbadmin2','fa fa-tumblr'),
(379,'sbadmin2','fa fa-tumblr-square'),
(380,'sbadmin2','fa fa-long-arrow-down'),
(381,'sbadmin2','fa fa-long-arrow-up'),
(382,'sbadmin2','fa fa-long-arrow-left'),
(383,'sbadmin2','fa fa-long-arrow-right'),
(384,'sbadmin2','fa fa-apple'),
(385,'sbadmin2','fa fa-windows'),
(386,'sbadmin2','fa fa-android'),
(387,'sbadmin2','fa fa-linux'),
(388,'sbadmin2','fa fa-dribbble'),
(389,'sbadmin2','fa fa-skype'),
(390,'sbadmin2','fa fa-foursquare'),
(391,'sbadmin2','fa fa-trello'),
(392,'sbadmin2','fa fa-female'),
(393,'sbadmin2','fa fa-male'),
(394,'sbadmin2','fa fa-gittip'),
(395,'sbadmin2','fa fa-sun-o'),
(396,'sbadmin2','fa fa-moon-o'),
(397,'sbadmin2','fa fa-archive'),
(398,'sbadmin2','fa fa-bug'),
(399,'sbadmin2','fa fa-vk'),
(400,'sbadmin2','fa fa-weibo'),
(401,'sbadmin2','fa fa-renren'),
(402,'sbadmin2','fa fa-pagelines'),
(403,'sbadmin2','fa fa-stack-exchange'),
(404,'sbadmin2','fa fa-arrow-circle-o-right'),
(405,'sbadmin2','fa fa-arrow-circle-o-left'),
(406,'sbadmin2','fa fa-toggle-left'),
(407,'sbadmin2','fa fa-caret-square-o-left'),
(408,'sbadmin2','fa fa-dot-circle-o'),
(409,'sbadmin2','fa fa-wheelchair'),
(410,'sbadmin2','fa fa-vimeo-square'),
(411,'sbadmin2','fa fa-turkish-lira'),
(412,'sbadmin2','fa fa-try'),
(413,'sbadmin2','fa fa-plus-square-o'),
(414,'sbadmin2','fa fa-space-shuttle'),
(415,'sbadmin2','fa fa-slack'),
(416,'sbadmin2','fa fa-envelope-square'),
(417,'sbadmin2','fa fa-wordpress'),
(418,'sbadmin2','fa fa-openid'),
(419,'sbadmin2','fa fa-institution'),
(420,'sbadmin2','fa fa-bank'),
(421,'sbadmin2','fa fa-university'),
(422,'sbadmin2','fa fa-mortar-board'),
(423,'sbadmin2','fa fa-graduation-cap'),
(424,'sbadmin2','fa fa-yahoo'),
(425,'sbadmin2','fa fa-google'),
(426,'sbadmin2','fa fa-reddit'),
(427,'sbadmin2','fa fa-reddit-square'),
(428,'sbadmin2','fa fa-stumbleupon-circle'),
(429,'sbadmin2','fa fa-stumbleupon'),
(430,'sbadmin2','fa fa-delicious'),
(431,'sbadmin2','fa fa-digg'),
(432,'sbadmin2','fa fa-pied-piper-square'),
(433,'sbadmin2','fa fa-pied-piper'),
(434,'sbadmin2','fa fa-pied-piper-alt'),
(435,'sbadmin2','fa fa-drupal'),
(436,'sbadmin2','fa fa-joomla'),
(437,'sbadmin2','fa fa-language'),
(438,'sbadmin2','fa fa-fax'),
(439,'sbadmin2','fa fa-building'),
(440,'sbadmin2','fa fa-child'),
(441,'sbadmin2','fa fa-paw'),
(442,'sbadmin2','fa fa-spoon'),
(443,'sbadmin2','fa fa-cube'),
(444,'sbadmin2','fa fa-cubes'),
(445,'sbadmin2','fa fa-behance'),
(446,'sbadmin2','fa fa-behance-square'),
(447,'sbadmin2','fa fa-steam'),
(448,'sbadmin2','fa fa-steam-square'),
(449,'sbadmin2','fa fa-recycle'),
(450,'sbadmin2','fa fa-automobile'),
(451,'sbadmin2','fa fa-car'),
(452,'sbadmin2','fa fa-cab'),
(453,'sbadmin2','fa fa-taxi'),
(454,'sbadmin2','fa fa-tree'),
(455,'sbadmin2','fa fa-spotify'),
(456,'sbadmin2','fa fa-deviantart'),
(457,'sbadmin2','fa fa-soundcloud'),
(458,'sbadmin2','fa fa-database'),
(459,'sbadmin2','fa fa-file-pdf-o'),
(460,'sbadmin2','fa fa-file-word-o'),
(461,'sbadmin2','fa fa-file-excel-o'),
(462,'sbadmin2','fa fa-file-powerpoint-o'),
(463,'sbadmin2','fa fa-file-photo-o'),
(464,'sbadmin2','fa fa-file-picture-o'),
(465,'sbadmin2','fa fa-file-image-o'),
(466,'sbadmin2','fa fa-file-zip-o'),
(467,'sbadmin2','fa fa-file-archive-o'),
(468,'sbadmin2','fa fa-file-sound-o'),
(469,'sbadmin2','fa fa-file-audio-o'),
(470,'sbadmin2','fa fa-file-movie-o'),
(471,'sbadmin2','fa fa-file-video-o'),
(472,'sbadmin2','fa fa-file-code-o'),
(473,'sbadmin2','fa fa-vine'),
(474,'sbadmin2','fa fa-codepen'),
(475,'sbadmin2','fa fa-jsfiddle'),
(476,'sbadmin2','fa fa-life-bouy'),
(477,'sbadmin2','fa fa-life-saver'),
(478,'sbadmin2','fa fa-support'),
(479,'sbadmin2','fa fa-life-ring'),
(480,'sbadmin2','fa fa-circle-o-notch'),
(481,'sbadmin2','fa fa-ra'),
(482,'sbadmin2','fa fa-rebel'),
(483,'sbadmin2','fa fa-ge'),
(484,'sbadmin2','fa fa-empire'),
(485,'sbadmin2','fa fa-git-square'),
(486,'sbadmin2','fa fa-git'),
(487,'sbadmin2','fa fa-hacker-news'),
(488,'sbadmin2','fa fa-tencent-weibo'),
(489,'sbadmin2','fa fa-qq'),
(490,'sbadmin2','fa fa-wechat'),
(491,'sbadmin2','fa fa-weixin'),
(492,'sbadmin2','fa fa-send'),
(493,'sbadmin2','fa fa-paper-plane'),
(494,'sbadmin2','fa fa-send-o'),
(495,'sbadmin2','fa fa-paper-plane-o'),
(496,'sbadmin2','fa fa-history'),
(497,'sbadmin2','fa fa-circle-thin'),
(498,'sbadmin2','fa fa-header'),
(499,'sbadmin2','fa fa-paragraph'),
(500,'sbadmin2','fa fa-sliders'),
(501,'sbadmin2','fa fa-share-alt'),
(502,'sbadmin2','fa fa-share-alt-square'),
(503,'sbadmin2','fa fa-bomb'),
(504,'bootstrap','glyphicon glyphicon-asterisk'),
(505,'bootstrap','glyphicon glyphicon-plus'),
(506,'bootstrap','glyphicon glyphicon-euro'),
(507,'bootstrap','glyphicon glyphicon-eur'),
(508,'bootstrap','glyphicon glyphicon-minus'),
(509,'bootstrap','glyphicon glyphicon-cloud'),
(510,'bootstrap','glyphicon glyphicon-envelope'),
(511,'bootstrap','glyphicon glyphicon-pencil'),
(512,'bootstrap','glyphicon glyphicon-glass'),
(513,'bootstrap','glyphicon glyphicon-music'),
(514,'bootstrap','glyphicon glyphicon-search'),
(515,'bootstrap','glyphicon glyphicon-heart'),
(516,'bootstrap','glyphicon glyphicon-star'),
(517,'bootstrap','glyphicon glyphicon-star-empty'),
(518,'bootstrap','glyphicon glyphicon-user'),
(519,'bootstrap','glyphicon glyphicon-film'),
(520,'bootstrap','glyphicon glyphicon-th-large'),
(521,'bootstrap','glyphicon glyphicon-th'),
(522,'bootstrap','glyphicon glyphicon-th-list'),
(523,'bootstrap','glyphicon glyphicon-ok'),
(524,'bootstrap','glyphicon glyphicon-remove'),
(525,'bootstrap','glyphicon glyphicon-zoom-in'),
(526,'bootstrap','glyphicon glyphicon-zoom-out'),
(527,'bootstrap','glyphicon glyphicon-off'),
(528,'bootstrap','glyphicon glyphicon-signal'),
(529,'bootstrap','glyphicon glyphicon-cog'),
(530,'bootstrap','glyphicon glyphicon-trash'),
(531,'bootstrap','glyphicon glyphicon-home'),
(532,'bootstrap','glyphicon glyphicon-file'),
(533,'bootstrap','glyphicon glyphicon-time'),
(534,'bootstrap','glyphicon glyphicon-road'),
(535,'bootstrap','glyphicon glyphicon-download-alt'),
(536,'bootstrap','glyphicon glyphicon-download'),
(537,'bootstrap','glyphicon glyphicon-upload'),
(538,'bootstrap','glyphicon glyphicon-inbox'),
(539,'bootstrap','glyphicon glyphicon-play-circle'),
(540,'bootstrap','glyphicon glyphicon-repeat'),
(541,'bootstrap','glyphicon glyphicon-refresh'),
(542,'bootstrap','glyphicon glyphicon-list-alt'),
(543,'bootstrap','glyphicon glyphicon-lock'),
(544,'bootstrap','glyphicon glyphicon-flag'),
(545,'bootstrap','glyphicon glyphicon-headphones'),
(546,'bootstrap','glyphicon glyphicon-volume-off'),
(547,'bootstrap','glyphicon glyphicon-volume-down'),
(548,'bootstrap','glyphicon glyphicon-volume-up'),
(549,'bootstrap','glyphicon glyphicon-qrcode'),
(550,'bootstrap','glyphicon glyphicon-barcode'),
(551,'bootstrap','glyphicon glyphicon-tag'),
(552,'bootstrap','glyphicon glyphicon-tags'),
(553,'bootstrap','glyphicon glyphicon-book'),
(554,'bootstrap','glyphicon glyphicon-bookmark'),
(555,'bootstrap','glyphicon glyphicon-print'),
(556,'bootstrap','glyphicon glyphicon-camera'),
(557,'bootstrap','glyphicon glyphicon-font'),
(558,'bootstrap','glyphicon glyphicon-bold'),
(559,'bootstrap','glyphicon glyphicon-italic'),
(560,'bootstrap','glyphicon glyphicon-text-height'),
(561,'bootstrap','glyphicon glyphicon-text-width'),
(562,'bootstrap','glyphicon glyphicon-align-left'),
(563,'bootstrap','glyphicon glyphicon-align-center'),
(564,'bootstrap','glyphicon glyphicon-align-right'),
(565,'bootstrap','glyphicon glyphicon-align-justify'),
(566,'bootstrap','glyphicon glyphicon-list'),
(567,'bootstrap','glyphicon glyphicon-indent-left'),
(568,'bootstrap','glyphicon glyphicon-indent-right'),
(569,'bootstrap','glyphicon glyphicon-facetime-video'),
(570,'bootstrap','glyphicon glyphicon-picture'),
(571,'bootstrap','glyphicon glyphicon-map-marker'),
(572,'bootstrap','glyphicon glyphicon-adjust'),
(573,'bootstrap','glyphicon glyphicon-tint'),
(574,'bootstrap','glyphicon glyphicon-edit'),
(575,'bootstrap','glyphicon glyphicon-share'),
(576,'bootstrap','glyphicon glyphicon-check'),
(577,'bootstrap','glyphicon glyphicon-move'),
(578,'bootstrap','glyphicon glyphicon-step-backward'),
(579,'bootstrap','glyphicon glyphicon-fast-backward'),
(580,'bootstrap','glyphicon glyphicon-backward'),
(581,'bootstrap','glyphicon glyphicon-play'),
(582,'bootstrap','glyphicon glyphicon-pause'),
(583,'bootstrap','glyphicon glyphicon-stop'),
(584,'bootstrap','glyphicon glyphicon-forward'),
(585,'bootstrap','glyphicon glyphicon-fast-forward'),
(586,'bootstrap','glyphicon glyphicon-step-forward'),
(587,'bootstrap','glyphicon glyphicon-eject'),
(588,'bootstrap','glyphicon glyphicon-chevron-left'),
(589,'bootstrap','glyphicon glyphicon-chevron-right'),
(590,'bootstrap','glyphicon glyphicon-plus-sign'),
(591,'bootstrap','glyphicon glyphicon-minus-sign'),
(592,'bootstrap','glyphicon glyphicon-remove-sign'),
(593,'bootstrap','glyphicon glyphicon-ok-sign'),
(594,'bootstrap','glyphicon glyphicon-question-sign'),
(595,'bootstrap','glyphicon glyphicon-info-sign'),
(596,'bootstrap','glyphicon glyphicon-screenshot'),
(597,'bootstrap','glyphicon glyphicon-remove-circle'),
(598,'bootstrap','glyphicon glyphicon-ok-circle'),
(599,'bootstrap','glyphicon glyphicon-ban-circle'),
(600,'bootstrap','glyphicon glyphicon-arrow-left'),
(601,'bootstrap','glyphicon glyphicon-arrow-right'),
(602,'bootstrap','glyphicon glyphicon-arrow-up'),
(603,'bootstrap','glyphicon glyphicon-arrow-down'),
(604,'bootstrap','glyphicon glyphicon-share-alt'),
(605,'bootstrap','glyphicon glyphicon-resize-full'),
(606,'bootstrap','glyphicon glyphicon-resize-small'),
(607,'bootstrap','glyphicon glyphicon-exclamation-sign'),
(608,'bootstrap','glyphicon glyphicon-gift'),
(609,'bootstrap','glyphicon glyphicon-leaf'),
(610,'bootstrap','glyphicon glyphicon-fire'),
(611,'bootstrap','glyphicon glyphicon-eye-open'),
(612,'bootstrap','glyphicon glyphicon-eye-close'),
(613,'bootstrap','glyphicon glyphicon-warning-sign'),
(614,'bootstrap','glyphicon glyphicon-plane'),
(615,'bootstrap','glyphicon glyphicon-calendar'),
(616,'bootstrap','glyphicon glyphicon-random'),
(617,'bootstrap','glyphicon glyphicon-comment'),
(618,'bootstrap','glyphicon glyphicon-magnet'),
(619,'bootstrap','glyphicon glyphicon-chevron-up'),
(620,'bootstrap','glyphicon glyphicon-chevron-down'),
(621,'bootstrap','glyphicon glyphicon-retweet'),
(622,'bootstrap','glyphicon glyphicon-shopping-cart'),
(623,'bootstrap','glyphicon glyphicon-folder-close'),
(624,'bootstrap','glyphicon glyphicon-folder-open'),
(625,'bootstrap','glyphicon glyphicon-resize-vertical'),
(626,'bootstrap','glyphicon glyphicon-resize-horizontal'),
(627,'bootstrap','glyphicon glyphicon-hdd'),
(628,'bootstrap','glyphicon glyphicon-bullhorn'),
(629,'bootstrap','glyphicon glyphicon-bell'),
(630,'bootstrap','glyphicon glyphicon-certificate'),
(631,'bootstrap','glyphicon glyphicon-thumbs-up'),
(632,'bootstrap','glyphicon glyphicon-thumbs-down'),
(633,'bootstrap','glyphicon glyphicon-hand-right'),
(634,'bootstrap','glyphicon glyphicon-hand-left'),
(635,'bootstrap','glyphicon glyphicon-hand-up'),
(636,'bootstrap','glyphicon glyphicon-hand-down'),
(637,'bootstrap','glyphicon glyphicon-circle-arrow-right'),
(638,'bootstrap','glyphicon glyphicon-circle-arrow-left'),
(639,'bootstrap','glyphicon glyphicon-circle-arrow-up'),
(640,'bootstrap','glyphicon glyphicon-circle-arrow-down'),
(641,'bootstrap','glyphicon glyphicon-globe'),
(642,'bootstrap','glyphicon glyphicon-wrench'),
(643,'bootstrap','glyphicon glyphicon-tasks'),
(644,'bootstrap','glyphicon glyphicon-filter'),
(645,'bootstrap','glyphicon glyphicon-briefcase'),
(646,'bootstrap','glyphicon glyphicon-fullscreen'),
(647,'bootstrap','glyphicon glyphicon-dashboard'),
(648,'bootstrap','glyphicon glyphicon-paperclip'),
(649,'bootstrap','glyphicon glyphicon-heart-empty'),
(650,'bootstrap','glyphicon glyphicon-link'),
(651,'bootstrap','glyphicon glyphicon-phone'),
(652,'bootstrap','glyphicon glyphicon-pushpin'),
(653,'bootstrap','glyphicon glyphicon-usd'),
(654,'bootstrap','glyphicon glyphicon-gbp'),
(655,'bootstrap','glyphicon glyphicon-sort'),
(656,'bootstrap','glyphicon glyphicon-sort-by-alphabet'),
(657,'bootstrap','glyphicon glyphicon-sort-by-alphabet-alt'),
(658,'bootstrap','glyphicon glyphicon-sort-by-order'),
(659,'bootstrap','glyphicon glyphicon-sort-by-order-alt'),
(660,'bootstrap','glyphicon glyphicon-sort-by-attributes'),
(661,'bootstrap','glyphicon glyphicon-sort-by-attributes-alt'),
(662,'bootstrap','glyphicon glyphicon-unchecked'),
(663,'bootstrap','glyphicon glyphicon-expand'),
(664,'bootstrap','glyphicon glyphicon-collapse-down'),
(665,'bootstrap','glyphicon glyphicon-collapse-up'),
(666,'bootstrap','glyphicon glyphicon-log-in'),
(667,'bootstrap','glyphicon glyphicon-flash'),
(668,'bootstrap','glyphicon glyphicon-log-out'),
(669,'bootstrap','glyphicon glyphicon-new-window'),
(670,'bootstrap','glyphicon glyphicon-record'),
(671,'bootstrap','glyphicon glyphicon-save'),
(672,'bootstrap','glyphicon glyphicon-open'),
(673,'bootstrap','glyphicon glyphicon-saved'),
(674,'bootstrap','glyphicon glyphicon-import'),
(675,'bootstrap','glyphicon glyphicon-export'),
(676,'bootstrap','glyphicon glyphicon-send'),
(677,'bootstrap','glyphicon glyphicon-floppy-disk'),
(678,'bootstrap','glyphicon glyphicon-floppy-saved'),
(679,'bootstrap','glyphicon glyphicon-floppy-remove'),
(680,'bootstrap','glyphicon glyphicon-floppy-save'),
(681,'bootstrap','glyphicon glyphicon-floppy-open'),
(682,'bootstrap','glyphicon glyphicon-credit-card'),
(683,'bootstrap','glyphicon glyphicon-transfer'),
(684,'bootstrap','glyphicon glyphicon-cutlery'),
(685,'bootstrap','glyphicon glyphicon-header'),
(686,'bootstrap','glyphicon glyphicon-compressed'),
(687,'bootstrap','glyphicon glyphicon-earphone'),
(688,'bootstrap','glyphicon glyphicon-phone-alt'),
(689,'bootstrap','glyphicon glyphicon-tower'),
(690,'bootstrap','glyphicon glyphicon-stats'),
(691,'bootstrap','glyphicon glyphicon-sd-video'),
(692,'bootstrap','glyphicon glyphicon-hd-video'),
(693,'bootstrap','glyphicon glyphicon-subtitles'),
(694,'bootstrap','glyphicon glyphicon-sound-stereo'),
(695,'bootstrap','glyphicon glyphicon-sound-dolby'),
(696,'bootstrap','glyphicon glyphicon-sound-5-1'),
(697,'bootstrap','glyphicon glyphicon-sound-6-1'),
(698,'bootstrap','glyphicon glyphicon-sound-7-1'),
(699,'bootstrap','glyphicon glyphicon-copyright-mark'),
(700,'bootstrap','glyphicon glyphicon-registration-mark'),
(701,'bootstrap','glyphicon glyphicon-cloud-download'),
(702,'bootstrap','glyphicon glyphicon-cloud-upload'),
(703,'bootstrap','glyphicon glyphicon-tree-conifer'),
(704,'bootstrap','glyphicon glyphicon-tree-deciduous'),
(705,'bootstrap','glyphicon glyphicon-cd'),
(706,'bootstrap','glyphicon glyphicon-save-file'),
(707,'bootstrap','glyphicon glyphicon-open-file'),
(708,'bootstrap','glyphicon glyphicon-level-up'),
(709,'bootstrap','glyphicon glyphicon-copy'),
(710,'bootstrap','glyphicon glyphicon-paste'),
(711,'bootstrap','glyphicon glyphicon-alert'),
(712,'bootstrap','glyphicon glyphicon-equalizer'),
(713,'bootstrap','glyphicon glyphicon-king'),
(714,'bootstrap','glyphicon glyphicon-queen'),
(715,'bootstrap','glyphicon glyphicon-pawn'),
(716,'bootstrap','glyphicon glyphicon-bishop'),
(717,'bootstrap','glyphicon glyphicon-knight'),
(718,'bootstrap','glyphicon glyphicon-baby-formula'),
(719,'bootstrap','glyphicon glyphicon-tent'),
(720,'bootstrap','glyphicon glyphicon-blackboard'),
(721,'bootstrap','glyphicon glyphicon-bed'),
(722,'bootstrap','glyphicon glyphicon-apple'),
(723,'bootstrap','glyphicon glyphicon-erase'),
(724,'bootstrap','glyphicon glyphicon-hourglass'),
(725,'bootstrap','glyphicon glyphicon-lamp'),
(726,'bootstrap','glyphicon glyphicon-duplicate'),
(727,'bootstrap','glyphicon glyphicon-piggy-bank'),
(728,'bootstrap','glyphicon glyphicon-scissors'),
(729,'bootstrap','glyphicon glyphicon-bitcoin'),
(730,'bootstrap','glyphicon glyphicon-btc'),
(731,'bootstrap','glyphicon glyphicon-xbt'),
(732,'bootstrap','glyphicon glyphicon-yen'),
(733,'bootstrap','glyphicon glyphicon-jpy'),
(734,'bootstrap','glyphicon glyphicon-ruble'),
(735,'bootstrap','glyphicon glyphicon-rub'),
(736,'bootstrap','glyphicon glyphicon-scale'),
(737,'bootstrap','glyphicon glyphicon-ice-lolly'),
(738,'bootstrap','glyphicon glyphicon-ice-lolly-tasted'),
(739,'bootstrap','glyphicon glyphicon-education'),
(740,'bootstrap','glyphicon glyphicon-option-horizontal'),
(741,'bootstrap','glyphicon glyphicon-option-vertical'),
(742,'bootstrap','glyphicon glyphicon-menu-hamburger'),
(743,'bootstrap','glyphicon glyphicon-modal-window'),
(744,'bootstrap','glyphicon glyphicon-oil'),
(745,'bootstrap','glyphicon glyphicon-grain'),
(746,'bootstrap','glyphicon glyphicon-sunglasses'),
(747,'bootstrap','glyphicon glyphicon-text-size'),
(748,'bootstrap','glyphicon glyphicon-text-color'),
(749,'bootstrap','glyphicon glyphicon-text-background'),
(750,'bootstrap','glyphicon glyphicon-object-align-top'),
(751,'bootstrap','glyphicon glyphicon-object-align-bottom'),
(752,'bootstrap','glyphicon glyphicon-object-align-horizontal'),
(753,'bootstrap','glyphicon glyphicon-object-align-left'),
(754,'bootstrap','glyphicon glyphicon-object-align-vertical'),
(755,'bootstrap','glyphicon glyphicon-object-align-right'),
(756,'bootstrap','glyphicon glyphicon-triangle-right'),
(757,'bootstrap','glyphicon glyphicon-triangle-left'),
(758,'bootstrap','glyphicon glyphicon-triangle-bottom'),
(759,'bootstrap','glyphicon glyphicon-triangle-top'),
(760,'bootstrap','glyphicon glyphicon-console'),
(761,'bootstrap','glyphicon glyphicon-superscript'),
(762,'bootstrap','glyphicon glyphicon-subscript'),
(763,'bootstrap','glyphicon glyphicon-menu-left'),
(764,'bootstrap','glyphicon glyphicon-menu-right'),
(765,'bootstrap','glyphicon glyphicon-menu-down'),
(766,'bootstrap','glyphicon glyphicon-menu-up');

UNLOCK TABLES;

/*Table structure for table `frm_attributes` */

DROP TABLE IF EXISTS `frm_attributes`;

CREATE TABLE `frm_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `att_form` int(10) unsigned NOT NULL,
  `att_name` varchar(64) DEFAULT NULL,
  `att_field` varchar(64) NOT NULL,
  `att_title` varchar(128) DEFAULT NULL,
  `att_type` enum('Text','Email','Number','Date','Datetime','Time','Radio','Dropdown','Checkbox','Enum','Icon') NOT NULL DEFAULT 'Text',
  `att_type_helper` varchar(128) DEFAULT NULL,
  `att_placeholder` varchar(128) DEFAULT NULL,
  `att_default` varchar(128) DEFAULT NULL,
  `att_required` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `att_id` varchar(128) DEFAULT NULL,
  `att_class` varchar(256) DEFAULT NULL,
  `att_attributes` varchar(256) DEFAULT NULL,
  `att_span` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `att_group` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `att_grouname` varchar(32) DEFAULT NULL,
  `att_position` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `att_trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `form` (`att_form`),
  CONSTRAINT `form` FOREIGN KEY (`att_form`) REFERENCES `frm_form` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `frm_attributes` */

LOCK TABLES `frm_attributes` WRITE;

UNLOCK TABLES;

/*Table structure for table `frm_controller` */

DROP TABLE IF EXISTS `frm_controller`;

CREATE TABLE `frm_controller` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `c_schema` varchar(64) NOT NULL,
  `c_schema_prefix` varchar(5) DEFAULT NULL,
  `c_name` varchar(64) NOT NULL,
  `c_title` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `frm_controller` */

LOCK TABLES `frm_controller` WRITE;

UNLOCK TABLES;

/*Table structure for table `frm_form` */

DROP TABLE IF EXISTS `frm_form`;

CREATE TABLE `frm_form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `f_controller` int(10) unsigned NOT NULL,
  `f_name` varchar(32) NOT NULL,
  `f_title` varchar(128) NOT NULL,
  `f_action_after` varchar(128) DEFAULT NULL,
  `f_updated` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `form_name` (`f_name`),
  KEY `controller_form` (`f_controller`),
  CONSTRAINT `controller_form` FOREIGN KEY (`f_controller`) REFERENCES `frm_controller` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `frm_form` */

LOCK TABLES `frm_form` WRITE;

UNLOCK TABLES;

/*Table structure for table `frm_view` */

DROP TABLE IF EXISTS `frm_view`;

CREATE TABLE `frm_view` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `v_controller` int(10) unsigned NOT NULL,
  `v_name` varchar(128) NOT NULL,
  `v_functions` varchar(128) NOT NULL DEFAULT 'edit,remove',
  `v_updateded` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `viewname` (`v_name`),
  KEY `controller_view` (`v_controller`),
  CONSTRAINT `controller_view` FOREIGN KEY (`v_controller`) REFERENCES `frm_controller` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `frm_view` */

LOCK TABLES `frm_view` WRITE;

UNLOCK TABLES;

/*Table structure for table `frm_view_filter` */

DROP TABLE IF EXISTS `frm_view_filter`;

CREATE TABLE `frm_view_filter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vf_view` int(10) unsigned NOT NULL,
  `vf_field` varchar(64) NOT NULL,
  `vf_name` varchar(64) NOT NULL,
  `vf_data_helper` varchar(128) DEFAULT NULL,
  `vf_default` varchar(128) DEFAULT NULL,
  `vf_position` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `view_filter` (`vf_view`),
  CONSTRAINT `view_filter` FOREIGN KEY (`vf_view`) REFERENCES `frm_view` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `frm_view_filter` */

LOCK TABLES `frm_view_filter` WRITE;

UNLOCK TABLES;

/*Table structure for table `frm_view_item` */

DROP TABLE IF EXISTS `frm_view_item`;

CREATE TABLE `frm_view_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `vi_view` int(10) unsigned NOT NULL,
  `vi_field` varchar(64) NOT NULL,
  `vi_title` varchar(128) DEFAULT NULL,
  `vi_function` varchar(128) DEFAULT NULL,
  `vi_position` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `vi_aggrigate_function` varchar(128) NOT NULL,
  `vi_trash` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`vi_position`),
  KEY `view_item` (`vi_view`),
  CONSTRAINT `view_item` FOREIGN KEY (`vi_view`) REFERENCES `frm_view` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `frm_view_item` */

LOCK TABLES `frm_view_item` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_acl` */

DROP TABLE IF EXISTS `sys_acl`;

CREATE TABLE `sys_acl` (
  `appliesto` int(4) NOT NULL,
  `utype` enum('u','r') NOT NULL,
  `privilege` int(3) NOT NULL,
  `access` tinyint(1) NOT NULL DEFAULT '0',
  `entrytime` datetime NOT NULL,
  `entryby` int(10) unsigned NOT NULL,
  PRIMARY KEY (`appliesto`,`utype`,`privilege`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `sys_acl` */

LOCK TABLES `sys_acl` WRITE;

insert  into `sys_acl`(`appliesto`,`utype`,`privilege`,`access`,`entrytime`,`entryby`) values 
(3,'r',160,1,'0000-00-00 00:00:00',1),
(3,'r',161,1,'0000-00-00 00:00:00',1),
(3,'r',162,1,'0000-00-00 00:00:00',1),
(3,'r',159,1,'0000-00-00 00:00:00',1),
(3,'r',156,1,'0000-00-00 00:00:00',1),
(3,'r',157,1,'0000-00-00 00:00:00',1),
(3,'r',158,1,'0000-00-00 00:00:00',1),
(3,'r',155,1,'0000-00-00 00:00:00',1),
(3,'r',168,1,'0000-00-00 00:00:00',1),
(3,'r',169,1,'0000-00-00 00:00:00',1),
(3,'r',170,1,'0000-00-00 00:00:00',1),
(3,'r',167,1,'0000-00-00 00:00:00',1),
(3,'r',164,1,'0000-00-00 00:00:00',1),
(3,'r',165,1,'0000-00-00 00:00:00',1),
(3,'r',166,1,'0000-00-00 00:00:00',1),
(3,'r',163,1,'0000-00-00 00:00:00',1),
(2,'u',160,1,'0000-00-00 00:00:00',1),
(2,'u',162,1,'0000-00-00 00:00:00',1),
(2,'u',159,1,'0000-00-00 00:00:00',1),
(2,'u',156,1,'0000-00-00 00:00:00',1),
(2,'u',158,1,'0000-00-00 00:00:00',1),
(2,'u',155,1,'0000-00-00 00:00:00',1),
(2,'u',168,1,'0000-00-00 00:00:00',1),
(2,'u',170,1,'0000-00-00 00:00:00',1),
(2,'u',167,1,'0000-00-00 00:00:00',1),
(2,'u',164,1,'0000-00-00 00:00:00',1),
(2,'u',166,1,'0000-00-00 00:00:00',1),
(2,'u',163,1,'0000-00-00 00:00:00',1),
(2,'u',176,1,'0000-00-00 00:00:00',1),
(2,'u',178,1,'0000-00-00 00:00:00',1),
(2,'u',175,1,'0000-00-00 00:00:00',1),
(2,'u',172,1,'0000-00-00 00:00:00',1),
(2,'u',174,1,'0000-00-00 00:00:00',1),
(2,'u',171,1,'0000-00-00 00:00:00',1),
(2,'u',151,1,'0000-00-00 00:00:00',1),
(2,'u',153,1,'0000-00-00 00:00:00',1),
(2,'u',150,1,'0000-00-00 00:00:00',1),
(2,'u',147,1,'0000-00-00 00:00:00',1),
(2,'u',149,1,'0000-00-00 00:00:00',1),
(2,'u',146,1,'0000-00-00 00:00:00',1),
(2,'u',142,1,'0000-00-00 00:00:00',1),
(2,'u',145,1,'0000-00-00 00:00:00',1),
(2,'u',141,1,'0000-00-00 00:00:00',1),
(2,'u',138,1,'0000-00-00 00:00:00',1),
(2,'u',140,1,'0000-00-00 00:00:00',1),
(2,'u',137,1,'0000-00-00 00:00:00',1),
(2,'u',128,1,'0000-00-00 00:00:00',1),
(2,'u',130,1,'0000-00-00 00:00:00',1),
(2,'u',127,1,'0000-00-00 00:00:00',1),
(2,'u',132,1,'0000-00-00 00:00:00',1),
(2,'u',135,1,'0000-00-00 00:00:00',1),
(2,'u',131,1,'0000-00-00 00:00:00',1),
(2,'u',124,1,'0000-00-00 00:00:00',1),
(2,'u',111,1,'0000-00-00 00:00:00',1),
(2,'u',112,1,'0000-00-00 00:00:00',1),
(2,'u',114,1,'0000-00-00 00:00:00',1),
(2,'u',125,1,'0000-00-00 00:00:00',1),
(2,'u',121,1,'0000-00-00 00:00:00',1),
(2,'u',118,1,'0000-00-00 00:00:00',1),
(2,'u',115,1,'0000-00-00 00:00:00',1),
(2,'u',117,1,'0000-00-00 00:00:00',1),
(2,'u',116,1,'0000-00-00 00:00:00',1);

UNLOCK TABLES;

/*Table structure for table `sys_change_log` */

DROP TABLE IF EXISTS `sys_change_log`;

CREATE TABLE `sys_change_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `l_module` varchar(64) NOT NULL,
  `l_id` int(10) unsigned NOT NULL,
  `l_time` datetime NOT NULL,
  `l_user` int(11) DEFAULT NULL,
  `l_old_value` text NOT NULL,
  `l_new_value` text NOT NULL,
  `l_remarks` text,
  `l_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sys_change_log` */

LOCK TABLES `sys_change_log` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_fraud_user` */

DROP TABLE IF EXISTS `sys_fraud_user`;

CREATE TABLE `sys_fraud_user` (
  `f_username` varchar(32) NOT NULL,
  `f_password` varchar(64) DEFAULT NULL,
  `f_ip` varchar(15) NOT NULL,
  `f_attempts` int(11) NOT NULL DEFAULT '1',
  `f_date` datetime NOT NULL,
  PRIMARY KEY (`f_username`,`f_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sys_fraud_user` */

LOCK TABLES `sys_fraud_user` WRITE;

insert  into `sys_fraud_user`(`f_username`,`f_password`,`f_ip`,`f_attempts`,`f_date`) values 
('sysem','21232f297a57a5a743894a0e4a801fc3','::1',1,'2017-12-29 16:03:48'),
('system','21232f297a57a5a743894a0e4a801fc3','::1',1,'2017-06-14 17:17:17');

UNLOCK TABLES;

/*Table structure for table `sys_guest_log` */

DROP TABLE IF EXISTS `sys_guest_log`;

CREATE TABLE `sys_guest_log` (
  `gl_time` datetime NOT NULL,
  `gl_ip` varchar(64) NOT NULL,
  `gl_url` varchar(256) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `sys_guest_log` */

LOCK TABLES `sys_guest_log` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_log` */

DROP TABLE IF EXISTS `sys_log`;

CREATE TABLE `sys_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `l_logger` varchar(128) NOT NULL,
  `l_type` varchar(128) NOT NULL,
  `l_msg` text NOT NULL,
  `l_ref1` varchar(128) DEFAULT NULL,
  `l_ref2` varchar(128) DEFAULT NULL,
  `l_ref3` varchar(128) DEFAULT NULL,
  `l_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sys_log` */

LOCK TABLES `sys_log` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_privilege` */

DROP TABLE IF EXISTS `sys_privilege`;

CREATE TABLE `sys_privilege` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(64) NOT NULL DEFAULT 'sys',
  `module` varchar(64) DEFAULT NULL,
  `link` varchar(64) DEFAULT NULL,
  `option` varchar(24) DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `title` varchar(32) DEFAULT NULL,
  `note` text,
  `root` int(2) DEFAULT NULL,
  `position` int(2) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `glyphicon` varchar(64) NOT NULL,
  `icon` varchar(128) NOT NULL DEFAULT 'noicon.png',
  `show_in_frontpage` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `password_required` tinyint(1) NOT NULL DEFAULT '0',
  `controller` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `target` enum('_blank','_self','_parent','_top') NOT NULL DEFAULT '_top',
  `sk1` enum('Ctrl','Alt','Shift') DEFAULT NULL,
  `sk2` enum('Ctrl','Alt','Shift') DEFAULT NULL,
  `sk3` enum('Ctrl','Alt','Shift') DEFAULT NULL,
  `sk4` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `link` (`link`,`option`,`root`),
  UNIQUE KEY `shortcut` (`sk1`,`sk2`,`sk3`,`sk4`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `sys_privilege` */

LOCK TABLES `sys_privilege` WRITE;

insert  into `sys_privilege`(`id`,`app`,`module`,`link`,`option`,`name`,`title`,`note`,`root`,`position`,`active`,`glyphicon`,`icon`,`show_in_frontpage`,`hidden`,`password_required`,`controller`,`target`,`sk1`,`sk2`,`sk3`,`sk4`) values 
(0,'sys','sys',NULL,NULL,'Root','Root',NULL,NULL,0,1,'','noicon.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(1,'sys','sys',NULL,NULL,'System','System',NULL,0,99,1,'fa fa-gears','system2.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(2,'sys','sys',NULL,NULL,'UNALLOCATED','UNALLOCATED',NULL,0,2,1,'glyphicon glyphicon-knight','add32.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(3,'sys','sys',NULL,NULL,'NOTC','NOTC',NULL,0,0,1,'fa fa-adjust','',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(5,'sys','user',NULL,NULL,'User Management','User Management',NULL,0,98,1,'fa fa-users','group.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(6,'sys','menu','menu','remove','Remove Menu','Remove Menu',NULL,1,0,1,'glyphicon glyphicon-trash','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(7,'sys','menu','menu','edit','Edit Menu','Edit Menu',NULL,1,0,1,'glyphicon glyphicon-edit','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(8,'sys','menu','menu','hide','Hide Menu','Hide Menu',NULL,1,0,1,'glyphicon glyphicon-eye-close','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(9,'sys','menu','menu','show','Show Menu','Show Menu',NULL,1,0,1,'glyphicon glyphicon-eye-open','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(10,'sys','menu','menu','front','Add to Homepage','Add to Homepage',NULL,1,0,1,'glyphicon glyphicon-th-large','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(11,'sys','menu','menu','remove_front','Remove from Homepage','Remove from Homepage',NULL,1,0,1,'glyphicon glyphicon-folder-close','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(12,'sys','menu','menu','deactivate','Deactivate Menu','Deactivate Menu',NULL,1,0,1,'glyphicon glyphicon-eye-close','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(13,'sys','menu','menu','activate','Activate Menu','Activate Menu',NULL,1,0,1,'glyphicon glyphicon-eye-open','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(14,'sys','menu','menu','set_controller','Activate Controller','Activate Controller',NULL,1,0,1,'glyphicon glyphicon-volume-down','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(15,'sys','menu','menu','not_controller','Deactivate Controller','Deactivate Controller',NULL,1,0,1,'glyphicon glyphicon-volume-off','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(16,'sys','menu','menu','view','View Menu','View Menu',NULL,1,0,1,'glyphicon glyphicon-list','noicon.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(17,'sys','menu','menu','add','Add Menu','Add Menu',NULL,1,0,1,'glyphicon glyphicon-plus-sign','noicon.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(18,'sys','user','user','add','Add User','Add User',NULL,5,0,1,'glyphicon glyphicon-file','add.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(19,'sys','user','user','view','List of Users','List of Users','User Management',5,0,1,'fa fa-user','group.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(20,'sys','user','user','permission','User Privilege','User Privilege',NULL,5,0,1,'fa fa-user-md','control.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(21,'sys','user','role','view','List of Roles','List of Roles',NULL,5,0,1,'fa fa-group','group.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(22,'sys','user','user','edit','Edit User','Edit User',NULL,5,0,1,'glyphicon glyphicon-edit','edit.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(23,'sys','user','user','remove','Remove User','Remove User',NULL,5,0,1,'glyphicon glyphicon-trash','delete.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(24,'sys','user','role','add','New Role','New Role',NULL,5,0,1,'glyphicon glyphicon-file','add32.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(25,'sys','user','role','edit','Edit Role','Edit Role',NULL,5,0,1,'glyphicon glyphicon-edit','edit.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(26,'sys','user','role','remove','Remove Role','Remove Role',NULL,5,0,1,'glyphicon glyphicon-trash','delete.png',0,1,0,0,'_top',NULL,NULL,NULL,NULL),
(27,'sys',NULL,'sys_register','view','Config','Config',NULL,1,0,1,'fa fa-wrench','noicon.png',0,0,0,0,'_top',NULL,NULL,NULL,NULL),
(28,'sys',NULL,'sys_register','add','Add Sys Register','Add Sys Register',NULL,1,1,1,'glyphicon glyphicon-file','noicon.png',0,1,0,1,'_top',NULL,NULL,NULL,NULL),
(29,'sys',NULL,'sys_register','edit','Edit Sys Register','Edit Sys Register',NULL,1,2,1,'glyphicon glyphicon-edit','noicon.png',0,1,0,1,'_top',NULL,NULL,NULL,NULL),
(30,'sys',NULL,'sys_register','erase','Erase Sys Register','Erase Sys Register',NULL,1,3,1,'glyphicon glyphicon-remove','noicon.png',0,1,0,1,'_top',NULL,NULL,NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `sys_privilege_shortcut` */

DROP TABLE IF EXISTS `sys_privilege_shortcut`;

CREATE TABLE `sys_privilege_shortcut` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ps_privilege` int(10) unsigned NOT NULL,
  `ps_sk1` enum('Ctrl','Alt','Shift') DEFAULT NULL,
  `ps_sk2` enum('Ctrl','Alt','Shift') DEFAULT NULL,
  `ps_sk3` enum('Ctrl','Alt','Shift') DEFAULT NULL,
  `ps_sk4` varchar(12) DEFAULT NULL,
  `ps_user` int(10) unsigned NOT NULL,
  `ps_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ps_privilege` (`ps_privilege`),
  CONSTRAINT `ps_privilege` FOREIGN KEY (`ps_privilege`) REFERENCES `sys_privilege` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sys_privilege_shortcut` */

LOCK TABLES `sys_privilege_shortcut` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_register` */

DROP TABLE IF EXISTS `sys_register`;

CREATE TABLE `sys_register` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(128) NOT NULL,
  `value` text,
  `group` varchar(32) DEFAULT NULL,
  `description` text,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `register_key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sys_register` */

LOCK TABLES `sys_register` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_registration` */

DROP TABLE IF EXISTS `sys_registration`;

CREATE TABLE `sys_registration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `r_username` varchar(256) NOT NULL,
  `r_password` varchar(64) NOT NULL,
  `r_ip` varchar(64) NOT NULL,
  `r_geo` text,
  `r_time` datetime NOT NULL,
  `r_verification` varchar(64) NOT NULL,
  `r_status` enum('Pending','Approved','Rejected') NOT NULL DEFAULT 'Pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sys_registration` */

LOCK TABLES `sys_registration` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `r_name` varchar(32) NOT NULL,
  `r_active` tinyint(1) NOT NULL DEFAULT '1',
  `r_date_created` date DEFAULT NULL,
  `r_owner` int(10) unsigned NOT NULL DEFAULT '0',
  `r_scope` enum('Global','Local') NOT NULL,
  `r_created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `sys_role` */

LOCK TABLES `sys_role` WRITE;

insert  into `sys_role`(`id`,`r_name`,`r_active`,`r_date_created`,`r_owner`,`r_scope`,`r_created_by`) values 
(0,'Guest',1,'2015-02-17',0,'Global',0),
(1,'System',1,'2011-01-03',0,'Global',0),
(2,'System Administrator',1,'2013-08-22',1,'Global',1),
(3,'Accounts',1,'2014-12-15',2,'Global',2),
(4,'Admin',1,'2014-12-15',2,'Global',2),
(5,'Customer Service',1,NULL,0,'Global',0);

UNLOCK TABLES;

/*Table structure for table `sys_search_options` */

DROP TABLE IF EXISTS `sys_search_options`;

CREATE TABLE `sys_search_options` (
  `schema` varchar(128) DEFAULT NULL,
  `object` varchar(128) DEFAULT NULL,
  `display` varchar(128) DEFAULT NULL,
  `functions` varchar(128) DEFAULT NULL,
  `keys` varchar(128) DEFAULT NULL,
  `note` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `sys_search_options` */

LOCK TABLES `sys_search_options` WRITE;

insert  into `sys_search_options`(`schema`,`object`,`display`,`functions`,`keys`,`note`) values 
('company','company','name','statement','name, reg_no, phone','Company'),
('os_agent','agent','a_name','statement','a_name, a_phone','Agent'),
('bill_invoice','invoice','id','print','id','Invoice'),
('bill_invoice_payment','bill_invoice_payment','id','print','id','Receipt'),
('worker','worker','name','view, edit, statement','passport, name','Invidual Worker');

UNLOCK TABLES;

/*Table structure for table `sys_sessions` */

DROP TABLE IF EXISTS `sys_sessions`;

CREATE TABLE `sys_sessions` (
  `id` varchar(32) NOT NULL,
  `access` int(10) unsigned DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sys_sessions` */

LOCK TABLES `sys_sessions` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_site` */

DROP TABLE IF EXISTS `sys_site`;

CREATE TABLE `sys_site` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varbinary(128) NOT NULL,
  `s_description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sys_site` */

LOCK TABLES `sys_site` WRITE;

insert  into `sys_site`(`id`,`s_name`,`s_description`) values 
(1,0x44656661756c74,NULL);

UNLOCK TABLES;

/*Table structure for table `sys_site_privilege` */

DROP TABLE IF EXISTS `sys_site_privilege`;

CREATE TABLE `sys_site_privilege` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sp_site` int(10) unsigned NOT NULL,
  `sp_privilege` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_privilege` (`sp_site`,`sp_privilege`),
  KEY `sp_privilege` (`sp_privilege`),
  CONSTRAINT `sp_privilege` FOREIGN KEY (`sp_privilege`) REFERENCES `sys_privilege` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sp_site` FOREIGN KEY (`sp_site`) REFERENCES `sys_site` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sys_site_privilege` */

LOCK TABLES `sys_site_privilege` WRITE;

UNLOCK TABLES;

/*Table structure for table `sys_token` */

DROP TABLE IF EXISTS `sys_token`;

CREATE TABLE `sys_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `page` varchar(256) NOT NULL,
  `token` varchar(128) NOT NULL,
  `times` int(10) unsigned NOT NULL DEFAULT '1',
  `valid_till` datetime NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `sys_token` */

LOCK TABLES `sys_token` WRITE;

insert  into `sys_token`(`id`,`user`,`page`,`token`,`times`,`valid_till`,`status`) values 
(1,1,'','64333239306637616630373962336162666239306165623634616564336463303630356335326366',1,'2018-01-27 00:00:00',1),
(2,1,'','3d88f60c328b7c96f3b959ea9b42dfba9a60ed67',1,'2018-01-27 00:00:00',1),
(3,1,'','aaa7ff7613faed0894a23ca0cafc7cec329f72a0',1,'2018-01-27 00:00:00',1),
(4,1,'','765473630ed7e898826df23814e29198a199f55744e358462598675860',1,'2018-01-27 00:00:00',1),
(5,1,'ajax/list/guest','9386140001b9fbf643bfce031886ad0ad8eea149957321da71362159182',1,'2018-01-27 00:00:00',1),
(6,1,'ajax/list/guest','7633444356e21928e1dc8c59d568c95c7ffb1a7d7203ea293319152010',1,'2018-01-27 00:00:00',1),
(7,1,'ajax/list/guest','5162262412aa70af7079d5b63060346d313bd2bda476adadb925698057',1,'2018-01-27 00:00:00',1),
(8,1,'ajax/list/guest','1447572098df4e091c50f3ec9831d830221b7100e371cd6c0c1421878841',1,'2018-01-27 00:00:00',1),
(9,1,'ajax/list/guest','13471138026c134bef7a561dc3e763bc749dc1007db42ea9d1569233066',1,'2018-01-27 00:00:00',1),
(10,1,'ajax/list/guest','2326282487873adecb42fc7cc86e08d0ce5447bbd9b2b698c305448961',1,'2018-01-27 00:00:00',1),
(11,1,'ajax/list/guest','148766306189cdad44af500366145452befc7fa38494481778814221856',1,'2018-01-27 00:00:00',1),
(12,1,'ajax/list/guest','863525172133eb92012192a4ffa7a651628a430dd86639203146011898',1,'2018-01-27 00:00:00',1),
(13,1,'ajax/list/guest','1269710219e858b651d803ff1dd055779391e5658f798ea0161423638325',1,'2018-01-27 00:00:00',1),
(14,1,'ajax/list/guest','2589233967b487e3057ba5b6206997935b9f430bc255085ca327299886',1,'2018-01-27 00:00:00',1),
(15,1,'ajax/list/guest','266608230326785db80d72e7a7d4bafe3bfef44db0a2fa6c31401972774',1,'2018-01-27 00:00:00',1),
(16,1,'ajax/list/guest','646868558da45f40d9533fe7907e6a3f767ef97d477971e12991528665',1,'2018-01-27 00:00:00',1),
(17,1,'ajax/list/guest','327161015d62ddd952cfdec71f0689b655ad1d70d14ecdc6a512198822',1,'2018-01-27 00:00:00',1),
(18,1,'ajax/list/guest','867506594d62ddd952cfdec71f0689b655ad1d70d14ecdc6a1196241511',1,'2018-01-27 00:00:00',1),
(19,1,'ajax/list/guest','881857795ec72a7c823da7a128591fbd0fffa0b85fbca300e1101338468',1,'2018-01-27 00:00:00',1),
(20,1,'ajax/list/guest','8243604446d8df857ff0adfddfd6af3c773e4c80f6d0a7a5f1139021940',1,'2018-01-27 00:00:00',1),
(21,1,'ajax/list/guest','42836009026a85d8eb44783d74be02d027812ba79ec7782a41066340099',1,'2018-01-27 00:00:00',1),
(22,1,'ajax/list/guest','5175226839660233e10c6339635143f4826ba649d9e6fc0091161798717',1,'2018-01-27 00:00:00',1),
(23,1,'ajax/list/guest','110962519662734be9a2cad0c798cce34cabd01c3ad84dade5329151688',1,'2018-01-27 00:00:00',1),
(24,1,'ajax/list/guest','10373137503172b76ed7894d443b24b46427869ee43da0adbd269061863',1,'2018-01-27 00:00:00',1),
(25,1,'ajax/list/guest','6027965985057b98a7b72a060fee00d190d0494142c210077548956468',1,'2018-01-27 00:00:00',1),
(26,1,'ajax/list/guest','1198602684dc62929207ad4ce699e372e292c4081c0e5c416b1257720351',1,'2018-01-27 00:00:00',1),
(27,1,'ajax/list/guest','37743654404d20ba7616c21a3d58515f10e8b77ba05e91ea71109486401',1,'2018-01-27 00:00:00',1),
(28,1,'ajax/list/guest','5747423487646c3c9c24f985a5df8609e1c089f6116f34f8d98328938',1,'2018-01-27 00:00:00',1),
(29,1,'ajax/list/guest','11446239014e4b73c866bf834c7347055d98e295b9391a92f4635989656',1,'2018-01-27 00:00:00',1),
(30,1,'ajax/list/guest','10145372568495115eb8eb65575ca8607092f78a11e2c474de1104764631',1,'2018-01-27 00:00:00',1),
(31,1,'ajax/list/guest','1339985419c3b18818d893ced202f1739cea5b21db0112e23e29165439',1,'2018-01-27 00:00:00',1),
(32,1,'ajax/list/guest','57437212538ee0039ce97750c5100873a2db070bd698b1e17247581432',1,'2018-01-27 00:00:00',1),
(33,1,'ajax/list/guest','1576318242d4687b29fef36bf246ea5708c1dc5e127cd2c54948197744',1,'2018-01-27 00:00:00',1),
(34,1,'ajax/list/guest','1103088162b941565cb9239415c15faebed578110fdd553886323699757',1,'2018-01-28 00:00:00',1),
(35,1,'ajax/list/guest','584113181df879e12cccf90c2ff5341d2642b57ea7d65a6451259891785',1,'2018-01-28 00:00:00',1),
(36,1,'ajax/list/guest','65698392703b224ddaf4134be5d89071f55955d54f63ae852337589140',1,'2018-01-28 00:00:00',1),
(37,1,'ajax/list/guest','296524813e028800cd8ab676d2739d2d4622a084b22097a411106377554',1,'2018-01-28 00:00:00',1),
(38,1,'ajax/list/guest','1084942633c79cfa3b0b778735451b32032b0c96232a0d11e652221784',1,'2018-01-28 00:00:00',1),
(39,1,'ajax/list/guest','764482989542b68243d6627ab47f2b623eed86c795aa959591388411766',1,'2018-01-28 00:00:00',1),
(40,1,'ajax/list/guest','44328260471d88b0a325ef0987635e85f0e8424ea47f669bd1506095810',1,'2018-01-28 00:00:00',1),
(41,1,'ajax/list/guest','1182071367485aeddb515b7f208aeeba7097cc0105db9de71a731196339',1,'2018-01-28 00:00:00',1),
(42,1,'ajax/list/guest','58087383780dbb08fd37355b507d63eee53ff1cbeb02b5e201428133762',1,'2018-01-28 00:00:00',1),
(43,1,'ajax/list/guest','15018830489909da6686154e3febcbabe595205c16d0a1702635462704',1,'2018-01-28 00:00:00',1),
(44,1,'ajax/list/guest','1071054162af75a4a76c71c2a32d2ca6e10f758bc61cf666a8339811503',1,'2018-01-28 00:00:00',1);

UNLOCK TABLES;

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_fullname` varchar(128) NOT NULL,
  `u_username` varchar(64) NOT NULL,
  `u_password` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `u_pin` varchar(32) NOT NULL DEFAULT '1111',
  `u_date_created` datetime NOT NULL,
  `u_loggedin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `u_last_login_time` datetime DEFAULT NULL,
  `u_last_ip` varchar(42) DEFAULT NULL,
  `u_remarks` text,
  `u_email` varchar(128) DEFAULT NULL,
  `u_created_by` int(10) unsigned NOT NULL,
  `u_last_modified_by` int(10) unsigned NOT NULL,
  `u_failed_attempt` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `u_status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `u_avatar` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`u_username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `sys_user` */

LOCK TABLES `sys_user` WRITE;

insert  into `sys_user`(`id`,`u_fullname`,`u_username`,`u_password`,`u_pin`,`u_date_created`,`u_loggedin`,`u_last_login_time`,`u_last_ip`,`u_remarks`,`u_email`,`u_created_by`,`u_last_modified_by`,`u_failed_attempt`,`u_status`,`u_avatar`) values 
(0,'Guest User','guest','','1111','0000-00-00 00:00:00',1,'2018-01-27 09:54:38','::1',NULL,NULL,0,0,0,1,NULL),
(1,'System','system','21232f297a57a5a743894a0e4a801fc3','1111','2014-12-15 23:40:24',1,'2018-02-23 05:04:45','::1',NULL,NULL,0,0,0,1,NULL),
(2,'Administrator','admin','21232f297a57a5a743894a0e4a801fc3','1111','2018-02-23 05:06:29',1,'2017-12-29 16:03:52','::1','','admin@gmail.com',1,1,0,1,NULL),
(3,'user','user','21232f297a57a5a743894a0e4a801fc3','1111','0000-00-00 00:00:00',0,NULL,NULL,NULL,NULL,0,0,0,1,NULL);

UNLOCK TABLES;

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ur_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ur_role_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ur_user_id`,`ur_role_id`),
  KEY `role` (`ur_role_id`),
  KEY `id` (`id`),
  CONSTRAINT `ur_role_id` FOREIGN KEY (`ur_role_id`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ur_user_id` FOREIGN KEY (`ur_user_id`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `sys_user_role` */

LOCK TABLES `sys_user_role` WRITE;

insert  into `sys_user_role`(`id`,`ur_user_id`,`ur_role_id`) values 
(0,0,0),
(1,1,1),
(3,2,2);

UNLOCK TABLES;

/*Table structure for table `tax_codes` */

DROP TABLE IF EXISTS `tax_codes`;

CREATE TABLE `tax_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(8) NOT NULL,
  `rate` decimal(3,2) unsigned NOT NULL,
  `type` varchar(12) NOT NULL,
  `description` text,
  `explanation` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `tax_codes` */

LOCK TABLES `tax_codes` WRITE;

insert  into `tax_codes`(`id`,`name`,`rate`,`type`,`description`,`explanation`) values 
(1,'SR',6.00,'S','Standard-rated supplies with GST charged.','A GST registered supplier must charge and account GST at 6% for all sales of goods and services made in Malaysia unless the supply qualifies for zero-rating, exemption or falls outside the scope of the proposed GST model. The GST collected from customer is called output tax. The value of sale and corresponding output tax must be reported in the GST returns.'),
(2,'ZRL',0.00,'S','Local supply of goods or services which are subject to zero rated supplies.','A GST registered supplier can zero-rate (i.e. charging GST at 0%) certain local supply of goods and services if such goods or services are included in the Goods and Services Tax (Zero Rate Supplies) Order 2014. Examples includes sale of fish, cooking oil.'),
(3,'ZRE',0.00,'S','Exportation of goods or services which are subject to zero rated supplies.','A GST registered supplier can zero-rate (i.e. charging GST at 0%) the supply of goods and services if they export the goods out of Malaysia or the services fall within the description of international services. Examples includes sale of air-tickets and international freight charges.'),
(4,'DS',6.00,'S','Deemed supplies (e.g. transfer or disposal of business assets without consideration).','GST is chargeable on supplies of goods and services. For GST to be applicable there must be goods or services provided and a consideration paid in return. However, there are situations where a supply has taken place even though no goods or services are provided or no consideration is paid. These are known as deemed supplies. Examples include free gifts (more than RM500) and disposal of business assets without consideration.'),
(5,'OS',0.00,'S','Out-of-scope supplies.','This refers to supplies (commonly known as out-of-scope supply) which are outside the scope and GST is therefore not chargeable. In general, they are transfer of business as a going concern, private transactions, third country sales (i.e. sale of goods from a place outside Malaysia to another place outside Malaysia).'),
(6,'ES',0.00,'S','Exempt supplies under GST.','This refers to supplies which are exempt under GST. These supply include residential property, public transportation etc.'),
(7,'ES43',0.00,'S','Incidental Exempt Supplies','This is only applicable to GST registered trader that makes both taxable and exempt supplies (or commonly known as partially exempt trader). This refers to exempt supplies made under incidental exempt supplies. Incidental Exempt Supplies include interest income from deposits placed with a financial institution in Malaysia, realized foreign exchange gains or losses, first issue of bonds, first issue of shares through an Initial Public Offering and interest received from loans provided to employees also include factoring receivables, money received from unit holders for units received by a unit trust etc.'),
(8,'RS',0.00,'S','Relief supply under GST.','This refers to supplies which are supply given relief from GST.'),
(9,'GS',0.00,'S','Disregarded supplies.','This refers to supplies which are disregarded under GST. These supplies include supply within GST group registration, sales made within Warehouse Scheme etc.'),
(10,'AJS',6.00,'S','Any adjustment made to Output Tax e.g. : Longer period adjustment, Bad Debt recover, outstanding invoice > 6 months & other output tax adjustments.','Any adjustment made to Output Tax, Example such as longer period adjustment, bad debt recovered, outstanding invoices more than 6 months & other output tax adjustments'),
(11,'TX',6.00,'P','Purchases with GST incurred at 6% and directly attributable to taxable supplies.','This refers to goods and/or services purchased from GST registered suppliers. The prevailing GST rate is 6% w.e.f 1/4/2015. As it is a tax on final consumption, a GST registered trader will be able to claim credits for GST paid on goods or services supplied to them. The recoverable credits are called input tax. Examples include goods or services purchased for business purposes from GST registered traders.'),
(12,'IM',6.00,'P','Import of goods with GST incurred.','All goods imported into Malaysia are subjected to duties and/or GST. GST is calculated on the value which includes cost, insurance and freight plus the customs duty payable (if any), unless the imported goods are for storage in a licensed warehouse or Free Trade Zone, or imported under Warehouse Scheme, or under the Approved Trader Scheme. If you are a GST registered trader and have paid GST to Malaysia Customs on your imports, you can claim input tax deduction in your GST returns submitted to the Director General of Custom.'),
(13,'IS',0.00,'P','Imports under special scheme with no GST incurred (e.g. Approved Trader Scheme, ATMS Scheme).','This refers to goods imported under the Approved Trader Scheme (ATS) and Approved Toll Manufacturer Scheme (ATMS), where GST is suspended when the trader imports the non-dutiable goods into Malaysia. These two schemes are designed to ease the cash flow of Trader Scheme (ATS) and Approved Toll Manufacturer Scheme (ATMS), who has significant imports.'),
(14,'BL',6.00,'P','Purchases with GST incurred but not claimable (Disallowance of Input Tax) (e.g. medical expenses for staff).','This refers to GST incurred by a business but GST registered trader is not allowed to claim input tax incurred. The expenses are as following:\r\n\r\nThe supply to or importation of a passenger car;\r\n\r\nThe hiring of passenger car;\r\n\r\nClub subscription fees (including transfer fee) charged by sporting and recreational clubs;\r\n\r\nMedical and personal accident insurance premiums by your staff\r\n\r\nExplanation (cont.) :\r\n\r\nMedical expenses incurred by your staff. Excluding those covered under the provision of the employee’s Social Security Act 1969, Workmen’s Compensation Act 1952 or under any collective agreement under the Industrial Relations Act 1967;\r\nBenefits provided to the family members or relatives of your staff;\r\nEntertainment expenses to a person other than employee and existing customer except entertainment expenses incurred by a person who is in the business of providing entertainment.'),
(15,'NR',0.00,'P','Purchases from non GST-registered supplier with no GST incurred.','This refers to goods and services purchased from non-GST registered supplier/ trader. A supplier / trader who is not registered for GST is not allowed to charge and collect GST. Under the GST model, any unauthorized collection of GST is an offence.'),
(16,'ZP',0.00,'P','Purchases from GST-registered supplier with no GST incurred. (e.g. supplier provides transportation of goods that qualify as international services).','This refers to goods and services purchased from GST registered suppliers where GST is charged at 0%. This is also commonly known as zero-rated purchases. The list as in the Appendix A1 to Budget 2014 Speech.'),
(17,'EP',0.00,'P','Purchases exempted from GST. E.g. purchase of residential property or financial services.','This refers to purchases in relation to residential properties or certain financial services where there no GST was charged (i.e. exempt from GST). Consequently, there is no input tax would be incurred on these supplies. Examples as in Appendix A2 Budget 2014 Speech.'),
(18,'OP',0.00,'P','Purchase transactions which is out of the scope of GST legislation (e.g. purchase of goods overseas).','This refers to purchase of goods outside the scope of GST. An example is purchase of goods overseas and the goods did not come into Malaysia, the purchase of a business transferred as a going concern. For purchase of goods overseas, there may be instances where tax is imposed by a foreign jurisdiction that is similar to GST (e.g. VAT). Nonetheless, the GST registered trader is not allowed to claim input tax for GST/ VAT incurred for such purchases. This is because the input tax is paid to a party outside Malaysia.'),
(19,'X-E43',6.00,'P','Purchases with GST incurred directly attributable to incidental exempt supplies.\r\n\r\n','This is only applicable to GST registered trader (group and ATS only) that makes both taxable and exempt supplies (or commonly known as partially exempt trader). TX-E43 should be used for transactions involving the payment of input tax that is directly attributable to the making Incidental Exempt Supplies. Incidental Exempt Supplies include interest income from deposits placed with a financial institution in Malaysia, realized foreign exchange gains or losses, first issue of bonds, first issue of shares through an Initial Public Offering and interest received from loans provided to employees, factoring receivables, money received from unit holders for units received by a unit trust etc.'),
(20,'X-N43',6.00,'P','Purchases with GST incurred directly attributable to non-incidental exempt supplies.','This is only applicable to GST registered trader that makes both taxable and exempt supplies (or commonly known as partially exempt trader). TX-N43 should be used for transactions involving the payment of input tax that is directly attributable to the making Non-Incidental Exempt Supplies. Example for this tax code are your company bought wall paper for your residential apartment rented to others and purchase costs are already included 6% GST, but you are not eligible to claim the amount of input tax as it would be applied directly to make exempt supply (rental of resident apartment).'),
(21,'X-RE',6.00,'P','Purchases with GST incurred that is not directly attributable to taxable or exempt supplies.','This is only applicable to GST registered trader that makes both taxable and exempt supplies (or commonly known as partially exempt trader). This refers to GST incurred that is not directly attributable to the making of taxable or exempt supplies (or commonly known as residual input tax). Example includes operation over-head for a development of mixed property (properties comprise of residential & commercial).'),
(22,'GP',0.00,'P','Purchase transactions which disregarded under GST legislation (e.g. purchase within GST Group registration).','Purchase within GST group registration, purchase made within a Warehouse Scheme etc.'),
(23,'AJP',6.00,'P','Any adjustment made to Input Tax e.g.: Bad Debt Relief & other input tax adjustment.','Any adjustment made to Input Tax such as bad debt relief & other input tax adjustments.');

UNLOCK TABLES;

/*Table structure for table `tax_zero_rated` */

DROP TABLE IF EXISTS `tax_zero_rated`;

CREATE TABLE `tax_zero_rated` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `tax_zero_rated` */

LOCK TABLES `tax_zero_rated` WRITE;

insert  into `tax_zero_rated`(`id`,`description`) values 
(1,'Livestock which includes cattle, buffalo, selembu, seladang, swine, sheep and goat'),
(2,'Fresh and frozen meat, including offal, of livestock'),
(3,'Live poultry and ducks'),
(4,'Fresh and frozen meat, including offal, of poultry and ducks'),
(5,'Chicken and duck eggs, fresh or salted'),
(6,'All kinds of live, fresh, chilled and frozen fish (except for fish for ornamental use)'),
(7,'Other kinds of water creatures, live, fresh, chilled and frozen such as crustaceans (crabs, prawns, shrimps), molluscs (oysters, mussels, cuttlefish, snails) and others such as sea cucumbers and jellyfish'),
(8,'Fish, dried, salted or in brine, including anchovies (except smoked fish)'),
(9,'Vegetables such as tomatoes, onions, cabbages, radishes, cucumber, leguminous vegetables, asparagus, mushrooms, pumpkins, potatoes, fresh, chilled or in brine'),
(10,'Fresh fruits, local or imported'),
(11,'Spices and herbs such as tamarind, dried chillies, black pepper, cinnamon, cloves, nutmeg and ginger'),
(12,'Coconut including grated coconut'),
(13,'Paddy and rice including glutinous rice, basmathi and brown rice'),
(14,'Wheat flour, attar flour, rice flour, sago flour'),
(15,'Lentils'),
(16,'Granulated sugar including castor sugar, course and fine'),
(17,'Salt'),
(18,'Cooking oil from oil palm, coconut and groundnuts.'),
(19,'Shrimp sauce, fish sauce and shrimp paste'),
(20,'Coffee, tea and cocoa powder'),
(21,'Rice vermicelli, rice noodles, yellow noodles, flat rice noodles'),
(22,'White and wholemeal bread'),
(23,'Baby formula age 0 to 36 months'),
(24,'All types of reading materials and newspapers including student workbooks, encyclopedia, story books, novel, recipe books, reference book in all fields, books related to religion, religious texts and newspapers (except magazines and books that are not reading materials such as diaries, notebooks, log book and accounting books such as journals and ledgers)'),
(25,'Expanded to include 4,215 medicine brands'),
(26,'Goods supplied from mainland Malaysia to Langkawi, Labuan and Tioman'),
(27,'Goods, supplies and spare parts for sea voyages and flights'),
(28,'Treated water for domestic use'),
(29,'Electrical supply for the first 300 units of domestic use'),
(30,'Supply of raw materials and components under the Approved Manufacturers Toll Scheme');

UNLOCK TABLES;

/*Table structure for table `translation` */

DROP TABLE IF EXISTS `translation`;

CREATE TABLE `translation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `english` varchar(255) CHARACTER SET latin1 NOT NULL,
  `translate` varchar(255) DEFAULT NULL,
  `language` varchar(8) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang_tran` (`english`,`language`)
) ENGINE=InnoDB AUTO_INCREMENT=764 DEFAULT CHARSET=utf8;

/*Data for the table `translation` */

LOCK TABLES `translation` WRITE;

insert  into `translation`(`id`,`english`,`translate`,`language`) values 
(1,'Menu','قائمة','ar'),
(2,'Root','جذر','ar'),
(3,'System','النظام ','ar'),
(4,'UNALLOCATED','غير المخصصة','ar'),
(5,'NOTC','ملاحظة','ar'),
(6,'User Management','إدارة المستخدم','ar'),
(7,'Remove Menu','إزالة القائمة','ar'),
(8,'Edit Menu','تعديل القائمة','ar'),
(9,'Hide Menu','إخفاء القائمة','ar'),
(10,'Show Menu','قائمة العرض','ar'),
(11,'Add to Homepage','إضافة إلى الصفحة الرئيسية','ar'),
(12,'Remove from Homepage','حذف من الصفحة الرئيسية','ar'),
(13,'Deactivate Menu','إلغاء تفعيل القائمة','ar'),
(14,'Activate Menu','تفعيل القائمة','ar'),
(15,'Activate Controller','تفعيل المراقب','ar'),
(16,'Deactivate Controller','إلغاء تفعيل المراقب','ar'),
(17,'View Menu','عرض القائمة','ar'),
(18,'Add Menu','إضافة قائمة','ar'),
(19,'Login','دخول','ar'),
(20,'Logout Login','خروج','ar'),
(21,'Vendor Login','دخول البائع','ar'),
(22,'Add User','إضافة مستخدم','ar'),
(23,'List of Users','قائمة المستخدمين','ar'),
(24,'User Privilege','امتيازات المستخدم','ar'),
(25,'List of Roles','قائمة الوظائف','ar'),
(26,'Edit User','تعديل المستخدم','ar'),
(27,'Remove User','حذف المستخدم','ar'),
(28,'New Role','وظيفة جديدة','ar'),
(29,'Edit Role','تعديل الوظيفة','ar'),
(30,'Remove Role','حذف الوظيفة','ar'),
(31,'Home','الصفحة الرئيسية','ar'),
(32,'Profile My','الملف الشخصي','ar'),
(33,'HR','الموارد البشرية','ar'),
(34,'Department','قسم','ar'),
(35,'Add Department','إضافة قسم','ar'),
(36,'Edit Department','تعديل القسم','ar'),
(37,'Erase Department','حذف القسم','ar'),
(38,'Employee','الموظف','ar'),
(39,'Add Employee','إضافة موظف','ar'),
(40,'Edit Employee','تعديل الموظف','ar'),
(41,'Remove Employee','إزالة الموظف','ar'),
(42,'Erase Employee','محو الموظف','ar'),
(43,'File','ملف','ar'),
(44,'Supplier Category','فئة المورد','ar'),
(45,'Add Supplier Category','إضافة فئة المورد','ar'),
(46,'Edit Supplier Category','تعديل فئة المورد','ar'),
(47,'Erase Supplier Category','محو فئة المورد','ar'),
(48,'Supplier','المورد','ar'),
(49,'Add Supplier','إضافة مورد','ar'),
(50,'Edit Supplier','تعديل المورد','ar'),
(51,'Remove Supplier','إزالة المورد','ar'),
(52,'Erase Supplier','محو المورد','ar'),
(53,'Service Category','فئة الخدمة','ar'),
(54,'Add Service Category','إضافة فئة الخدمة','ar'),
(55,'Edit Service Category','تعديل فئة الخدمة','ar'),
(56,'Erase Service Category','محو فئة الخدمة','ar'),
(57,'Service','الخدمة','ar'),
(58,'Add Service','إضافة خدمة','ar'),
(59,'Edit Service','تعديل الخدمة','ar'),
(60,'Erase Service','محو الخدمة','ar'),
(61,'Accounts','الحسابات','ar'),
(62,'Account Category','فئة الحساب','ar'),
(63,'Add Account Category','إضافة فئة الحساب','ar'),
(64,'Edit Account Category','تعديل فئة الحساب','ar'),
(65,'Erase Account Category','محو فئة الحساب','ar'),
(66,'Account','الحساب','ar'),
(67,'Add Account','إضافة حساب','ar'),
(68,'Edit Account','تعديل الحساب','ar'),
(69,'Erase Account','محو الحساب','ar'),
(70,'Expense Category','فئة المصروف','ar'),
(71,'Add Expense Category','إضافة فئة المصروف','ar'),
(72,'Edit Expense Category','تعديل فئة المصروف','ar'),
(73,'Erase Expense Category','محو فئة المصروف','ar'),
(74,'Guest','النزيل','ar'),
(75,'New Guest','نزيل جديد','ar'),
(76,'Edit Guest','تعديل النزيل','ar'),
(77,'Erase Guest','محو النزيل','ar'),
(78,'Agent','الوكيل','ar'),
(79,'Add Agent','إضافة وكيل','ar'),
(80,'Edit Agent','تعديل الوكيل','ar'),
(81,'Erase Agent','محو الوكيل','ar'),
(82,'Operation','العملية','ar'),
(83,'Booking','الحجز','ar'),
(84,'New Booking','حجز جديد','ar'),
(85,'Edit Booking','تعديل الحجز','ar'),
(86,'Erase Booking','محو الحجز','ar'),
(90,'Payment Method','طريقة الدفع','ar'),
(91,'Add Payment Method','إضافة طريقة الدفع','ar'),
(92,'Edit Payment Method','تعديل طريقة الدفع','ar'),
(93,'Erase Payment Method','محو طريقة الدفع','ar'),
(94,'Transaction','عملية','ar'),
(95,'Add Transaction','إضافة عملية','ar'),
(96,'Edit Transaction','تعديل العملية','ar'),
(97,'Remove Transaction','إزالة العملية','ar'),
(98,'Erase Transaction','محو العملية','ar'),
(99,'Temp','قالب','ar'),
(100,'Sample','العينة','ar'),
(101,'Add Sample','إضافة عينة','ar'),
(102,'Edit Sample','تعديل عينة','ar'),
(103,'Remove Sample','إزالة عينة','ar'),
(104,'Erase Sample','محو عينة','ar'),
(105,'Police Station','مركز الشرطة','ar'),
(106,'Add Police Station','إضافة مركز شرطة','ar'),
(107,'Edit Police Station','تعديل مركز الشرطة','ar'),
(108,'Remove Police Station','إزالة مركز الشرطة','ar'),
(109,'Erase Police Station','محو مركز الشرطة','ar'),
(110,'Baggage','الأمتعة','ar'),
(111,'Add Baggage','إضافة أمتعة','ar'),
(112,'Edit Baggage','تعديل الأمتعة','ar'),
(113,'Remove Baggage','إزالة الأمتعة','ar'),
(114,'Erase Baggage','محو الأمتعة','ar'),
(115,'Inter City','مدن داخلية','ar'),
(116,'Add Inter City','إضافة مدينة داخلية','ar'),
(117,'Edit Inter City','تعديل مدينة داخلية','ar'),
(118,'Erase Inter City','محو مدينة داخلية','ar'),
(119,'Finance','المالية','ar'),
(120,'Official Receipt','السند الرسمي ','ar'),
(121,'Add Official Receipt','إضافة سند رسمي','ar'),
(122,'Edit Official Receipt','تعديل السند الرسمي','ar'),
(123,'Erase Official Receipt','محو السند الرسمي','ar'),
(124,'Room','غرفة','ar'),
(125,'Add Room','إضافة غرفة','ar'),
(126,'Edit Room','تعديل الغرفة','ar'),
(127,'Erase Room','محو الغرفة','ar'),
(128,'Activity','النشاط','ar'),
(129,'Add Activity','إضافة نشاط','ar'),
(130,'Edit Activity','تعديل النشاط','ar'),
(131,'Erase Activity','محو النشاط','ar'),
(132,'Payment Voucher','مستند الدفع','ar'),
(133,'Add Payment Voucher','إضافة مستند دفع','ar'),
(134,'Edit Payment Voucher','تعديل مستند الدفع','ar'),
(135,'Erase Payment Voucher','محو مستند الدفع','ar'),
(136,'Expense','المصروف','ar'),
(137,'Add Expense','إضافة مصروف','ar'),
(138,'Edit Expense','تعديل المصروف','ar'),
(139,'Erase Expense','محو المصروف','ar'),
(140,'Taxonomy','التصنيف','ar'),
(142,'Add Taxonomy','إضافة تصنيف','ar'),
(143,'Edit Taxonomy','تعديل التصنيف','ar'),
(144,'Erase Taxonomy','محو التصنيف','ar'),
(145,'Translation','الترجمة','ar'),
(146,'Add Translation','إضافة ترجمة','ar'),
(147,'Edit Translation','تعديل الترجمة','ar'),
(148,'Erase Translation','محو الترجمة','ar'),
(512,'Add','إضافة','ar'),
(513,'Edit','تحرير','ar'),
(514,'Remove','إزالة','ar'),
(515,'Update','تحديث','ar'),
(516,'View','عرض','ar'),
(517,'Booking Details','تفاصيل الحجز','ar'),
(518,'Ticket','تذكرة','ar'),
(520,'Airport Transport','مواصلات المطار','ar'),
(522,'Ferry','عبارة','ar'),
(524,'Room Upgrade','تحديث الغرفة','ar'),
(526,'Dining','الوجبات','ar'),
(527,'Date','التاريخ','ar'),
(528,'Start Date','تاريخ البدء','ar'),
(529,'End Date','تاريخ النهاية','ar'),
(530,'Duration','مدة الإقامة','ar'),
(531,'Number of Adults','عدد البالغين','ar'),
(532,'Number of Children','عدد الأطفال','ar'),
(533,'Number of Infants','عدد الرضع','ar'),
(534,'Requirements','المتطلبات','ar'),
(535,'Budget','الموازنة','ar'),
(536,'Special','خاص','ar'),
(537,'Operator','المشغل','ar'),
(538,'Special Remarks','ملاحظات خاصة','ar'),
(539,'Operator Remarks','ملاحظات المشغل','ar'),
(540,'Initial','استهلال','ar'),
(541,'Attended','حضور','ar'),
(542,'Quotation Sent','تم ارسال عرض السعر','ar'),
(543,'Under Revision','قيد المراجعة','ar'),
(544,'Quotation Resent','إعادة ارسال عرض السعر','ar'),
(545,'Payment Received','الدفعة المستلمة','ar'),
(546,'On-Tour','في جولة','ar'),
(547,'Finish','انهاء','ar'),
(548,'Create Booking','إنشاء حجز جديد','ar'),
(549,'Create Quotation','إنشاء عرض سعر جديد','ar'),
(550,'Review Quotation','مراجعة عرض السعر','ar'),
(551,'Payment','الدفع','ar'),
(552,'On Trip','رحلة قصيرة','ar'),
(554,'Name','الاسم','ar'),
(555,'Code','الرمز','ar'),
(556,'Description','الوصف','ar'),
(557,'Group','المجموعة','ar'),
(558,'Opening Balance','الرصيد الافتتاحي','ar'),
(559,'Parent','الأبوين','ar'),
(560,'Account Category Details','تفاصيل بنود الحساب','ar'),
(561,'Finish!','انهاء','ar'),
(599,'Step 1','خطوة 1','ar'),
(600,'Step 2','خطوة 2','ar'),
(601,'Step 3','خطوة 3','ar'),
(602,'Step 4','خطوة 4','ar'),
(603,'Step 5','خطوة 5','ar'),
(604,'Step 6','خطوة 6','ar'),
(605,'You need to add at least 1 Item to proceed to step 2','تحتاج لإضافة عنصر واحد على الأقل للانتقال للخطوة 2','ar'),
(606,'TOTAL','الإجمالي','ar'),
(607,'Save Draft','حفظ المسودة','ar'),
(608,'Preview','معاينة','ar'),
(609,'Save','حفظ','ar'),
(610,'Accounts Details','تفاصيل الحسابات','ar'),
(611,'Monthly Budget','الموازنة الشهرية','ar'),
(612,'Expense Category Details','تفاصيل بند النفقات','ar'),
(613,'Payment Method Details','تفاصيل طريقة الدفع','ar'),
(614,'Charge Type','نوع الرسوم','ar'),
(615,'Service Charge','نوع الخدمة','ar'),
(616,'GST','الضريبة على السلع والخدمات','ar'),
(617,'Employee Details','تفاصيل الموظف','ar'),
(618,'Date Of Birth','تاريخ الميلاد','ar'),
(619,'ID No','رقم الهوية','ar'),
(620,'Phone','هاتف','ar'),
(621,'Secondary Phone','هاتف آخر','ar'),
(622,'Email','بريد الكتروني','ar'),
(623,'Address','العنوان','ar'),
(624,'Emergency Contact','الاتصال وقت الطوارئ','ar'),
(625,'Contact Phone','هاتف التواصل','ar'),
(626,'Contact Email','بريد الكتروني التواصل','ar'),
(627,'Joining Date','تاريخ الانضمام','ar'),
(628,'Basic Salary','الراتب الاساسي','ar'),
(629,'EPF','قسط التقاعد','ar'),
(630,'SOCSO','الضمان الاجتماعي','ar'),
(631,'PCB','','ar'),
(632,'User','المستخدم','ar'),
(633,'Status','الحالة','ar'),
(634,'Department Details','تفاصيل القسم','ar'),
(635,'Department Head','رئيس القسم','ar'),
(636,'Official Receipt Details','تفاصيل الاستلام الرسمي','ar'),
(637,'Details','التفاصيل','ar'),
(638,'Amount','المبلغ','ar'),
(639,'Payment Remarks','ملاحظات الدفع','ar'),
(640,'Notes','ملاحظات','ar'),
(641,'Payment Voucher Details','تفاصيل قسيمة الدفع','ar'),
(642,'Remarks','ملاحظات','ar'),
(643,'Category','الفئة','ar'),
(644,'Ref No','رقم المرجع','ar'),
(645,'Expense Details','تفاصيل النفقات','ar'),
(646,'City','المدينة','ar'),
(647,'Tour Type','نوع الجولة','ar'),
(648,'Pick Up From','التحرك من','ar'),
(649,'Drop At','النزول في','ar'),
(650,'Start Time','وقت البدء','ar'),
(651,'End Time','وقت الانتهاء','ar'),
(652,'Vehicle Type','نوع المركبة','ar'),
(653,'Activity Details','تفاصيل الأنشطة','ar'),
(654,'Dashboard','لوحة القيادة','ar'),
(655,'Accounting','المحاسبة','ar'),
(656,'Reg No','رقم التسجيل','ar'),
(657,'Country','البلد','ar'),
(658,'Primary Phone','الهاتف الرئيسي','ar'),
(659,'Contact Person','الشخص الذي يمكن التواصل معه','ar'),
(660,'Handphone','الهاتف المحمول','ar'),
(661,'Bank','البنك','ar'),
(662,'Account No','رقم الحساب','ar'),
(663,'Balance','الرصيد','ar'),
(664,'Entry By','الدخول بواسطة','ar'),
(665,'Entry Time','تاريخ الدخول','ar'),
(666,'Modify By','التعديل بواسطة','ar'),
(667,'Modify Time','وقت التعديل','ar'),
(668,'Clean Name','','ar'),
(669,'Show During Init','','ar'),
(670,'Cost','التكلفة','ar'),
(671,'Supplier Category Details','تفاصيل فئة المورد','ar'),
(672,'Trash','المهملات','ar'),
(673,'Supplier Details','تفاصيل المورد','ar'),
(674,'Service Category Details','تفاصيل فئة الخدمة','ar'),
(675,'Service Details','تفاصيل الخدمة','ar'),
(676,'Agent Details','تفاصيل الوكيل','ar'),
(677,'Guest Details','تفاصيل النزيل','ar'),
(678,'Baggage Details','تفاصيل الحقائب','ar'),
(679,'Price','السعر','ar'),
(680,'Inter City Details','تفاصيل المدينة','ar'),
(681,'Pick Up Time','وقت التحرك','ar'),
(682,'Drop Time','وقت النزول','ar'),
(683,'Room Details','تفاصيل الغرفة','ar'),
(684,'Hotel','الفندق','ar'),
(685,'Check In Date','تاريخ الوصول','ar'),
(686,'Check Out Date','تاريخ المغادرة','ar'),
(687,'Room Type','نوع الغرفة','ar'),
(688,'Number Of Room','رقم الغرفة','ar'),
(689,'Cost Per Room','تكلفة الغرفة','ar'),
(690,'Price Per Room','سعر الغرفة','ar'),
(691,'Breakfast','الفطور','ar'),
(692,'Wifi','واي فاي','ar'),
(693,'Smoking','تدخين','ar'),
(694,'Special Request','طلبات خاصة','ar'),
(695,'Setup','تثبيت','ar'),
(696,'State','الدولة','ar'),
(697,'Airports','المطارات','ar'),
(698,'Short Name','الاسم المختصر','ar'),
(699,'Currency','العملة','ar'),
(700,'Currency Sign','علامة العملة','ar'),
(701,'Country Details','تفاصيل العملة','ar'),
(702,'City Details','تفاصيل المدينة','ar'),
(703,'State Details','تفاصيل الدولة','ar'),
(704,'Employee Salary','راتب الموظف','ar'),
(705,'Employee Advance','سلفة الموظف','ar'),
(706,'Month','الشهر','ar'),
(707,'Year','السنة','ar'),
(708,'Basic','أساسي','ar'),
(709,'Allowance','بدلات','ar'),
(710,'Overtime','اجر اضافي','ar'),
(711,'Gross Salary','اجمالي الراتب','ar'),
(712,'Net Payment','صافي الدفع','ar'),
(713,'Advance','دفعة مقدمة','ar'),
(714,'Return Date','تاريخ العودة','ar'),
(715,'Employee Advance Details','تفاصيل سلفة الموظف','ar'),
(716,'Cheque Clearance Date','تاريخ شيك التصفية','ar'),
(717,'Cheque Cleared Date','تاريخ شيك التصفية','ar'),
(718,'Cheque Status','حالة الشيك','ar'),
(719,'Payment Reference','مرجع الدفع','ar'),
(720,'Employee Salary Details','تفاصيل راتب الموظف','ar'),
(721,'Unauthorised Absence','غياب غير مصرح به','ar'),
(722,'Net Salary','صافي الراتب','ar'),
(723,'Exempt Allowance','بدل الاعفاء','ar'),
(724,'Check Cleared Date','التحقق من تاريخ التصفية','ar'),
(725,'Place Caregory','فئة المكان','ar'),
(726,'Place','المكان','ar'),
(727,'Vehicle Category','فئة المركبة','ar'),
(728,'Vehicle','المركبة','ar'),
(729,'Driver','السائق','ar'),
(730,'Transportation Pricing','أسعار المواصلات','ar'),
(731,'From City','من مدينة','ar'),
(732,'From Place Category','من فئة المكان','ar'),
(733,'From Place','من مكان','ar'),
(734,'To City','إلى مدينة','ar'),
(735,'To Place Category','إلى فئة المكان','ar'),
(736,'To Place','فئة المكان','ar'),
(737,'Internal Cost','التكلفة الداخلية','ar'),
(738,'External Cost','التكلفة الخارجية','ar'),
(739,'Company Cost','تكلفة الشركة','ar'),
(740,'Other Cost','تكاليف أخرى','ar'),
(741,'Normal Price','السعر العادي','ar'),
(742,'Off Season Price','سعر غير الموسم','ar'),
(743,'Season Price','سعر الموسم','ar'),
(744,'Transportation Pricing Details','تفاصيل اسعار المواصلات','ar'),
(745,'Emergency Phone','هاتف للطوارئ','ar'),
(746,'Monthly Salary','الراتب الشهري','ar'),
(747,'Hourly Salary','اجر الساعة','ar'),
(748,'Per Trip Salary','اجر كل جولة','ar'),
(749,'Fixed Allowance','بدل ثابت','ar'),
(750,'Plate No','رقم اللوحة','ar'),
(751,'Driver Details','تفاصيل السائق','ar'),
(752,'Vehicle Details','تفاصيل المركبة','ar'),
(753,'Vehicle Category Details','تفاصيل فئة المركبة','ar'),
(754,'Place Details','تفاصيل المكان','ar'),
(755,'Place Caregory Details','تفاصيل فئة المكان','ar'),
(756,'User Profile','ملف المستخدم','ar'),
(757,'Settings','الاعدادات','ar'),
(758,'Logout','خروج','ar'),
(759,'Place Category','فئة المكان','ar'),
(760,'Hotel Room Type','','ar'),
(761,'Holiday','','ar'),
(762,'Pricing Management','','ar'),
(763,'Hotel Room Pricing','','ar');

UNLOCK TABLES;

/*Table structure for table `sys_acls` */

DROP TABLE IF EXISTS `sys_acls`;

/*!50001 DROP VIEW IF EXISTS `sys_acls` */;
/*!50001 DROP TABLE IF EXISTS `sys_acls` */;

/*!50001 CREATE TABLE  `sys_acls`(
 `privilege` int(11) ,
 `link` varchar(64) ,
 `access` tinyint(4) ,
 `utype` varchar(1) ,
 `user` int(11) 
)*/;

/*Table structure for table `sys_permission` */

DROP TABLE IF EXISTS `sys_permission`;

/*!50001 DROP VIEW IF EXISTS `sys_permission` */;
/*!50001 DROP TABLE IF EXISTS `sys_permission` */;

/*!50001 CREATE TABLE  `sys_permission`(
 `id` int(11) ,
 `privilege` int(11) ,
 `link` varchar(64) ,
 `icon` varchar(128) ,
 `title` varchar(32) ,
 `position` int(2) ,
 `option` varchar(24) ,
 `root` int(2) ,
 `active` tinyint(1) ,
 `hidden` tinyint(1) unsigned ,
 `access` tinyint(4) ,
 `user` int(11) ,
 `show_in_frontpage` tinyint(1) ,
 `module` varchar(64) ,
 `glyphicon` varchar(64) ,
 `target` enum('_blank','_self','_parent','_top') 
)*/;

/*Table structure for table `sys_privileges` */

DROP TABLE IF EXISTS `sys_privileges`;

/*!50001 DROP VIEW IF EXISTS `sys_privileges` */;
/*!50001 DROP TABLE IF EXISTS `sys_privileges` */;

/*!50001 CREATE TABLE  `sys_privileges`(
 `gid` int(3) unsigned ,
 `gtitle` varchar(32) ,
 `pid` int(3) unsigned ,
 `position` int(2) ,
 `ptitle` varchar(32) ,
 `icon` varchar(128) ,
 `option` varchar(24) ,
 `link` varchar(64) 
)*/;

/*Table structure for table `sys_users_roles` */

DROP TABLE IF EXISTS `sys_users_roles`;

/*!50001 DROP VIEW IF EXISTS `sys_users_roles` */;
/*!50001 DROP TABLE IF EXISTS `sys_users_roles` */;

/*!50001 CREATE TABLE  `sys_users_roles`(
 `uid` int(10) unsigned ,
 `username` varchar(64) ,
 `rid` int(2) unsigned ,
 `name` varchar(32) 
)*/;

/*View structure for view sys_acls */

/*!50001 DROP TABLE IF EXISTS `sys_acls` */;
/*!50001 DROP VIEW IF EXISTS `sys_acls` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `sys_acls` AS select `a`.`privilege` AS `privilege`,`p`.`link` AS `link`,`a`.`access` AS `access`,`a`.`utype` AS `utype`,`a`.`appliesto` AS `user` from (`sys_acl` `a` join `sys_privilege` `p`) where ((`a`.`privilege` = `p`.`id`) and (`a`.`utype` = _latin1'u')) union select `a`.`privilege` AS `privilege`,`p`.`link` AS `link`,`a`.`access` AS `access`,`a`.`utype` AS `utype`,`r`.`ur_user_id` AS `user` from ((`sys_acl` `a` join `sys_privilege` `p`) join `sys_user_role` `r`) where ((`a`.`privilege` = `p`.`id`) and (`r`.`ur_role_id` = `a`.`appliesto`) and (`a`.`utype` = _latin1'r')) */;

/*View structure for view sys_permission */

/*!50001 DROP TABLE IF EXISTS `sys_permission` */;
/*!50001 DROP VIEW IF EXISTS `sys_permission` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `sys_permission` AS select `a`.`privilege` AS `id`,`a`.`privilege` AS `privilege`,`p`.`link` AS `link`,`p`.`icon` AS `icon`,`p`.`title` AS `title`,`p`.`position` AS `position`,`p`.`option` AS `option`,`p`.`root` AS `root`,`p`.`active` AS `active`,`p`.`hidden` AS `hidden`,`a`.`access` AS `access`,`a`.`user` AS `user`,`p`.`show_in_frontpage` AS `show_in_frontpage`,`p`.`module` AS `module`,`p`.`glyphicon` AS `glyphicon`,`p`.`target` AS `target` from (`sys_acls` `a` join `sys_privilege` `p`) where (`a`.`privilege` = `p`.`id`) */;

/*View structure for view sys_privileges */

/*!50001 DROP TABLE IF EXISTS `sys_privileges` */;
/*!50001 DROP VIEW IF EXISTS `sys_privileges` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `sys_privileges` AS (select `g`.`id` AS `gid`,`g`.`title` AS `gtitle`,`p`.`id` AS `pid`,`p`.`position` AS `position`,`p`.`title` AS `ptitle`,`p`.`icon` AS `icon`,`p`.`option` AS `option`,`p`.`link` AS `link` from (`sys_privilege` `g` left join `sys_privilege` `p` on((`g`.`id` = `p`.`root`))) where ((`g`.`root` = 0) and (`g`.`active` = 1) and (`p`.`active` = 1)) order by `g`.`position`,`g`.`title`,`p`.`link`,`p`.`position`) */;

/*View structure for view sys_users_roles */

/*!50001 DROP TABLE IF EXISTS `sys_users_roles` */;
/*!50001 DROP VIEW IF EXISTS `sys_users_roles` */;

/*!50001 CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `sys_users_roles` AS (select `u`.`id` AS `uid`,`u`.`u_username` AS `username`,`r`.`id` AS `rid`,`r`.`r_name` AS `name` from ((`sys_user` `u` join `sys_user_role` `ur`) join `sys_role` `r`) where ((`u`.`id` = `ur`.`ur_user_id`) and (`r`.`id` = `ur`.`ur_role_id`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
