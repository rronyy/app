<?php
if(!loggedin()):
	$controller = "login";
endif;
if($controller!='home'):
	if(isset($module)):
		$file = "modules/$module/index";
	elseif(isset($app_module)):
		$file = "modules/$app_module/index";
	else:
		$file = "controller/$controller";
	endif;
	
	include "content_header.php";
	if(file_exists("$file.php")){
		if(hasAccess($controller, $function) || $controller=='login' || $controller=='logout'){
			include "$file.php";
		} else{
			print "<div class='alert alert-danger' role='alert'>ACCESS DENIED!, PLEASE CONTACT ADMINISTRATOR!!!</div>";
		}
	} else{
		print "<div class='alert alert-warning' role='alert'>RESOURCE NOT FOUND!</div>";
	}
	
	include "content_footer.php";
else:
	include "home.php";
endif;
?>