<?php require 'includes.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php print APPTITLE; ?></title>
    <style media="screen">
        #canvas{
            display: none;
        }
        .container-fluid {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
    </style>
    <!-- Bootstrap Core CSS -->
    <?php
        $cssfiles = array("core/vendor/bootstrap/css/bootstrap.min.css",
            "themes/fb/bower_components/metisMenu/dist/metisMenu.min.css",
            "themes/fb/dist/css/timeline.css",
            "themes/fb/dist/css/fb.css",
            "themes/fb/bower_components/morrisjs/morris.css",
            "themes/fb/bower_components/font-awesome/css/font-awesome.min.css",
            "core/vendor/bootstrap/css/bootstrap-select.min.css",
            "core/vendor/jqueryplugins/jquery.qtip.min.css",
            "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
            "core/vendor/fontawesome/font-awesome-animation.min.css",
            "core/vendor/panacea/css/styles.css");
        foreach ($cssfiles as $cssfile) {
            print "<link href='".BASEURL."/framework/$cssfile' rel='stylesheet'>\n";
        }

        if(file_exists("assets/css/".theme().".css")){
            print "<link href='".BASEURL."/framework/assets/css/".theme().".css' rel='stylesheet'>";
        }
    ?>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <?php
        $jsfiles = array("core/vendor/jquery/jquery.min.js",
            "themes/fb/bower_components/datatables/media/js/jquery.dataTables.min.js",
            "themes/fb/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
            "core/vendor/jqueryplugins/jquery.qtip.min.js",
            "core/vendor/jqueryplugins/jquery.cookie.js",
            "core/vendor/jqueryplugins/jquery.validate.min.js",
            "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
            "core/vendor/panacea/js/scripts.js",
            "core/vendor/bootstrap/js/bootstrap-select.min.js",
            "core/vendor/bootstrap/js/bootstrap-typeahead.js",
            "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js");
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
        }
    ?>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
</head>

<body>
<div id="wrapper">
 <div class="box">
  <div class="row row-offcanvas row-offcanvas-left">
    <?php if(loggedin()): ?>
		<?php require 'nav.php'; ?>
		<?php require 'content.php';?>
    <?php elseif (isset($module)): require "modules/$module/index.php"; ?>
    <?php else: 
            $app_mod = new IO("modules");
            if($app_mod->exists("login")){
                require 'modules/login/login.php'; 
            }
            else{
                require 'framework/modules/login/login.php'; 
            }
    ?>
    <?php endif; ?>
  </div> <!-- /row-offcanvas -->
 </div> <!-- /box -->
</div>
	<!-- /#wrapper -->

	<?php
        $jsfiles = array("core/vendor/bootstrap/js/bootstrap.min.js",
            "themes/fb/bower_components/metisMenu/dist/metisMenu.min.js");
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
        }
        if($controller=='home'){
            $jsfiles = array("themes/fb/bower_components/raphael/raphael-min.js",
                "themes/fb/bower_components/morrisjs/morris.min.js",
                //"themes/fb/js/morris-data.js",
                "themes/fb/dist/js/fb.js");
            foreach ($jsfiles as $jsfile) {
                print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
            }
        }
        $jsfiles = array("themes/fb/dist/js/fb.js");
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
        }

    ?>


    <script>
        $(document).ready(function() {
            $('.dataTables').DataTable({
                responsive: true
            });
            $(".pdf").height($(".content-wrapper").height()+15);
            $(".pdf").width($(".content-wrapper").width()+15);
        });
    </script>
</body>
</html>
