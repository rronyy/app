<?php
  $filename = "temps/menus/menu_".uid().".php";
  $div = "<ul id='nav'>";
  if(!file_exists($filename)){
    $root_menus = select("*", "sys_privilege", "root=0", "ORDER BY position");
    while($root_menu = mysqli_fetch_object($root_menus)){
      $menus = select("DISTINCT module, `link`, `option`, `title`, `icon`", uid()=="1"?"sys_privilege":"sys_permission", "active=1 AND hidden=0 AND root=$root_menu->id ".(uid()=="1"?"":"AND user=".uid())." AND title IS NOT NULL ORDER BY position");
      if($menus->num_rows){
        $div .= "<li><a href='#' id='".str_replace(" ", "", $root_menu->name)."'>$root_menu->name</a>";
        $div .= "<ul>";
        $i = 1;
        while($menu=mysqli_fetch_object($menus)){
          $div .= "<li> <a href='".BASEURL."/framework/".(nn($menu->module)?"$menu->module/":"")."$menu->link"."/$menu->option'>$menu->title</a></li>";
          $div .= $i++==$menus->num_rows?"<br />":"<hr />";

        }
        $div .= "</ul></li>";
      }
      //
    }
    //file_put_contents($filename, $div);
  }
  $div .= "</ul>";
  //include($filename);
  echo $div;
 ?>