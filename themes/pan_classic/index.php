<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php print APPTITLE; ?></title>
    <!-- Bootstrap Core CSS -->
    <?php
        $cssfiles = ["core/vendor/bootstrap/css/bootstrap.min.css",
                    "core/vendor/jqueryplugins/jquery.qtip.min.css",
                    "themes/pan_classic/assets/style.css",
                    "themes/bootdesk/assets/styles.css",
                    "core/vendor/panacea/css/styles.css"];
        foreach ($cssfiles as $cssfile) {
            print "<link href='".BASEURL."/framework/$cssfile' rel='stylesheet'>\n";
        }

        if(file_exists("assets/css/".theme().".css")){
            print "<link href='".BASEURL."/framework/assets/css/".theme().".css' rel='stylesheet'>";
        }
    ?>
    <?php
        $jsfiles = ["core/vendor/jquery/jquery.min.js",
            "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
            "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
            "core/vendor/jqueryplugins/jquery.qtip.min.js",
            "core/vendor/jqueryplugins/jquery.cookie.js",
            "core/vendor/jqueryplugins/jquery.validate.min.js",
            "core/vendor/panacea/js/scripts.js",
            "themes/bootdesk/assets/scripts.js"];
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
        }
    ?>
  </head>
  <body>
    <?php if(loggedin()): ?>
    <div id="header">
    <div id="menu-wrapper">

    <?php require 'nav.php'; ?>

        </div>
        <div id="search-wrapper">
          <input type="text" name="key" id="search" onfocus="search(this.value)" onkeyup="search(this.value)" value="" size="20" />
            <div></div>
        </div>
    </div>

    <div id="controls-wrapper">
      <div id="title" style="padding-left:60px">Sylvarina Skyline Travel & Tours</div>
      <div id="page-title"><div>Create Refund</div></div>
      <div id="controls">
          <a href='.'><img src='<?php print BASEDIR; ?>/ico/home.png' width="24px" alt="home" /></a>
            <a href='javascript:jprint()'><img src='<?php print BASEDIR; ?>/ico/print.png' width="24px" alt="print" /></a>
            <a href='javascript:excel()'><img src='<?php print BASEDIR; ?>/ico/excel.png' width="24px" alt="excel" /></a>
            <a href='javascript:pdf()'><img src='<?php print BASEDIR; ?>/ico/pdf.png' width="24px" alt="pdf" /></a>
            <a href='logout'><img src='<?php print BASEDIR; ?>/ico/exit.png' width="24px" alt="logout" /></a>

        </div>
    </div>


    <div id="content-wrapper" class="cm">
      <div id="content">

        <?php require 'content.php';?>
      </div>
    </div>
    <?php elseif (isset($module)): require "modules/$module/index.php"; ?>
    <?php else: require 'modules/login/login.php'; ?>
    <?php endif; ?>
    <!-- Javascripts -->
    <?php
      if($controller=='home'){
          $jsfiles = ["themes/sbadmin/bower_components/raphael/raphael-min.js",
              "themes/sbadmin/bower_components/morrisjs/morris.min.js",
              //"themes/sbadmin/js/morris-data.js",
              ];
          foreach ($jsfiles as $jsfile) {
              print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
          }
      }
      $jsfiles = [];
      foreach ($jsfiles as $jsfile) {
          print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
      }
    ?>
    <script type="text/javascript">

    </script>
    <!-- END Javascripts -->
  </body>
</html>
