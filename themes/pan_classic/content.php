<?php
if(!loggedin()):
	$controller = "login";
endif;
if($controller!='home'):
	if(isset($module)){
		$file = "modules/$module/index";
	} else{
		$file = "controller/$controller";
	}
	
	if(file_exists("$file.php")){
		if(hasAccess($controller, $function) || $controller=='login' || $controller=='logout'){
			include "$file.php";
		} else{
			print "<div class='alert alert-danger' role='alert'>ACCESS DENIED!, PLEASE CONTACT ADMINISTRATOR!</div>";
		}
	} else{
		print "<div class='alert alert-warning' role='alert'>RESOURCE NOT FOUND!</div>";
	}
	
else:
	//include "themes/home.php";
endif;
