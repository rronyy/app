<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Welcome to PACIFIC INTER-LINK :: Your Partner in Success</title>
	<link rel="icon" href="favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<?php
		$css_files = [
				"core/vendor/jquery/jquery-ui/jquery-ui.min.css",
        "core/vendor/bootstrap/css/bootstrap.min.css",
        "themes/sbadmin/bower_components/font-awesome/css/font-awesome.min.css",
        "core/vendor/bootstrap/css/bootstrap-select.min.css",
        "core/vendor/jqueryplugins/jquery.qtip.min.css",
        "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
        "core/vendor/fontawesome/font-awesome-animation.min.css",
        "core/vendor/owlcarousel/dist/assets/owl.carousel.min.css",
        "core/vendor/owlcarousel/dist/assets/owl.theme.default.min.css",
        "core/vendor/animate.css/animate.min.css",
        "core/vendor/magic/magic.min.css",
        "core/vendor/csshake/dist/csshake.min.css",
        "core/vendor/panacea/css/styles.css",
        "core/vendor/ionicons/css/ionicons.min.css",
        "core/vendor/loading-bar/dist/loading-bar.css",
        'vendor/lightbox2/src/css/lightbox.css',
        "themes/pil2/css/custom.css"
      ];
		foreach ($css_files as $css_file) {
			print '<link rel="stylesheet" type="text/css" href="'.$appurl.'/framework/'.$css_file.'">'.PHP_EOL;
		}
		if(file_exists("assets/css/".theme().".css")){
      print "<link href='assets/css/".theme().".css' rel='stylesheet'>".PHP_EOL;
    }

		$js_files = [
				"core/vendor/jquery/jquery.min.js",
        "core/vendor/jquery/jquery-ui.min.js",
        "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
        "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
        "core/vendor/jqueryplugins/jquery.qtip.min.js",
        "core/vendor/jqueryplugins/jquery.cookie.js",
        "core/vendor/jqueryplugins/jquery.validate.min.js",
        "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
        "core/vendor/panacea/js/scripts.js",
        "core/vendor/bootstrap/js/bootstrap-select.min.js",
        "core/vendor/bootstrap/js/bootstrap-typeahead.js",
        "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js",
        "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js","core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js",
        "core/vendor/waypoints/lib/jquery.waypoints.min.js",
        "themes/pil2/js/custom.js",
        "core/vendor/highcharts/highcharts.js",
        "core/vendor/loading-bar/dist/loading-bar.min.js",
        "core/vendor/highcharts/exporting.js",
        "core/vendor/scrollify/jquery.scrollify.min.js",
        "vendor/lightbox2/src/js/lightbox.js",
        // "core/vendor/zeptojs/zepto.min.js",
        // "core/vendor/zepto-onepage-scroll/zepto.onepagescroll.min.js"
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.$appurl.'/framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
	<style type="text/css">
		/* The Overlay (background) */
		.overlay {
		    /* Height & width depends on how you want to reveal the overlay (see JS below) */   
		    height: 100%;
		    width: 0;
		    position: fixed; /* Stay in place */
		    z-index: 1; /* Sit on top */
		    left: 0;
		    top: 0;
		    background-color: rgb(0,0,0); /* Black fallback color */
		    background-color: rgba(0,0,0, 0.9); /* Black w/opacity */
		    overflow-x: hidden; /* Disable horizontal scroll */
		    transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
		}

		/* Position the content inside the overlay */
		.overlay-content {
		    position: relative;
		    top: 25%; /* 25% from the top */
		    width: 100%; /* 100% width */
		    text-align: center; /* Centered text/links */
		    margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
		}

		/* The navigation links inside the overlay */
		.overlay a {
		    padding: 8px;
		    text-decoration: none;
		    font-size: 36px;
		    color: #818181;
		    display: block; /* Display block instead of inline */
		    transition: 0.3s; /* Transition effects on hover (color) */
		}

		/* When you mouse over the navigation links, change their color */
		.overlay a:hover, .overlay a:focus {
		    color: #f1f1f1;
		}

		/* Position the close button (top right corner) */
		.overlay .closebtn {
		    position: absolute;
		    bottom: 20px;
		    right: 45px;
		    font-size: 60px;
		}

		.overlay-content{
			color: #fff;
		}
		.overlay-content td{
			padding: 5px;
		}

		/* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
		@media screen and (max-height: 450px) {
		    .overlay a {font-size: 20px}
		    .overlay .closebtn {
		        font-size: 40px;
		        top: 15px;
		        right: 35px;
		    }
		}
	</style>
	<script>
		<?php if(in_array($controller, ['home', 'sustainability', 'contact'])) { ?>
		$(function() {
			// $.scrollify({
			// 	section : "section",
			// });
		});
		<?php } ?>
	</script>
</head>
<body oncontextmenu="return true;">	
	<nav id="header" class="navbar navbar-fixed-top">
    <div id="header-container" class="container navbar-container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php print $controller!='home'?'home':''; ?>#home" id="brand" class="navbar-brand"><img src="assets/logo.png" alt='Pil' id="homelogo"></a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
		          <li><a class='uppercase' href="<?php print $controller!='home'?'home':''; ?>#home">Home</a></li>
		          <li class="nav-item dropdown">
		          	<a class='uppercase dropdown-toggle' href="about">About <i class="fa fa-chevron-down"></i></a>
		          	<ul class="dropdown-menu w200" aria-labelledby="navbarDropdownMenuLink">
				          <li><a class="dropdown-item" href="about?sec=intro#start">Who we are</a></li>
									<li><a class="dropdown-item" href="about?sec=value#start">Our Belief</a></li>
									<li><a class="dropdown-item" href="about?sec=philosophy#start">Our Philosophy</a></li>
									<li><a class="dropdown-item" href="about?sec=wedo#start">What we do</a></li>
									<li><a class="dropdown-item" href="about?sec=where#start">Where we do </a></li>
									<li><a class="dropdown-item" href="about?sec=memberships#start">Memberships</a></li>
									<li><a class="dropdown-item" href="about#quality">Quality Policy</a></li>
									<li><a class="dropdown-item" href="about#vision">Vision & Mission</a></li>
									<li><a class="dropdown-item" href="about#msg">Management</a></li>
									<li><a class="dropdown-item" href="about#conduct">Code of Conduct</a></li>
		          	</ul>
		          </li>
		          <li class="nav-item dropdown">
		          	<a class="uppercase dropdown-toggle" href="product">Products & Facilites <i class="fa fa-chevron-down"></i></a>
		          	<ul class="dropdown-menu w200" aria-labelledby="navbarDropdownMenuLink">
				          <li><a class="dropdown-item" href="product#bulk">Bulk Palm Oil</a></li>
				          <li><a class="dropdown-item" href="product#oil">Packed Oil</a></li>
				          <li><a class="dropdown-item" href="product#soap">Soap</a></li>
				          <li><a class="dropdown-item" href="product#detergent">Detergent</a></li>
				          <li><a class="dropdown-item" href="product#dairy">Dairy</a></li>
				        </ul>
		          </li>
		          <li class="nav-item dropdown">
		          	<a class="uppercase dropdown-toggle" href="sustainability">Sustainability <i class="fa fa-chevron-down"></i></a>
		          	<ul class="dropdown-menu w200" aria-labelledby="navbarDropdownMenuLink">
		          			<li><a class="dropdown-item" href="sustainability#approach">PIL Group Approach</a></li>
				          <li><a class="dropdown-item" href="sustainability#strategy">Strategy 6 R<span style='text-transform:lowercase;'>s</span></a></li>
				          <li><a class="dropdown-item" href="sustainability#draft">Darft Policy</a></li>
				          <li><a class="dropdown-item" href="sustainability#init">Initiatives</a></li>
				        </ul>
		          </li>
		          <li><a class='uppercase' href="csr" title='Corporate Social Responsibility'>CSR</a></li>
		          <li class="nav-item dropdown">
		          	<a class="uppercase dropdown-toggle" href="gallery">Media <i class="fa fa-chevron-down"></i></a>
		          	<ul class="dropdown-menu w200" aria-labelledby="navbarDropdownMenuLink">
				          <li><a class="dropdown-item" href="video">Video</a></li>
				          <li><a class="dropdown-item" href="gallery">Gallery</a></li>
				        </ul>
		          </li>
		          <li><a class='uppercase' href="contact">Contact</a></li>
		          <li><a class='uppercase active' href="#">Login</a></li>
		        </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </nav><!-- /.navbar -->

<?php 
	if(file_exists("$controller.php")){
		require "$controller.php";
	}	else{
		require "404.php";
	}
?>
	<!-- <div class='separator'></div> -->
	<footer>
	<!-- <section> -->
	<div class="top-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block" style="margin-top: 68px;">
					<h3>Pacific Inter-Link</h3>
					<p>Incorporated in 1988 in Kuala Lumpur to promote the export of various commodities and services from Malaysia and the Far East to the Global Markets.</p>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block" style="margin-top: 68px;">
					<h3>Our Contacts</h3>
					<div>
						<div>31st Floor, Menara Dato' Onn </div>
						<div>Putra World Trade Centre 45</div>
						<div>Jalan Tun Ismail</div>
						<div>50480 Kuala Lumpur </div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block" style="margin-top: 68px;">					
					<h3>Reach Us</h3>
						<div><i class="fa fa-phone"></i> <a href='tel:+603 4042 3933'>+603 4042 3933, 4041 3939, 4042 8088 </a></div>
						<div><i class="fa fa-fax"></i> <a href='tel:+603 2166 0418'>+603 2166 0418</a></div>
						<div><i class="fa fa-envelope-open-o"></i> <a href='mailto:info@pilgroup.com '>info@pilgroup.com </a></div>
						<div><i class="fa fa-link"></i> <a href="https://secure.ethicspoint.eu/domain/media/en/gui/102478/index.html" target="_blank">Ethical Hotline</a></div>
						<div><i class="fa fa-globe"></i> <a href='http://www.pilgroup.com/'>www.pilgroup.com</a></div>
						<div><i class="fa fa-globe"></i> <a href='http://www.hsagroup.com/'>www.hsagroup.com</a></div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block" style="margin-top: 68px;">
					<div class="social-network">
						<h3>Social Network</h3>
						<ul class="clearfix">
							<li class='social-icon'><a href="https://www.youtube.com/channel/UCe9GUN-7iJWCebQiutwNmoQ" target="_blank"><i class='fa fa-youtube'></i></a></li>
							<li class='social-icon'><a href="https://twitter.com/Pilgroupmy" target="_blank"><i class='fa fa-twitter'></i></a></li>
							<li class='social-icon'><a href="https://www.instagram.com/pilgroupmy/" target="_blank"><i class='fa fa-instagram'></i></a></li>
							<li class='social-icon'><a href="https://www.linkedin.com/company/pacific-inter-link-sdn-bhd" target="_blank"><i class='fa fa-linkedin'></i></a></li>
							<li class='social-icon'><a href="https://www.facebook.com/pilgroupmy/" target="_blank"><i class='fa fa-facebook'></i></a></li>
						</ul>
					</div>
					<br clear="all">
					<br clear="all">
					<br clear="all">
					<div class='center'><a href='javascript:openNav()' class='btn btn-warning white'><b class='white'>SUBSCRIBE</b></a></div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p class="copyright center">
						<br>
						All rights reserved. ©2009 - <?php print date('Y', time());?> Copyright pilgroup.com, owned and operated by Pacific Inter-Link.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- </section> -->
	</footer>

	<?php
		$js_files = [
				"core/vendor/bootstrap/js/bootstrap.min.js",
        "core/vendor/owlcarousel/dist/owl.carousel.min.js",
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.$appurl.'/framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
	<div id="myNav" class="overlay">

	  <!-- Button to close the overlay navigation -->
	  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

	  <!-- Overlay content -->
	  <div class="overlay-content">
	    <form>
	    	<table align="center">
	    		<tr><td>Agency Name</td><td><input type="email" class="form-control" ></td></tr>
	    		<tr><td>Fullname</td><td><input class="form-control" ></td></tr>
	    		<tr><td>Email</td><td><input type="email" class="form-control" ></td></tr>
	    		<tr><td>Mobile</td><td><input type="email" class="form-control" ></td></tr>
	    		<tr><td></td><td></td></tr>
	    		<tr><td></td><td><button class="btn btn-danger">Subscribe</button></td></tr>
			  
			</form>
	  </div>

	</div>
	<script type="text/javascript">
		function openNav() {
			document.getElementById("myNav").style.width = "100%";
		}

		/* Close when someone clicks on the "x" symbol inside the overlay */
		function closeNav() {
			document.getElementById("myNav").style.width = "0%";
		}
	</script>
</body>
</html>