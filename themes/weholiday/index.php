<!DOCTYPE html>
<html>
<head>
	<title>Within Earth Holiday</title>
	<link rel="icon" href="favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<?php
		$css_files = [
				"core/vendor/jquery/jquery-ui/jquery-ui.min.css",
        "core/vendor/bootstrap/css/bootstrap.min.css",
        "themes/sbadmin/bower_components/font-awesome/css/font-awesome.min.css",
        "core/vendor/bootstrap/css/bootstrap-select.min.css",
        "core/vendor/jqueryplugins/jquery.qtip.min.css",
        "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
        "core/vendor/fontawesome/font-awesome-animation.min.css",
        "core/vendor/owlcarousel/dist/assets/owl.carousel.min.css",
        "core/vendor/owlcarousel/dist/assets/owl.theme.default.min.css",
        "core/vendor/animate.css/animate.min.css",
        "core/vendor/magic/magic.min.css",
        "core/vendor/csshake/dist/csshake.min.css",
        "core/vendor/panacea/css/styles.css",
        "core/vendor/ionicons/css/ionicons.min.css",
        "core/vendor/loading-bar/dist/loading-bar.css",
        "themes/weholiday/css/custom.css"
      ];
		foreach ($css_files as $css_file) {
			print '<link rel="stylesheet" type="text/css" href="'.BASEURL.'framework/'.$css_file.'">'.PHP_EOL;
		}
		if(file_exists("assets/css/".theme().".css")){
      print "<link href='assets/css/".theme().".css' rel='stylesheet'>".PHP_EOL;
    }

		$js_files = [
				"core/vendor/jquery/jquery.min.js",
        "core/vendor/jquery/jquery-ui.min.js",
        "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
        "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
        "core/vendor/jqueryplugins/jquery.qtip.min.js",
        "core/vendor/jqueryplugins/jquery.cookie.js",
        "core/vendor/jqueryplugins/jquery.validate.min.js",
        "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
        "core/vendor/panacea/js/scripts.js",
        "core/vendor/bootstrap/js/bootstrap-select.min.js",
        "core/vendor/bootstrap/js/bootstrap-typeahead.js",
        "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js",
        "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js","core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js",
        "core/vendor/waypoints/lib/jquery.waypoints.min.js",
        "themes/weholiday/js/custom.js",
        "core/vendor/highcharts/highcharts.js",
        "core/vendor/loading-bar/dist/loading-bar.min.js",
        "core/vendor/highcharts/exporting.js"
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.BASEURL.'framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
	<style type="text/css">
		/* The Overlay (background) */
		.overlay {
		    /* Height & width depends on how you want to reveal the overlay (see JS below) */   
		    height: 100%;
		    width: 0;
		    position: fixed; /* Stay in place */
		    z-index: 1; /* Sit on top */
		    left: 0;
		    top: 0;
		    background-color: rgb(0,0,0); /* Black fallback color */
		    background-color: rgba(0,0,0, 0.9); /* Black w/opacity */
		    overflow-x: hidden; /* Disable horizontal scroll */
		    transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
		}

		/* Position the content inside the overlay */
		.overlay-content {
		    position: relative;
		    top: 25%; /* 25% from the top */
		    width: 100%; /* 100% width */
		    text-align: center; /* Centered text/links */
		    margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
		}

		/* The navigation links inside the overlay */
		.overlay a {
		    padding: 8px;
		    text-decoration: none;
		    font-size: 36px;
		    color: #818181;
		    display: block; /* Display block instead of inline */
		    transition: 0.3s; /* Transition effects on hover (color) */
		}

		/* When you mouse over the navigation links, change their color */
		.overlay a:hover, .overlay a:focus {
		    color: #f1f1f1;
		}

		/* Position the close button (top right corner) */
		.overlay .closebtn {
		    position: absolute;
		    bottom: 20px;
		    right: 45px;
		    font-size: 60px;
		}

		.overlay-content{
			color: #fff;
		}
		.overlay-content td{
			padding: 5px;
		}

		/* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
		@media screen and (max-height: 450px) {
		    .overlay a {font-size: 20px}
		    .overlay .closebtn {
		        font-size: 40px;
		        top: 15px;
		        right: 35px;
		    }
		}
	</style>
</head>
<body oncontextmenu="return true;">	
	<nav id="header" class="navbar navbar-fixed-top">
      <div id="header-container" class="container navbar-container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php print $controller!='home'?'home':''; ?>#home" id="brand" class="navbar-brand"><img src="assets/logo.png" alt='Within Earth' id="homelogo"></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
			          <li><a class='uppercase' href="<?php print $controller!='home'?'home':''; ?>#home">Home</a></li>
			          <li><a class='uppercase' href="about">About</a></li>
			          <li><a class='uppercase' href="<?php print $controller!='home'?'home':''; ?>#services">Services</a></li>
			          <li><a class='uppercase' href="platform">Platform</a></li>
			          <li><a class='uppercase' href="product">Products</a></li>
			          <li><a class='uppercase' href="mice">MICE</a></li>
			          <li class='nav-item dropdown'>
			          	<a class='uppercase dropdown-toggle' href="dmc">DMC <i class="fa fa-chevron-down"></i></a>
			          	<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					          <li><a class="dropdown-item" href="dmc?c=my">Malaysia</a></li>
					          <li><a class="dropdown-item" href="dmc?c=in">Indonesia</a></li>
					          <li><a class="dropdown-item" href="dmc?c=uae">UAE</a></li>
					          <li><a class="dropdown-item" href="dmc?c=turkey">Turkey</a></li>
					          <li><a class="dropdown-item" href="dmc?c=egypt">Egypt</a></li>
					        </ul>
			          </li>
			          <li><a class='uppercase' href="gallery">Gallery</a></li>
			          <li><a class='uppercase' href="contact">Contact</a></li>
			          <li><a class='uppercase active' href="http://onlinewe.net/">Login</a></li>
			        </ul>
          </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
  </nav><!-- /.navbar -->

<?php 
	if(file_exists("$controller.php")){
		require "$controller.php";
	}	else{
		require "404.php";
	}
?>
	<div class='separator'></div>

	<footer>
	<div class="top-footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block">
					<img src="assets/logo.png" class="img-responsive w200">
					<br>
					<p>Since 2007 We have been offering travel services to our agents of the highest quality, combining our energy and enthusiasm .</p>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block">
					<h3>Our Contacts</h3>
					<div>
						<div>Within Earth Holidays SDN. BHD.</div>
						<div>Country: Malaysia </div>
						<div>City: Kuala Lumpur </div>
						<div>Address: Megan Avenue 2, Jalan Yap Kwan Seng,50450 </div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block">					
					<h3>&nbsp;</h3>
						<div><i class="fa fa-phone"></i> <a href='tel:+603 2166 3969'>+603 2166 3969</a></div>
						<div><i class="fa fa-fax"></i> <a href='tel:+603 2166 0418'>+603 2166 0418</a></div>
						<div><i class="fa fa-envelope-open-o"></i> <a href='mailto:sales@withinearth.com'>sales@withinearth.com</a></div>
						<div><i class="fa fa-globe"></i> <a href='http://withinearth.com'>www.withinearth.com</a></div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4 block">
					<div class="social-network">
						<h3>Social Network</h3>
						<ul class="clearfix">
							<li class="youtube"><a href="https://www.facebook.com/withinearth/" target="_blank"></a></li>
							<li class="linkedin"><a href="https://www.linkedin.com/company/withinearth" target="_blank"></a></li>
							<!-- <li class="issuu"><a href="https://issuu.com/withinearth" target="_blank"></a></li> -->
							<li class="facebook"><a href="https://www.facebook.com/withinearth/" target="_blank"></a></li>
							<li class="google"><a href="https://www.facebook.com/withinearth/" target="_blank"></a></li>
							<li class="twitter"><a href="https://www.facebook.com/withinearth/" target="_blank"></a></li>
						</ul>
					</div>
					<br clear="all">
					<br clear="all">
					<br clear="all">
					<div class='center'><a href='javascript:openNav()' class='btn btn-warning'><b>SUBSCRIBE</b></a></div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p class="copyright center">
						<br>
						All rights reserved. ©2009 - 2017 Copyright withinearth.com, owned and operated by Within Earth Holiday Sdn Bhd.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<?php
		$js_files = [
				"core/vendor/bootstrap/js/bootstrap.min.js",
        "core/vendor/owlcarousel/dist/owl.carousel.min.js",
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.BASEURL.'framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
	<div id="myNav" class="overlay">

	  <!-- Button to close the overlay navigation -->
	  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

	  <!-- Overlay content -->
	  <div class="overlay-content">
	    <form>
	    	<table align="center">
	    		<tr><td>Agency Name</td><td><input type="email" class="form-control" ></td></tr>
	    		<tr><td>Fullname</td><td><input class="form-control" ></td></tr>
	    		<tr><td>Email</td><td><input type="email" class="form-control" ></td></tr>
	    		<tr><td>Mobile</td><td><input type="email" class="form-control" ></td></tr>
	    		<tr><td></td><td></td></tr>
	    		<tr><td></td><td><button class="btn btn-danger">Subscribe</button></td></tr>
			  
			</form>
	  </div>

	</div>
	<script type="text/javascript">
		function openNav() {
			document.getElementById("myNav").style.width = "100%";
		}

		/* Close when someone clicks on the "x" symbol inside the overlay */
		function closeNav() {
			document.getElementById("myNav").style.width = "0%";
		}
	</script>
</body>
</html>