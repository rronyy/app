
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    <!--/div-->
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<?php
 $jsfiles = array("core/vendor/jqueryplugins/validator.min.js",
    "core/vendor/jqueryplugins/moment.js",
    "core/vendor/bootstrap/js/bootstrap-datetimepicker.js");
foreach ($jsfiles as $jsfile) {
    print "<script src='".BASEURL."framework/$jsfile' type='text/javascript'></script>\n";
}
?>
<script type="text/javascript">
    $("fomr").validator({"disable":false});
    $('.datetime').datetimepicker({
        sideBySide: true
    });
    $('.date').datetimepicker({
        format: 'YYYY-MM-DD'
    });
</script>
