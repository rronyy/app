
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    <!--/div-->
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php
 $jsfiles = array("core/vendor/jqueryplugins/validator.min.js",
    "core/vendor/jqueryplugins/moment.js",
    "core/vendor/bootstrap/js/bootstrap-datetimepicker.js");
foreach ($jsfiles as $jsfile) {
    print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
}
?>

<script type="text/javascript">
    $("form").validator({"disable":false});
    $('.datetime').datetimepicker({
        sideBySide: true
    });
    // $('.date').datetimepicker({
    //     format: 'YYYY-MM-DD'
    // });
    $(".date").datepicker();
    $(".window-control-close").click(function(){
        location.href = "<?php print BASEURL.APP; ?>/home";
    });
    $(".window-control-back").click(function(){
        window.history.back();
    });

</script>
