// lazyload config
var MODULE_CONFIG = {
    easyPieChart:   [ 'framework/themes/flatui/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ 'framework/themes/flatui/libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ 'framework/themes/flatui/libs/jquery/flot/jquery.flot.js',
                      'framework/themes/flatui/libs/jquery/flot/jquery.flot.resize.js',
                      'framework/themes/flatui/libs/jquery/flot/jquery.flot.pie.js',
                      'framework/themes/flatui/libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      'framework/themes/flatui/libs/jquery/flot-spline/js/jquery.flot.spline.min.js',
                      'framework/themes/flatui/libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js'],
    vectorMap:      [ 'framework/themes/flatui/libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      'framework/themes/flatui/libs/jquery/bower-jvectormap/jquery-jvectormap.css', 
                      'framework/themes/flatui/libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      'framework/themes/flatui/libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js' ],
    dataTable:      [
                      'framework/themes/flatui/libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                      'framework/themes/flatui/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      'framework/themes/flatui/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    footable:       [
                      'framework/themes/flatui/libs/jquery/footable/dist/footable.all.min.js',
                      'framework/themes/flatui/libs/jquery/footable/css/footable.core.css'
                    ],
    screenfull:     [
                      'framework/themes/flatui/libs/jquery/screenfull/dist/screenfull.min.js'
                    ],
    sortable:       [
                      'framework/themes/flatui/libs/jquery/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      'framework/themes/flatui/libs/jquery/nestable/jquery.nestable.css',
                      'framework/themes/flatui/libs/jquery/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      'framework/themes/flatui/libs/jquery/summernote/dist/summernote.css',
                      'framework/themes/flatui/libs/jquery/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      'framework/themes/flatui/libs/jquery/parsleyjs/dist/parsley.css',
                      'framework/themes/flatui/libs/jquery/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      'framework/themes/flatui/libs/jquery/select2/dist/css/select2.min.css',
                      'framework/themes/flatui/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      'framework/themes/flatui/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      'framework/themes/flatui/libs/jquery/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      'framework/themes/flatui/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      'framework/themes/flatui/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      'framework/themes/flatui/libs/js/moment/moment.js',
                      'framework/themes/flatui/libs/jquery/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    chart:          [
                      'framework/themes/flatui/libs/js/echarts/build/dist/echarts-all.js',
                      'framework/themes/flatui/libs/js/echarts/build/dist/theme.js',
                      'framework/themes/flatui/libs/js/echarts/build/dist/jquery.echarts.js'
                    ],
    bootstrapWizard:[
                      'framework/themes/flatui/libs/jquery/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      'framework/themes/flatui/libs/jquery/moment/moment.js',
                      'framework/themes/flatui/libs/jquery/fullcalendar/dist/fullcalendar.min.js',
                      'framework/themes/flatui/libs/jquery/fullcalendar/dist/fullcalendar.css',
                      'framework/themes/flatui/libs/jquery/fullcalendar/dist/fullcalendar.theme.css',
                      'scripts/plugins/calendar.js'
                    ],
    dropzone:       [
                      'framework/themes/flatui/libs/js/dropzone/dist/min/dropzone.min.js',
                      'framework/themes/flatui/libs/js/dropzone/dist/min/dropzone.min.css'
                    ]
  };
