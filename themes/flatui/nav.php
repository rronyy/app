  <!-- aside -->
  <div id="aside" class="app-aside box-shadow-z3 modal fade lg nav-expand">
    <div class="left navside white-back dk" layout="column">
      <div class="navbar navbar-md info no-radius box-shadow-z4">
        <!-- brand -->
        <a class="navbar-brand">
        	<div ui-include="'framework/themes/flatui/assets/images/logo.white.svg'"></div>
        	<img src="framework/themes/flatui/assets/images/logo.png" alt="." class="hide">
        	<span class="hidden-folded inline">Flatkit</span>
        </a>
        <!-- / brand -->
      </div>
      <div flex class="hide-scroll">
        <nav class="scroll">
          <div ui-include="'framework/themes/flatui/views/blocks/aside.top.4.html'"></div>
          
            <ul class="nav" ui-nav>
              <li>
                <a href="<?php print $appurl; ?>/home" >
                  <span class="nav-icon">
                    <i class="material-icons">&#xe3fc;
                      <span ui-include="'framework/themes/flatui/assets/images/i_0.svg'"></span>
                    </i>
                  </span>
                  <span class="nav-text">Dashboard</span>
                </a>
              </li>               

              <?php 
                $menu_file = "temps/menus/menu.".APP.".".uid();
                if(file_exists($menu_file)){
                    include $menu_file;
                } else{
                    $menu_content = "";
                    $root_menu = select("*", "sys_privilege", "root=0 AND hidden=0 AND id<>84", "ORDER BY position");
                    while ($root = mysqli_fetch_object($root_menu)) {
                        if(uid()==1){
                            $menus = select("*", "sys_privilege", "root=$root->id AND hidden=0", "ORDER BY  `position`");
                        } else{
                            $menus = select("SELECT * FROM sys_permission WHERE root=$root->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                        }
                        if($menus->num_rows){
                            //$menu_content .= "<li><a href='#'><i class='$root->glyphicon fa-fw'></i>".ucfirst($root->title)."<span class='fa arrow'></span></a>
                            //    <ul class='nav nav-second-level w100p'>";
                            $menu_content .= "<li>
                              <a>
                                <span class='nav-caret'><i class='fa fa-caret-down'></i></span>
                                <span class='nav-icon'>
                                  <i class='$root->glyphicon fa-fw'></i>
                                </span>
                                <span class='nav-text'>".ucfirst($root->title)."</span>
                              </a>
                              <ul class='nav-sub'>";
                            while ($menu = mysqli_fetch_object($menus)) {
                                if(uid()==1){
                                    $menus3 = select("*", "sys_privilege", "root=$menu->id AND hidden=0", "ORDER BY  `position`");
                                } else{
                                    $menus3 = select("SELECT * FROM sys_permission WHERE root=$menu->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                                }
                                //$menu_content .= "<li><a href='".APPURL.(nn($menu->module)?"/$menu->module":"")."/$menu->link/$menu->option'><i class='$menu->glyphicon'></i> $menu->title".($menus3->num_rows?"<span class='fa arrow'></span>":"")."</a>";
                                $menu_content .= "<li><a href='".APPURL.(nn($menu->module)?"/$menu->module":"")."/$menu->link/$menu->option' ><span class='nav-text'><i class='$menu->glyphicon fa-fw'></i> $menu->title</span></a></li>";
                                if($menus3->num_rows){
                                    $menu_content .= "<ul class='nav nav-third-level'>";
                                        while ($menu3 = mysqli_fetch_object($menus3)) {
                                            $menu_content .= "<li><a href='".APPURL.(nn($menu3->module)?"/$menu3->module":"")."/$menu3->link/$menu3->option'><i class='$root->glyphicon fa-fw'></i>$menu3->title</a></li>";
                                        }
                                    $menu_content .= "</ul>";
                                }
                                $menu_content .= "</li>";
                            }
                            $menu_content .= "</ul>
                                </li>";
                        }
                    }
                    print $menu_content;
                    if(uid()==1){
                        $menus = select("*", "sys_privilege", "root=84 AND hidden=0", "ORDER BY  `position`");
                    } else{
                        $menus = select("SELECT * FROM sys_permission WHERE root=84 AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                    }
                    while ($menu = mysqli_fetch_object($menus)) {
                        $menu_content .= "<li><a href='$menu->link'><i class='$menu->glyphicon'></i>".ucfirst($menu->title)."</a></li>";
                    }
                }
                ?>
          
            </ul>
        </nav>
      </div>
      <div flex-no-shrink>
        <div ui-include="'framework/themes/flatui/views/blocks/aside.bottom.1.html'"></div>
      </div>
    </div>
  </div>
  <!-- / aside -->