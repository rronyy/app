<?php
if(isset($_GET['rtl'])){
  $_SESSION['rtl'] = $_GET['rtl']==1;
}
?>
<?php require "header.php"; ?>
<body>
<?php if(loggedin()): ?>
  <div class="app" id="app">
  <?php require "nav.php"; ?>
    <div id="content" class="app-content box-shadow-z3" role="main">      
      <?php require "content_header.php"; ?>
      <?php require "content_footer.php"; ?>
      <div class="app-body" id="view">        
          <?php require 'content.php';?>
      </div>
    </div>
    </div>
  <?php require "footer.php"; ?>
<?php elseif (isset($module)):  print "module"; require "$base/framework/modules/$module/index.php"; ?>
<?php else: require "login.php" ?>
<?php endif; ?>