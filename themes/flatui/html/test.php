<table align='center'>
	<tr><td colspan='5'><b>Agent Details</b></td><tr>
		<tr><td>Name</td><td><input type='text' name='name' id='name' value='$object->name' class='form-control required' /></td><td></td><td>Registration</td><td><input type='text' name='registration' id='registration' value='$object->registration_no' class='form-control ' /></td></tr>
		<tr><td>Contact Person</td><td><input type='text' name='contact_person' id='contact_person' value='$object->contact_person' class='form-control ' /></td><td></td><td>Address</td><td><textarea name='address' id='address' class='form-control '>$object->address</textarea></td></tr>
		<tr><td>Phone</td><td><input type='text' name='phone' id='phone' value='$object->phone' class='form-control ' /></td><td></td><td>Email</td><td><input type='text' name='email' id='email' value='$object->email' class='form-control email' /></td></tr>
		<tr><td>Notes</td><td><textarea name='notes' id='notes' class='form-control '>$object->notes</textarea></td><td></td><td>Credit Limit</td><td><input type='number' name='credit_limit' value='$object->credit_limit' class='form-control' required /></tr>
		<tr><td>Opening Balance</td><td><input type='number' name='opening_balance' class='form-control' value='$object->opening_balance' /></td><td></td><td>Currency</td><td>".enum("currency", $object->currency)."</td></tr>
	</table>