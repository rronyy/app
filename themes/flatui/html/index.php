<?php
session_start();
if(isset($_GET['rtl'])){
  $_SESSION['rtl'] = $_GET['rtl']==1;
}
?>
<?php require "header.php"; ?>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->
<?php require "nav.php"; ?>
  
  <!-- content -->
  <div id="content" class="app-content box-shadow-z3" role="main">
    
    <?php require "content_header.php"; ?>
    <?php require "content_footer.php"; ?>
    <div class="app-body" id="view">
      <?php require "content.php"; ?>
    </div>
  </div>
  <!-- / -->

  <!-- theme switcher -->
  
  <!-- / -->

<!-- ############ LAYOUT END-->

  </div>

  <?php require "footer.php"; ?>