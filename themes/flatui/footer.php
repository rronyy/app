  <?php
    $jsfiles = array(
      "themes/flatui/libs/jquery/jquery/dist/jquery.js",
      "themes/flatui/libs/jquery/tether/dist/js/tether.min.js",
      "themes/flatui/libs/jquery/bootstrap/dist/js/bootstrap.js",
      "themes/flatui/libs/jquery/underscore/underscore-min.js",
      "themes/flatui/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js",
      "themes/flatui/libs/jquery/PACE/pace.min.js",
      "themes/flatui/libs/jquery/jquery-pjax/jquery.pjax.js",
      "themes/flatui/scripts/config.lazyload.js",
      "themes/flatui/scripts/palette.js",
      "themes/flatui/scripts/ui-load.js",
      "themes/flatui/scripts/ui-jp.js",
      "themes/flatui/scripts/ui-include.js",
      "themes/flatui/scripts/ui-device.js",
      "themes/flatui/scripts/ui-form.js",
      "themes/flatui/scripts/ui-nav.js",
      "themes/flatui/scripts/ui-screenfull.js",
      "themes/flatui/scripts/ui-scroll-to.js",
      "themes/flatui/scripts/ui-toggle-class.js",
      //"themes/flatui/scripts/app.js",
      "themes/flatui/scripts/ajax.js",
        // "core/vendor/jquery/jquery.min.js",
        // "core/vendor/jquery/jquery-ui.min.js",
        // "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
        // "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
        // "core/vendor/jqueryplugins/jquery.qtip.min.js",
        // "core/vendor/jqueryplugins/jquery.cookie.js",
        // "core/vendor/jqueryplugins/jquery.validate.min.js",
        // "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
        // "core/vendor/jquery.hotkeys/jquery.hotkeys.js",
        // "core/vendor/panacea/js/scripts.js",
        // "core/vendor/bootstrap/js/bootstrap-select.min.js",
        // "core/vendor/bootstrap/js/bootstrap-typeahead.js",
        // "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js",
        // "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js","core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js",
        // "core/vendor/bootstrap-table/dist/bootstrap-table.min.js",
        // "core/vendor/highcharts/highcharts.js",
        // "core/vendor/highcharts/exporting.js",
        // "core/vendor/bootstrap-treeview/bootstrap-treeview.min.js"
        );
    foreach ($jsfiles as $jsfile) {
        print "<script src='$appurl/framework/$jsfile' type='text/javascript'></script>\n";
    }
  ?>
</body>
</html>
