<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Flatkit - HTML Version | Bootstrap 4 Web App Kit with AngularJS</title>
  <meta name="description" content="Admin, Dashboard, Bootstrap, Bootstrap 4, Angular, AngularJS" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="assets/images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="assets/images/logo.png">
  
  <!-- style -->
  <?php
    $cssfiles = array("themes/flatui/assets/animate.css/animate.min.css",
        "themes/flatui/assets/glyphicons/glyphicons.css",
        "themes/flatui/assets/font-awesome/css/font-awesome.min.css",
        "themes/flatui/assets/material-design-icons/material-design-icons.css",
        "themes/flatui/assets/bootstrap/dist/css/bootstrap.min.css",
        "themes/flatui/assets/styles/app.css",
        "themes/flatui/assets/styles/font.css",
        "core/vendor/bootstrap/css/bootstrap-select.min.css",
        "core/vendor/bootstrap-table/dist/bootstrap-table.min.css",
        "core/vendor/jqueryplugins/jquery.qtip.min.css",
        "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
        "core/vendor/fontawesome/font-awesome-animation.min.css",
        "core/vendor/bootstrap-treeview/bootstrap-treeview.min.css",
        "core/vendor/panacea/css/styles.css"
      );
    if(isset($_SESSION['rtl'])){
      if($_SESSION['rtl']) array_push($cssfiles, 'assets/styles/app.rtl.css');
    }
    foreach ($cssfiles as $cssfile) {
      print "<link href='$appurl/framework/$cssfile' rel='stylesheet'>\n";
    }
    if(file_exists("$appurl/assets/css/".theme().".css")){
      print "<link href='$appurl/assets/css/".theme().".css' rel='stylesheet'>";
    }
    if(file_exists("css/custom.css")){
      print "<link href='$appurl/css/custom.css' rel='stylesheet'>";
    }
    if(file_exists("assets/custom.css")){
      print "<link href='$appurl/assets/custom.css' rel='stylesheet'>";
    }
  ?>
</head>
<body>