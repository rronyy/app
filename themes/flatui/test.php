<form ui-jp="parsley" novalidate="">
<table align='center'>
	<tr><td colspan='5'><b>Agent Details</b></td><tr>
		<tr><td>Name</td><td><input type='text' name='name' id='name' class='form-control required' required="" data-parsley-id="4" /></td><td></td><td>Registration</td><td><input type='text' name='registration' id='registration' class='form-control ' /></td></tr>
		<tr><td>Contact Person</td><td><input type='text' name='contact_person' id='contact_person' class='form-control ' /></td><td></td><td>Address</td><td><textarea name='address' id='address' class='form-control '>address</textarea></td></tr>
		<tr><td>Phone</td><td><input type='text' name='phone' id='phone' class='form-control ' /></td><td></td><td>Email</td><td><input type='text' name='email' id='email' class='form-control email' /></td></tr>
		<tr><td>Notes</td><td><textarea name='notes' id='notes' class='form-control '>notes</textarea></td><td></td><td>Credit Limit</td><td><input type='number' name='credit_limit' class='form-control' required /></tr>
		<tr><td>Opening Balance</td><td><input type='number' name='opening_balance' class='form-control' /></td><td></td><td>Currency</td><td>".enum("currency", currency)."</td></tr>
	</table>
<?php closeForm(); ?>

<form ui-jp="parsley" novalidate="">
        <div class="box">
          <div class="box-header">
            <h2>Register</h2>
          </div>
          <div class="box-body">
            <p class="text-muted">Please fill the information to continue</p>
            <div class="form-group">
              <label>Username</label>
              <input type="text" class="form-control" required="" data-parsley-id="4">                        
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" required="" data-parsley-id="6">                        
            </div>
            <div class="row m-b">
              <div class="col-sm-6">
                <label>Enter password</label>
                <input type="password" class="form-control" required="" id="pwd" data-parsley-id="8">   
              </div>
              <div class="col-sm-6">
                <label>Confirm password</label>
                <input type="password" class="form-control" data-parsley-equalto="#pwd" required="" data-parsley-id="10">      
              </div>   
            </div>
            <div class="form-group">
              <label>Phone</label>
              <input type="number" class="form-control" placeholder="XXX XXXX XXX" required="" data-parsley-id="12">
            </div>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox" name="check" checked="" required="true" class="has-value" data-parsley-multiple="check" data-parsley-id="15"><i></i> I agree to the <a href="#" class="text-info">Terms of Service</a>
              </label>
            </div>
          </div>
          <div class="dker p-a text-right">
            <button type="submit" class="btn info">Submit</button>
          </div>
        </div>
      </form>