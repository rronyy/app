<!DOCTYPE html>
<html>
<head>
	<title>WELCOME TO PACIFIC INTERLINK</title>
	<link rel="icon" href="favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<?php
		$css_files = [
				"core/vendor/jquery/jquery-ui/jquery-ui.min.css",
        "core/vendor/bootstrap/css/bootstrap.min.css",
        "themes/sbadmin/bower_components/font-awesome/css/font-awesome.min.css",
        "core/vendor/bootstrap/css/bootstrap-select.min.css",
        "core/vendor/jqueryplugins/jquery.qtip.min.css",
        "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
        "core/vendor/fontawesome/font-awesome-animation.min.css",
        "core/vendor/owlcarousel/dist/assets/owl.carousel.min.css",
        "core/vendor/owlcarousel/dist/assets/owl.theme.default.min.css",
        "core/vendor/animate.css/animate.min.css",
        "core/vendor/magic/magic.min.css",
        "core/vendor/csshake/dist/csshake.min.css",
        "core/vendor/panacea/css/styles.css",
        "core/vendor/ionicons/css/ionicons.min.css",
        "themes/pil/css/custom.css"
      ];
		foreach ($css_files as $css_file) {
			print '<link rel="stylesheet" type="text/css" href="'.BASEURL.'framework/'.$css_file.'">'.PHP_EOL;
		}
		if(file_exists("assets/css/".theme().".css")){
      print "<link href='assets/css/".theme().".css' rel='stylesheet'>".PHP_EOL;
    }

		$js_files = [
				"core/vendor/jquery/jquery.min.js",
        "core/vendor/jquery/jquery-ui.min.js",
        "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
        "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
        "core/vendor/jqueryplugins/jquery.qtip.min.js",
        "core/vendor/jqueryplugins/jquery.cookie.js",
        "core/vendor/jqueryplugins/jquery.validate.min.js",
        "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
        "core/vendor/panacea/js/scripts.js",
        "core/vendor/bootstrap/js/bootstrap-select.min.js",
        "core/vendor/bootstrap/js/bootstrap-typeahead.js",
        "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js",
        "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js","core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js",
        "core/vendor/waypoints/lib/jquery.waypoints.min.js",
        "themes/pil/js/custom.js",
        "core/vendor/highcharts/highcharts.js",
        "core/vendor/highcharts/exporting.js"
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.BASEURL.'framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
</head>
<body>	
	<nav id="header" class="navbar navbar-fixed-top">
      <div id="header-container" class="container navbar-container">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php print $controller!='home'?'home':''; ?>#home" id="brand" class="navbar-brand"><img src="assets/logo.png" alt='Within Earth' id="homelogo"></a>
          </div>
          <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav navbar-right">
			          <li>
			          	<a class='uppercase' href="<?php print $controller!='home'?'home':''; ?>#home">Home</a>
			          </li>
			          <li class='nav-item dropdown'>
			          	<a class='uppercase dropdown-toggle' href="<?php print $controller!='home'?'home':''; ?>#about">About <i class="fa fa-chevron-down"></i></a>
			          	<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					          <li><a class="dropdown-item" href="#about">PIL</a></li>
					          <li><a class="dropdown-item" href="#about_us">About Us</a></li>
					          <li><a class="dropdown-item" href="#">Something else here</a></li>
					        </ul>
					      </li>
			          <li><a class='uppercase' href="<?php print $controller!='home'?'home':''; ?>#services">Services</a></li>
			          <li><a class='uppercase' href="<?php print $controller!='home'?'home':''; ?>#product">Products</a></li>
			          <li><a class='uppercase' href="sustainability">Sustainability</a></li>
			          <li><a class='uppercase' href="contact">Contact</a></li>
			        </ul>
          </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
  </nav><!-- /.navbar -->

<?php 
	if(file_exists("$controller.php")){
		require "$controller.php";
	}	else{
		require "404.php";
	}
?>
	<div class='separator'></div>

	<footer>
	<div class="top-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
					<img src="assets/logo.png" class="img-responsive">
					<p>Pacific Inter-Link (PIL) was incorporated in 1988 in Kuala Lumpur to promote the export of various commodities and services from Malaysia and the Far East to the Global Markets.</p>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4">
					<h3>Our Contacts</h3>
					<div>
						<div><a href='./'>Pacific Inter-Link Group (PIL)</a></div>
						<div>Country: Malaysia </div>
						<div>City: Kuala Lumpur </div>
						<div>Address: PWTC </div>
					</div>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-4">					
					<h3>&nbsp;</h3>
						<div><i class="fa fa-phone"></i> + 603-4042 3933</div>
						<div><i class="fa fa-fax"></i> + 603-4042 3933</div>
						<div><i class="fa fa-envelope-open-o"></i> <a href="mailto:info@pilgroup.com">info@pilgroup.com</a></div>
						<div><i class="fa fa-globe"></i> www.pilgroup.com</div>
				</div>

				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-4">
					<div class="social-network">
						<h3>Social Network</h3>
						<ul class="clearfix">
							<li class="linkedin"><a href="https://www.linkedin.com/company/pil" target="_blank"></a></li>
							<li class="issuu"><a href="https://issuu.com/pil" target="_blank"></a></li>
							<li class="facebook"><a href="https://www.facebook.com/pil/" target="_blank"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p class="copyright center grey">
						All rights reserved. ©2009 - 2017 Copyright withinearth.com, owned and operated by Within Earth Holiday Sdn Bhd.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<?php
		$js_files = [
				"core/vendor/bootstrap/js/bootstrap.min.js",
        "core/vendor/owlcarousel/dist/owl.carousel.min.js",
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.BASEURL.'framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
	
</body>
</html>