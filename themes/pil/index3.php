<!DOCTYPE html>
<html>
<head>
	<title>Within Earth Holiday</title>
	<link rel="icon" href="favicon.ico">
	<?php
		$css_files = [
				"core/vendor/jquery/jquery-ui/jquery-ui.min.css",
        "core/vendor/bootstrap/css/bootstrap.min.css",
        "themes/sbadmin/bower_components/font-awesome/css/font-awesome.min.css",
        "core/vendor/bootstrap/css/bootstrap-select.min.css",
        "core/vendor/jqueryplugins/jquery.qtip.min.css",
        "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
        "core/vendor/fontawesome/font-awesome-animation.min.css",
        "core/vendor/superslides/dist/stylesheets/superslides.css",
        "core/vendor/animate.css/animate.css",
        "core/vendor/panacea/css/styles.css",
        "core/vendor/ionicons/css/ionicons.min.css"
      ];
		foreach ($css_files as $css_file) {
			print '<link rel="stylesheet" type="text/css" href="'.BASEURL.'framework/'.$css_file.'">'.PHP_EOL;
		}
		if(file_exists("assets/css/".theme().".css")){
      print "<link href='assets/css/".theme().".css' rel='stylesheet'>".PHP_EOL;
    }

		$js_files = [
				"core/vendor/jquery/jquery.min.js",
        "core/vendor/jquery/jquery-ui.min.js",
        "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
        "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
        "core/vendor/jqueryplugins/jquery.qtip.min.js",
        "core/vendor/jqueryplugins/jquery.cookie.js",
        "core/vendor/jqueryplugins/jquery.validate.min.js",
        "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
        "core/vendor/panacea/js/scripts.js",
        "core/vendor/bootstrap/js/bootstrap-select.min.js",
        "core/vendor/bootstrap/js/bootstrap-typeahead.js",
        "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js",
        "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js","core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js",
        "core/vendor/highcharts/highcharts.js",
        "core/vendor/highcharts/exporting.js"
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.BASEURL.'framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
	<style type="text/css">

		/* CUSTOMIZE THE NAVBAR
		-------------------------------------------------- */

		/* Special class on .container surrounding .navbar, used for positioning it into place. */
		.navbar-wrapper {
		  position: absolute;
		  top: 0;
		  right: 0;
		  left: 0;
		  z-index: 20;
		}

		/* Flip around the padding for proper display in narrow viewports */
		.navbar-wrapper > .container {
		  padding-right: 0;
		  padding-left: 0;
		}
		.navbar-wrapper .navbar {
		  padding-right: 15px;
		  padding-left: 15px;
		}
		.navbar-wrapper .navbar .container {
		  width: auto;
		}
		.navbar a{
			font-weight: bold !important;
    	color: #eef !important;
		}
		/*.navbar-static-top{*/
		ul.navbar-nav{
			background: rgba(40,133,199,.7);
	    /* color: #F80; */
	    border-radius: 5px;
		}
		/* RESPONSIVE CSS
-------------------------------------------------- */

@media (min-width: 768px) {
  /* Navbar positioning foo */
  .navbar-nav {
    float: right;
    margin: 0;
	}
  .navbar-wrapper {
    margin-top: 20px;
  }
  .navbar-wrapper .container {
    padding-right: 15px;
    padding-left: 15px;
  }
  .navbar-wrapper .navbar {
    padding-right: 0;
    padding-left: 0;
  }

  /* The navbar becomes detached from the top, so we round the corners */
  .navbar-wrapper .navbar {
    border-radius: 4px;
  }
}
.bounce {
	-webkit-animation: bounce 2s infinite;
	-moz-animation: bounce 2s infinite;
	-ms-animation: bounce 2s infinite;
	animation: bounce 2s infinite;
}

.move {
  width: 40px;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 15px;
}
.home-bottom {
  position: absolute;
  z-index: 99;
  width: 100%;
  left: 0;
  bottom: 0;
  text-align: center;
  opacity: 0.9;
}
/************** LOGO GLOW *******************/
/*12px 12px 7px*/
@keyframes greenPulse {
  from {filter: drop-shadow(6px 6px 3px rgba(100,100,255,0.5)); }
  to {filter: drop-shadow(6px 6px 3px rgba(100,100,255,0.5)); }
}
@-webkit-keyframes greenPulse {
  from { -webkit-filter: drop-shadow(6px 6px 3px rgba(100,100,255,0.5)); filter: url(assets/shadow.svg#drop-shadow); }
  50% {  -webkit-filter: drop-shadow(6px 6px 3px rgba(100,100,0,0.5)); filter: url(assets/shadow.svg#drop-shadow); }
  to {  -webkit-filter: drop-shadow(6px 6px 3px rgba(100,100,255,0.5)); filter: url(assets/shadow.svg#drop-shadow); }
}
#homelogo {
	/*-webkit-filter: drop-shadow(6px 6px 3px rgba(0,0,0,0.5)); filter: url(assets/shadow.svg#drop-shadow);*/
	animation-name: greenPulse;
	animation-duration: 4s;
	animation-iteration-count: infinite;
  -webkit-animation-name: greenPulse;
  -webkit-animation-duration: 4s;
  -webkit-animation-iteration-count: infinite; 
}
	</style>
</head>
<body style="margin: 0px; padding: 0px">
	<div class="navbar-wrapper">
	  <div class="container">

	    <nav class="navbar navbar-static-top">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="home"><img src="assets/logo.png" alt='Within Earth' id="homelogo"></a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li class="active"><a href="#">Home</a></li>
	            <li><a href="#services">Services</a></li>
	            <li><a href="#about">About</a></li>
	            <li><a href="#contact">Products</a></li>
	            <li><a href="#contact">MICE</a></li>
	            <li><a href="#contact">Platform</a></li>
	            <li><a href="#contact">Our Agent</a></li>
	            <!-- <li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
	              <ul class="dropdown-menu">
	                <li><a href="#">Action</a></li>
	                <li><a href="#">Another action</a></li>
	                <li><a href="#">Something else here</a></li>
	                <li role="separator" class="divider"></li>
	                <li class="dropdown-header">Nav header</li>
	                <li><a href="#">Separated link</a></li>
	                <li><a href="#">One more separated link</a></li>
	              </ul>
	            </li> -->
	          </ul>
	        </div>
	      </div>
	    </nav>

	  </div>
	</div>
	
	<div id="fullscreen-slides" class="slides">
		<div class="slides-container">
			<img src="assets/slide1.jpg" />
			<img src="assets/slide2.jpg" />
			<img src="assets/slide3.jpg" />
			<img src="assets/slide4.jpg" />
			<img src="assets/slide5.jpg" />
		</div>
	</div>
	<!-- <div class="home-bottom">
	    <div class="container text-center">
	        <div class="move bounce">
	            <a href="#start" class="ion-ios-arrow-down btn-scroll"></a>
	        </div>
	    </div>
	</div> -->
	<br clear="all">
	<div id="slide_separator"></div>
	<section>
		<div class="container-fluid">
		<div>
			sadfasdf
			sa
			dfas
			dfas
			fsad
			fas
			fs
			afsa
			saf
			sa
			afd
		</div>
		</div>
	</section>
	<?php
		$js_files = [
				"core/vendor/bootstrap/js/bootstrap.min.js",
        "core/vendor/superslides/dist/jquery.superslides.min.js",
			];
		foreach ($js_files as $js_file) {
			print '<script type="text/javascript" src="'.BASEURL.'framework/'.$js_file.'"></script>'.PHP_EOL;
		}
	?>
	<script type="text/javascript">
		$('.slides').superslides({
		  slide_easing: 'easeInOutCubic',
		  slide_speed: 800,
		  pagination: true,
		  hashchange: true,
		  scrollable: true
		});
		$('.slides').height($(window).height());
		// $('.slides').superslides();
		// $(function () {
		// 	$('.slides').superslides({
		// 		play: 2,
		// 		pagination: true
		// 	});
		// 	$('.slides').height($(window).height());
		// });
	</script>
</body>
</html>