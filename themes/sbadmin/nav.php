<?php
//"assets/css/".theme().".css"
if(file_exists("nav.php")):
    include("nav.php");
else:
?>
<!-- Navigation -->
<style type="text/css">

</style>
<?php
    $privileges = R::find("sys_privilege", "id>0");
?>
<nav class="navbar navbar-default navbar-static-top npf dp" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php print makeUri("home"); ?>"><img src='<?php print $appurl; ?>/assets/logo.png' width='32px' /></a>
        <a class="navbar-brand" href="<?php print makeUri("home"); ?>"><?php print APPTITLE." v".APPVERSION; ?></a>
    </div>
    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-right">
    <?php
        $menu_top = array();
        $menu_top_item = array(array('url'=>'home', 'title'=>'Home', 'description'=>'Go to Homepage'));
        array_push($menu_top, $menu_top_item);

        foreach ($menu_top as $menu_top_item) {
            /*$m = "<li class='dropdown'>
                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                    <i class='fa fa-reply fa-fw'></i><i class='fa fa-caret-down'></i>
                </a>
                <ul class='dropdown-menu'>
                    <li>
                        <a href='<?php print BASEURL.'/$menu_item->url'; ?>'>
                            <div>
                                <strong>$menu_item->title</strong>
                                <span class='pull-right text-muted'>
                                    <em>$menu_item->description</em>
                                </span>
                            </div>
                        </a>
                    </li>
                </ul>
            </li>";*/
        }
    ?>



        <li class='pointer'>
            <a class="dropdown-toggle" data-toggle="dropdown" onclick="window.print()">
                <i class="fa fa-print fa-fw"></i></i>
            </a>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                <li>
                    <a href="#">
                        <div>
                            <strong>John Smith</strong>
                            <span class="pull-right text-muted">
                                <em>Yesterday</em>
                            </span>
                        </div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <strong>John Smith</strong>
                            <span class="pull-right text-muted">
                                <em>Yesterday</em>
                            </span>
                        </div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <strong>John Smith</strong>
                            <span class="pull-right text-muted">
                                <em>Yesterday</em>
                            </span>
                        </div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>Read All Messages</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-messages -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-tasks">
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Task 1</strong>
                                <span class="pull-right text-muted">40% Complete</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Task 2</strong>
                                <span class="pull-right text-muted">20% Complete</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                    <span class="sr-only">20% Complete</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Task 3</strong>
                                <span class="pull-right text-muted">60% Complete</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                    <span class="sr-only">60% Complete (warning)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Task 4</strong>
                                <span class="pull-right text-muted">80% Complete</span>
                            </p>
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                    <span class="sr-only">80% Complete (danger)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>See All Tasks</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-tasks -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">

                <i class="fa fa-bell fa-fw faa-ring animated"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-comment fa-fw"></i> New Comment
                            <span class="pull-right text-muted small">4 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                            <span class="pull-right text-muted small">12 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-envelope fa-fw"></i> Message Sent
                            <span class="pull-right text-muted small">4 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-tasks fa-fw"></i> New Task
                            <span class="pull-right text-muted small">4 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div>
                            <i class="fa fa-upload fa-fw"></i> Server Rebooted
                            <span class="pull-right text-muted small">4 minutes ago</span>
                        </div>
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a class="text-center" href="#">
                        <strong>See All Alerts</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-alerts -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="javascript:goto('<?php print makeUri("user", "user/profile_own"); ?>')"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="javascript:goto('<?php print makeUri("login", "logout"); ?>')"><i class="fa fa-sign-out fa-fw"></i> Logout (<?php print username(); ?>)</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar dp" role="navigation">
        <div align="center" id='loader'><br ><img src="<?php print $appurl; ?>/framework/common/loader.gif" align="center" /></div>
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu" style="display:none">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input type="text" class="form-control" id="key" placeholder="Search..." onfocus="search()" onkeyup="search()">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li class="active">
                    <a href="<?php print makeUri("home"); ?>" class="active"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <?php //var_dump($config);
                $menu_file = "temps/menus/menu.".APP.".".uid();
                if(file_exists($menu_file)){
                    include $menu_file;
                } else{
                    $menu_content = "";
                    $root_menu = select("*", "sys_privilege", "root=0 AND hidden=0 AND id<>84", "ORDER BY position");
                    //$permissions = permissions();
                    while ($root = mysqli_fetch_object($root_menu)) {
                        if(uid()==1){
                            $menus = select("*", "sys_privilege", "root=$root->id AND hidden=0", "ORDER BY  `position`");
                        } else{
                            //console_log("$permissions AND root=$root->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                            $menus = select("SELECT * FROM sys_permission WHERE root=$root->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                        }
                        if($menus->num_rows){
                            $menu_content .= "<li><a href='#'><i class='$root->glyphicon fa-fw'></i>".ucfirst($root->title)."<span class='fa arrow'></span></a>
                                <ul class='nav nav-second-level w100p'>";
                            while ($menu = mysqli_fetch_object($menus)) {
                                if(uid()==1){
                                    $menus3 = select("*", "sys_privilege", "root=$menu->id AND hidden=0", "ORDER BY  `position`");
                                } else{
                                    //console_log("$permissions AND root=$menu->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                                    $menus3 = select("SELECT * FROM sys_permission WHERE root=$menu->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                                }
                                $menu_content .= "<li><a href='".APPURL.(nn($menu->module)?"/$menu->module":"")."/$menu->link/$menu->option'><i class='$menu->glyphicon'></i> $menu->title".($menus3->num_rows?"<span class='fa arrow'></span>":"")."</a>";
                                if($menus3->num_rows){
                                    $menu_content .= "<ul class='nav nav-third-level'>";
                                        while ($menu3 = mysqli_fetch_object($menus3)) {
                                            $menu_content .= "<li><a href='".APPURL.(nn($menu3->module)?"/$menu3->module":"")."/$menu3->link/$menu3->option'><i class='$root->glyphicon fa-fw'></i>$menu3->title</a></li>";
                                        }
                                    $menu_content .= "</ul>";
                                }
                                $menu_content .= "</li>";
                            }
                            $menu_content .= "</ul>
                                </li>";
                        }
                    }
                    /*$acls = "SELECT acls_a.*, acls_p.link FROM (sys_acl acls_a JOIN sys_privilege acls_p) WHERE ((acls_a.privilege = acls_p.id) AND (acls_a.utype = 'u'))UNION SELECT acls_a.*, acls_p.link FROM ((sys_acl acls_a JOIN sys_privilege acls_p) JOIN sys_user_role acls_r) WHERE ((acls_a.privilege = acls_p.id) AND (acls_r.ur_role_id = acls_a.appliesto) AND (acls_a.utype = 'r'))";
                    $privileges = "SELECT pr_g.id gid, pr_g.title gtitle, pr_p.id pid, pr_p.position `position`, pr_p.title ptitle, pr_p.icon icon FROM (sys_privilege pr_g LEFT JOIN sys_privilege pr_p ON ((pr_g.id = pr_p.root))) WHERE ((pr_g.root = 0) AND (pr_g.active = 1) AND (pr_p.active = 1)) ORDER BY pr_g.position,pr_g.title,pr_p.link,pr_p.position";
                    $permission = "SELECT pe_p.id pid, pe_a.privilege privilege, pe_p.link link, pe_p.icon icon, pe_p.title title, pe_p.position position, pe_p.option `option`, pe_p.root root, pe_p.active active, pe_p.hidden hidden, pe_a.access access, pe_a.appliesto user, pe_p.controller controller, pe_p.show_in_frontpage show_in_frontpage FROM ($acls) pe_a JOIN sys_privilege pe_p WHERE (pe_a.privilege = pe_p.id)";
                    */
                    if(uid()==1){
                        $menus = select("*", "sys_privilege", "root=84 AND hidden=0", "ORDER BY  `position`");
                    } else{
                        //console_log("$permissions AND root=84 AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                        $menus = select("SELECT * FROM sys_permission WHERE root=84 AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                    }
                    while ($menu = mysqli_fetch_object($menus)) {
                        $menu_content .= "<li><a href='$menu->link'><i class='$menu->glyphicon'></i>".ucfirst($menu->title)."</a></li>";
                    }
                    print $menu_content;
                    //file_put_contents($menu_file, $menu_content);
                }
                ?>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
<script type="text/javascript">
    $(function(){
        $(".dropdown a").removeClass("active");
        $("#side-menu").show();
        $("#loader").hide();
        $(".dropdown").hover(function(){
            $(".dropdown").removeClass("open");
            $(this).addClass("open");
        });
        $(".navbar-static-top").mouseleave(function(){
            $(".dropdown").removeClass("open");
        });
    });
    function goto(uri){
        redir(uri);
    }

    function search(){
        var key = $("#key").val();
        cookie('key', key);
        if(key.length!=null){
            if(key.length>2){
                $.post("<?php print $appurl; ?>/framework/common/ajax/search.php", {'app':'<?php print APP; ?>',key:key}, function(data){
                    console.log(data);
                    if(data!=""){
                        $("#main-container").hide();
                        $("#search-result").html(data);
                        $("#search-result").show();
                    } else{
                        $("#main-container").show();
                        $("#search-result").hide();
                    }
                });
            } else{
                $("#main-container").show();
                $("#search-result").hide();
            }
        }
    }

    /*function search(){
        var key = $("#key").val();

        //var type = $("#stype").val();
        if(typeof(key)!='undefined'){
        if(!exists("#main-container")){
            var content = $("#main-container")();
            $("#main-container")("<div id='result'></div><div id='main-container'>" + content + "</div>");
        }
        cookie('key', key);
        if(key.length!=null)
        if(key.length>2){
            $.post("<?php print BASEURL; ?>/common/ajax/search.php", {'app':'<?php print APP; ?>',key:key}, function(data){
                if(data!=""){
                    $("#main-container").css("display","none");
                } else{
                    $("#main-container").css("display","block");
                }
                $("#main-container")(data);
            });
        } else{
            $("#result")("");
            $("#main-container").css("display","block");
        }
        }
    }*/
</script>
<?php endif; ?>
