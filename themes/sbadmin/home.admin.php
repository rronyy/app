<div class="row">
<?php
	pane("Total", "1200", "soa", "", "green");
	pane("Absent", "39", "soa", "", "primary");
	pane("Holiday", "24th May", "soa", "", "yellow");
	pane("News", "5", "soa", "", "red");

?>
</div>

<?php
	function pane($text_line1, $text_line2, $link, $icon='info', $color='primary'){
		print "
		<div class='col-lg-3 col-md-6'>
			<div class='panel panel-$color'>
				<div class='panel-heading'>
					<div class='row'>
						<div class='col-xs-3'>
							<i class='fa fa-$icon fa-5x'></i>
						</div>
						<div class='col-xs-9 text-right'>
							<div class='huge'>$text_line1</div>
							<div>$text_line2</div>
						</div>
					</div>
				</div>
				<a href='$link'>
					<div class='panel-footer'>
						<span class='pull-left'>View Details</span>
						<span class='pull-right'><i class='fa fa-arrow-circle-right'></i></span>
						<div class='clearfix'></div>
					</div>
				</a>
			</div>
		</div>";
	}
?>
