
                </div>
                <!-- /.row (nested) -->
            <!-- </div> -->
            <!-- /.panel-body -->
        <!-- </div> -->
        <!-- /.panel -->
    <!--/div-->
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<?php
 $jsfiles = array(//"core/vendor/jqueryplugins/validator.min.js",
    "core/vendor/jqueryplugins/moment.js",
    "core/vendor/bootstrap/js/bootstrap-datetimepicker.js");
foreach ($jsfiles as $jsfile) {
    print "<script src='$appurl/framework/$jsfile' type='text/javascript'></script>\n";
}
?>

<script type="text/javascript">
    //$("form").validator({"disable":false});
    $('.datetime').datetimepicker();
    // $('.date').datetimepicker({
    //     format: 'YYYY-MM-DD'
    // });
    $(".date").datepicker();
    $(".window-control-close").click(function(){
        location.href = "<?php print BASEURL.APP; ?>/home";
    });
    $(".window-control-back").click(function(){
        window.history.back();
    });

    $("#quick_filter,.quick_filter").keyup(function(){
        console.log($(this).val());
      var selectSize = $(this).val();
      quick_filter(selectSize);
    }).addClass("quick_filter");

    function quick_filter(e) {
        $('.quick-filter-item').hide();
        $(".quick-filter-item:contains('"+e+"')" ).show();
        $(".quick-filter-item:contains('"+e.toLowerCase()+"')" ).show();
    }
    $.each($(".currency"), function(e, i){
        $(this).before($(this).data("currency") != undefined? "<span>" + $(this).data("currency") + "</span>":"");
    });
</script>
