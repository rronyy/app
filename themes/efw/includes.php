<?php
    function formGroup($name, $options = array()){
        $type = isset($options['type'])?$options['type']:'text';
        $span = isset($options['span'])?$options['span']:12;
        if(isset($options['placeholder'])){
            $options['placeholder'] = nn($options['placeholder'])?$options['placeholder']:(isset($options['label'])?$options['label']:ucfirst($name));
        }
        print "<div class='form-group col-sm-$span'>
            <label for='$name' class='control-label'>".(isset($options['label'])?$options['label']:ucfirst($name))."</label>";
        if(in_array($type, array('text', 'number', 'password'))){
            print "<input type='$type' name='$name' class='form-control' id='input_$name' placeholder='".(isset($options['placeholder'])?$options['placeholder']:"Your input here...")."' ".(isset($options['length'])?"data-minlength='{$options['length']}'":"")." ".(isset($options['required'])?"required":"").">";
        } elseif(in_array($type, array('date', 'datetime'))){
            print "<div class='input-group date' class='$type'><input type='text' name='$name' class='form-control $type' id='input_$name' placeholder='".(isset($options['placeholder'])?$options['placeholder']:"Your input here...")."' ".(isset($options['length'])?"data-minlength='{$options['length']}'":"")." ".(isset($options['required'])?"required":"")."></div>";
        }  elseif($type=='cpassword'){
            print "<div class='form-inline row'>
                    <div class='form-group col-sm-6 col-lg-6'>
                        <input name='$name' type='password' data-toggle='validator' class='form-control' id='input_$name' placeholder='".(isset($options['placeholder'])?$options['placeholder']:"Your input here...")."' ".(isset($options['length'])?"data-minlength='{$options['length']}'":"")." ".(isset($options['required'])?"required":"").">
                        <span class='help-block'>Minimum of 6 characters</span>
                    </div>
                    <div class='form-group col-sm-6'>
                        <input type='password' class='form-control' id='input_{$name}Confirm' data-match='#input_$name' data-match-error='Whoops, these do not match' placeholder='Confirm' ".(isset($options['required'])?"required":"").">
                        <div class='help-block with-errors'></div>
                    </div>
                </div>";
        } elseif($type=='textarea'){
            print "<textarea name='$name' class='form-control' id='input_$name' placeholder='".(isset($options['placeholder'])?$options['placeholder']:"Your input here...")."' ".(isset($options['length'])?"data-minlength='{$options['length']}'":"")." ".(isset($options['required'])?"required":"")."></textarea>";
        } elseif($type=='select'){
            print "<select name='$name' class='form-control' id='input_$name' ".(isset($options['required'])?"required":"").">";
            if(isset($options['values'])){
                foreach($options['values'] as $key=>$value){
                    print "<option value='$key'>$value</option>";
                }
            } else{
                print "<option></option>";
            }
            print "</select>";
        } else{
            print "<input type='$type' name='$name' class='form-control' id='input_$name' placeholder='".(isset($options['placeholder'])?$options['placeholder']:"Your input here...")."' ".(isset($options['length'])?"data-minlength='{$options['length']}'":"")." ".(isset($options['required'])?"required":"").">";
        }
        print "
        </div>";
    }
    function formSeparator($text=""){
        print "<br clear='all' />";
    }
?>