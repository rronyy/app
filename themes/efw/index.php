<!DOCTYPE html>
<html lang="ar">

<head>
    <?php
        if(file_exists("favicon.ico")){
            print '<link rel="shortcut icon" href="favicon.ico">';
        } else{
            print '<link rel="shortcut icon" href="assets/logo.png">';
        }
    ?>
    <!-- <meta charset="utf-8"> -->
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php print APPTITLE; ?></title>
    <!-- Bootstrap Core CSS -->
    <?php
        $cssfiles = array("core/vendor/jquery/jquery-ui/jquery-ui.min.css",
            "core/vendor/bootstrap/css/bootstrap.min.css",
            "themes/sbadmin/bower_components/metisMenu/dist/metisMenu.min.css",
            "themes/sbadmin/dist/css/timeline.css",
            "themes/sbadmin/dist/css/sb-admin-2.css",
            "themes/sbadmin/bower_components/morrisjs/morris.css",
            "themes/sbadmin/bower_components/font-awesome/css/font-awesome.min.css",
            "core/vendor/bootstrap/css/bootstrap-select.min.css",
            "core/vendor/bootstrap-table/dist/bootstrap-table.min.css",
            "core/vendor/jqueryplugins/jquery.qtip.min.css",
            "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
            "core/vendor/fontawesome/font-awesome-animation.min.css",
            "core/vendor/animate.css/animate.min.css",
            "core/vendor/bootstrap-treeview/bootstrap-treeview.min.css",
            "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.css",
            "core/vendor/fullcalendar/fullcalendar.min.css",
            "core/vendor/panacea/css/styles.css");
        
    //<link rel="stylesheet" type="text/css" href="/assets/overhang.min.css" />

    if(loggedin()){
        foreach ($cssfiles as $cssfile) {
            print "<link href='$appurl/framework/$cssfile' rel='stylesheet'>\n";
        }

        if(file_exists("$appurl/assets/css/".theme().".css")){
            print "<link href='$appurl/assets/css/".theme().".css' rel='stylesheet'>";
        }
        if(file_exists("css/custom.css")){
            print "<link href='$appurl/css/custom.css' rel='stylesheet'>";
        }
        if(file_exists("assets/custom.css")){
            print "<link href='$appurl/assets/custom.css' rel='stylesheet'>";
        }
    }
    ?>
    <?php
        $jsfiles = array(
            "core/vendor/jquery/jquery.min.js",
            "core/vendor/vuejs/vue.js",
            "core/vendor/jquery/jquery-ui.min.js",
            "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
            "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
            "core/vendor/jqueryplugins/jquery.qtip.min.js",
            "core/vendor/jqueryplugins/jquery.cookie.js",
            "core/vendor/jqueryplugins/jquery.validate.min.js",
            "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
            "core/vendor/jquery.hotkeys/jquery.hotkeys.js",
            "core/vendor/panacea/js/scripts.js",
            "core/vendor/bootstrap/js/bootstrap-select.min.js",
            "core/vendor/bootstrap/js/bootstrap-typeahead.js",
            "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js",
            "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js",
            "core/vendor/bootstrap-table/dist/bootstrap-table.min.js",
            "core/vendor/highcharts/highcharts.js",
            "core/vendor/highcharts/exporting.js",
            "core/vendor/highcharts/grouped-categories.js",
            "core/vendor/bootstrap-treeview/bootstrap-treeview.min.js",
            "core/vendor/momentjs/moment.min.js",
            "core/vendor/fullcalendar/fullcalendar.min.js",
            "core/vendor/dropzonejs/dropzone.js"
            );
        foreach ($jsfiles as $jsfile) {
            if(loggedin()){
                print "<script src='$appurl/framework/$jsfile' type='text/javascript'></script>\n";
            }
        }
    ?>
</head>

<body>
<div id="wrapper">
    <?php 
        if(loggedin()){
		  require 'nav.php';
		  print '<div id="page-wrapper">';
		  require 'content.php';
		  print '</div>';
        } elseif (isset($module)){
            print "module"; require "$base/framework/modules/$module/index.php";
        } else{
            $controller = $module = $function = 'login';
            if(file_exists("$base/modules/login")){
                $app_module = "login";
            }
            //require 'content.php';
            if(file_exists("$base/modules/login/index.php")){
                require "$base/modules/login/index.php";
            } else{
                require "$base/framework/modules/login/index.php";
            }
        }
    ?>
<!-- /#page-wrapper -->
</div>
	<!-- /#wrapper -->

	<?php
    if(loggedin()){
        $jsfiles = array("core/vendor/bootstrap/js/bootstrap.min.js",
            "themes/sbadmin/bower_components/metisMenu/dist/metisMenu.min.js");
        foreach ($jsfiles as $jsfile) {
            print "<script src='$appurl/framework/$jsfile' type='text/javascript'></script>\n";
        }
        if($controller=='home'){
            $jsfiles = array("themes/sbadmin/bower_components/raphael/raphael-min.js",
                "themes/sbadmin/bower_components/morrisjs/morris.min.js",
                //"themes/sbadmin/js/morris-data.js",
                "themes/sbadmin/dist/js/sb-admin-2.js");
            foreach ($jsfiles as $jsfile) {
                print "<script src='$appurl/framework/$jsfile' type='text/javascript'></script>\n";
            }
        }
        $jsfiles = array("themes/sbadmin/dist/js/sb-admin-2.js");
        foreach ($jsfiles as $jsfile) {
            print "<script src='$appurl/framework/$jsfile' type='text/javascript'></script>\n";
        }

        if(file_exists("js/custom.js")){
            print "<script src='$appurl/css/custom.js' type='text/javascript'></script>";
        }
        if(file_exists("assets/custom.js")){
            print "<script src='$appurl/assets/custom.js' type='text/javascript'></script>";
        }
    }
    ?>


    <script>
        // $(document).ready(function() {
        //     $('.bootstrapTable').bootstrapTable();
        //     $('.dataTable').DataTable({
        //         responsive: true
        //     });
        //     $(".pdf").height($(".content-wrapper").height()+15);
        //     $(".pdf").width($(".content-wrapper").width()+15);
        // });
    </script>
</body>
</html>
