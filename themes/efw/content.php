<?php
if(!loggedin()):
	$controller = "login";
endif;
if($controller!='home'):

	if(isset($app_module)):
		$file = "$base/modules/$app_module/index"; console_log("2");
		$module = $app_module;
	elseif(isset($module)):
		$file = "$base/framework/modules/$module/index"; console_log("1");
		//require "$file.php";
	else:
		$file = "$base/controller/$controller"; console_log("3");
	endif;
	
	include "content_header.php";
	print "<div id='search-result' class='row'></div>";
	print "<div id='main-container'>";
	if(file_exists("$file.php")){
		if(hasAccess($controller, $function) || $controller=='login' || $controller=='logout'){
			include "$file.php";
		} else{
			print "<div class='alert alert-danger' role='alert'>ACCESS DENIED!, PLEASE CONTACT ADMINISTRATOR!!!</div>";
		}
	} else{
		print "<div class='alert alert-warning' role='alert'>2. RESOURCE NOT FOUND!</div>";
	}
	print "</div>";
	
	include "content_footer.php";
else:
	include "home.php";
endif;
?>