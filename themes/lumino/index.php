<?php
session_start();
require 'config.php';
require 'core/f.inc.php';
require 'core/function.php';
define('BASEDIR', "/".str_replace("/index.php", "", str_replace($_SERVER['DOCUMENT_ROOT'], "", $_SERVER['SCRIPT_FILENAME'])));
$get = array(); foreach ($_GET as $key => $value) { $get[$key] = $value; } unset($_GET); $get = (object)$get;
$post = array(); foreach ($_POST as $key => $value) { $post[$key] = $value; file_put_contents("post.log", "$key = $value".PHP_EOL, FILE_APPEND); } unset($_POST); $post = (object)$post;

$url_params = isset($get->q)?$get->q:'dashboard';
$params = explode("/", $url_params);
$page = $params[0];
define('PAGE', $page);
$function = isset($params[1])?$params[1]:false;
define('FUNCTION', $function);
$id = isset($params[2])?$params[2]:false;
define('ID', $id);

$authenticated = isset($_SESSION['logged_in']);
$type = $authenticated?$_SESSION['type']:false;
if(!$authenticated){
	if($page != 'register') {
			$page = 'login';
			if($function){
				$page = 'login_'.$function;
			}
	}
	//require 'pages/'.$page.'.php';
} elseif($page == 'login'){
		header("location:".BASEURL."/framework/dashboard");
} else{
	//require "includes/controller.php";


	//Load content
	//require 'pages/'.$page.'.php';

	// $content = "";
	// require 'pages/'.$page.'.php';
	// $function = $function?$function:'index';
	// die($content);
	// $controller = new $page($function);

}

$css_files = ['bootstrap.min.css', 'datepicker3.css', 'styles.css', 'bootstrap-editable.css', 'dropzone.css'];
if($type){
	array_push($css_files, "styles_$type.css");
}
$js_files_in_head = ['jquery-1.11.1.min.js', 'lumino.glyphs.js'];
$js_files_in_head_ie = ['html5shiv.js', 'respond.min.js'];
$js_files = ['bootstrap.min.js', 'bootstrap-datepicker.js', 'bootstrap-editable.min.js', 'moment.js', 'dropzone.js', 'chart.min.js', 'chart-data.js', 'easypiechart.js', 'easypiechart-data.js'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Legalisation <?php if($type) print " | ".ucfirst($type); ?></title>
<?php
	foreach($css_files as $css_file) print "<link href='".BASEURL."/framework/css/$css_file' rel='stylesheet'>";
	foreach($js_files_in_head as $js_file) print "<script src='".BASEURL."/framework/js/$js_file' type='text/javascript'></script>";
	if(count($js_files_in_head_ie)){
		print '<!--[if lt IE 9]>';
		foreach($js_files_in_head_ie as $js_file) print "<script src='".BASEURL."/framework/js/$js_file' type='text/javascript'></script>";
		print '<![endif]-->';
	}
?>
<style media="screen">
.panel-default .panel-heading {
	background: #30a5ff;
}
.panel-default .panel-heading a, .panel-default .panel-heading a:hover, .panel-default .panel-heading a:focus {
    color: #fff;
}
.panel-heading-accordion {
	height: 40px;
}
</style>
</head>
<body>

<?php require 'includes/content.php'; ?>

<?php foreach($js_files as $js_file) print "<script src='".BASEURL."/framework/js/$js_file' type='text/javascript'></script>"; ?>

	<script>
		$('#calendar').datepicker({
		});

		//$.fn.editable.defaults.mode = 'inline';

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){
		        $(this).find('em:first').toggleClass("glyphicon-minus");
		    });
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		});

	  $(document).ready(function() {
	    $.each($("td.editable"), function(){
				type = 'text';
				if($(this).hasClass("date")){
					type = 'combodate';
				} else if($(this).hasClass("select")){
					type = 'select';
				} else if($(this).hasClass("textarea")){
					type = 'textarea';
				}
				value = $(this)();
				$(this)("<a href='#' class='editable' data-pk='1' id='fullname' data-type='" + type + "' data-url='<?php print BASEDIR; ?>/post.php'>" + value + "</a>");

				el = $(this).find("a");
				if(defined($(this).data("id"))){
					$(el).attr("id", $(this).data("id"));
				}

				if(type=='select'){
					values = $(this).data("values").split(" ");
					$(el).editable({
			        value: value,
			        source: values,
							pk: "<?php print isset($id)?$id:''; ?>"
			    });
				} else{
					$(el).editable({
							pk: "<?php print isset($id)?$id:''; ?>"
			    });
				}
	    });
	  });
		$(".a-editable").editable();

		function defined(val){
			return val != undefined;
		}
	</script>

</body>
</html>

<?php
	function alink($link){
		return BASEURL."/framework/$link";
	}
 ?>
