<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
  <form role="search">
    <div class="form-group">
      <input type="text" class="form-control" placeholder="Search">
    </div>
  </form>
  <ul class="nav menu">
    <?php
      $menu_items = [
          'immigration'=>[
            ['Dashboard', 'dashboard', 'dashboard-dial'],
            '',
            ['Company', 'company/view', 'app-window-with-content'],
            ['Widgets', 'widgets', 'calendar'],
            ['Charts', 'charts', 'line-graph'],
            ['Tables', 'tables', 'table'],
            ['Forms', 'forms', 'pencil'],
            ['Panels', 'panels', 'app-window'],
            ['Icons', 'icons', 'star']
          ],
          'embassy'=>[
            ['Dashboard', 'dashboard', 'dashboard-dial'],
            '',
            ['Company', 'company/view', 'app-window-with-content'],
            ['Widgets', 'widgets', 'calendar'],
            ['Charts', 'charts', 'line-graph'],
            ['Tables', 'tables', 'table'],
            ['Forms', 'forms', 'pencil'],
            ['Panels', 'panels', 'app-window'],
            ['Icons', 'icons', 'star']
          ]
        ];

      foreach ($menu_items as $menu_item) {
        if(is_array($menu_item)){
          print "<li><a href='".BASEURL."/framework/{$menu_item[1]}'><svg class='glyph stroked {$menu_item[2]}'><use xlink:href='#stroked-{$menu_item[2]}'></use></svg> {$menu_item[0]}</a></li>";
        } else{
          print '<li role="presentation" class="divider"></li>';
        }
      }
    ?>
    <li role="presentation" class="divider"></li>
    <li><a href="my"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> My Account</a></li>
  </ul>

</div><!--/.sidebar-->
