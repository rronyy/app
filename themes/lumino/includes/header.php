<div class="content">
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="<?php print BASEURL."framework/dashboard"; ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active"><a href='<?php print BASEURL."framework/".$page; ?>'><?php print ucfirst($page); ?></a></li>
        <?php if($function): ?>
				<li class="active"><?php print ucfirst($function); ?></li>
        <?php endif; ?>
			</ol>
		</div><!--/.row-->
		<?php
			$page_title = getTitle();
			if($page_title):
		?>
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">
					<?php
						print $page_title;
						if($function == 'view' && file_exists("pages/view/$page.php")){
							print "<span class='pull-right'><a class='btn btn-success' href='".BASEURL."/framework/$page/add'><svg class='glyph stroked plus sign w16 h16'><use xlink:href='#stroked-plus-sign'/></svg>New ".ucfirst($page)."</a></span>";
						}
					?>
				</h1>

			</div>
		</div><!--/.row-->
	<?php endif; ?>

<?php
	function getTitle($object = '', $operation = ''){
		global $page;
		global $function;
		$object = $object == '' ? $page : $object;
		$operation = $operation == '' ? $function : $operation;

		if($object == 'worker' && $function == 'view'){
			return '';
		}

		return ($operation=='index'?"":ucfirst($operation))." ".ucfirst($page);
	}
?>
