<?php
if(!$authenticated || in_array($page, ['login', 'exit'])){
  require 'pages/'.$page.'.php';
} else{
  require 'includes/nav.php';
  require 'includes/sidebar.php';
  require 'includes/header.php';
  if(file_exists('pages/'.$page.'.php')){
      require 'pages/'.$page.'.php';
  } else{
    print '404: this feature not available in DEMO';
  }
  require 'includes/footer.php';
}
