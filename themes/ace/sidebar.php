<div class="sidebar" id="sidebar">
				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-small btn-success">
							<i class="icon-signal"></i>
						</button>

						<button class="btn btn-small btn-info">
							<i class="icon-pencil"></i>
						</button>

						<button class="btn btn-small btn-warning">
							<i class="icon-group"></i>
						</button>

						<button class="btn btn-small btn-danger">
							<i class="icon-cogs"></i>
						</button>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!--#sidebar-shortcuts-->

				<ul class="nav nav-list">
					<li class="active">
						<a href="index.php">
							<i class="icon-dashboard"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
					</li>
					<?php //var_dump($config);
					$menu_file = "temps/menus/menu.".APP.".".uid();
					if(file_exists($menu_file)){
							include $menu_file;
					} else{
							$menu_content = "";
							$root_menu = select("*", "sys_privilege", "root=0 AND hidden=0 AND id<>84", "ORDER BY position");
							$permissions = permissions();
							while ($root = mysqli_fetch_object($root_menu)) {
									if(uid()==1){
											$menus = select("*", "sys_privilege", "root=$root->id AND hidden=0", "ORDER BY  `position`");
									} else{
											//console_log("$permissions AND root=$root->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
											$menus = select("$permissions AND root=$root->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
									}
									if($menus->num_rows){
											$menu_content .= "<li><a href='#' class='dropdown-toggle'><i class='$root->glyphicon'></i><span class='menu-text'> ".ucfirst($root->title)."
												<i class='fa fa-angle-down'></i></a>
													<ul class='submenu'>";
											while ($menu = mysqli_fetch_object($menus)) {
													if(uid()==1){
															$menus3 = select("*", "sys_privilege", "root=$menu->id AND hidden=0", "ORDER BY  `position`");
													} else{
															//console_log("$permissions AND root=$menu->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
															$menus3 = select("$permissions AND root=$menu->pid AND hidden=0 AND user=".uid()." ORDER BY  `position`");
													}
													$menu_content .= "<li>
														<a href='".BASEDIR.(nn($menu->module)?"/$menu->module":"")."/$menu->link/$menu->option'>
															<i class='$menu->glyphicon'></i> $menu->title".($menus3->num_rows?"<span class='fa arrow'></span>":"")."
														</a>";
													if($menus3->num_rows){
															$menu_content .= "<ul class='nav nav-third-level'>";
																	while ($menu3 = mysqli_fetch_object($menus3)) {
																			$menu_content .= "<li><a href='".BASEDIR.(nn($menu3->module)?"/$menu3->module":"")."/$menu3->link/$menu3->option'><i class='$root->glyphicon fa-fw'></i>$menu3->title</a></li>";
																	}
															$menu_content .= "</ul>";
													}
													$menu_content .= "</li>";
											}
											$menu_content .= "</ul>
													</li>";
									}
							}
							print $menu_content;
							//file_put_contents($menu_file, $menu_content);
					}
					?>
				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>
