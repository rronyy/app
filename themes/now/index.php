<?php 
$page = 'home';
$file = $page;
if($controller!='home'):
    $page = $controller;
    if(isset($app_module)):
        $file = "$base/modules/$app_module/index"; //console_log("2");
        $module = $app_module;
        $page = $module;
    elseif(isset($module)):
        $file = "$base/framework/modules/$module/index"; //console_log("1");
        //require "$file.php";
    else:
        $file = "$base/controller/$controller"; //console_log("3");
    endif;
else:
    $page = 'home';
    $file = $page;
endif;


require "framework/themes/now/header.php";
print '<body class="'.$page.'-page sidebar-collapse">';
require "framework/themes/now/nav.php";
require "framework/themes/now/content.php";
print '</body>';
require "framework/themes/now/footer.php";
?>