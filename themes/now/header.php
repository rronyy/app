<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php print $appurl; ?>/framework/themes/now/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?php print $appurl; ?>/framework/themes/now/assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Cafe Book</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?php print $appurl; ?>/framework/themes/now/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php print $appurl; ?>/framework/themes/now/assets/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?php print $appurl; ?>/framework/themes/now/assets/css/demo.css" rel="stylesheet" />
    <link href="<?php print $appurl; ?>/framework/themes/now/assets/custom.css" rel="stylesheet" />
    <link href="<?php print $appurl; ?>/framework/vendor/pikaday/css/pikaday.css" rel="stylesheet" />
    <link href="<?php print $appurl; ?>/assets/custom.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="<?php print $appurl; ?>/framework/themes/now/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://unpkg.com/popper.js@1.14.3/dist/umd/popper.min.js"></script>
    <script src="<?php print $appurl; ?>/framework/themes/now/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php print $appurl; ?>/framework/vendor/tinymce/tinymce.min.js"></script>
    <script src="<?php print $appurl; ?>/framework/vendor/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php print $appurl; ?>/framework/vendor/pikaday/plugins/pikaday.jquery.js"></script>
</head>