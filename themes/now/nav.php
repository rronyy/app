    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-primary fixed-top" color-on-scroll="400">
      <div class="container">
        <div class="navbar-translate">
          <a class="navbar-brand" href="<?php print $appurl.(loggedin()?"/news":"/home"); ?>" rel="tooltip" title="Powered by Trybe" data-placement="bottom">
            <b>WORK</b> MODE
          </a>
          <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar bar1"></span>
            <span class="navbar-toggler-bar bar2"></span>
            <span class="navbar-toggler-bar bar3"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="./assets/img/blurred-image-1.jpg">
          <ul class="navbar-nav" style="margin-left: 0px">
            <?php 
              // $private = [
              //   ['link'=>'', 'title'=>'Newsfeed'],
              //   ['link'=>'', 'title'=>'Jobs'],
              //   ['link'=>'', 'title'=>'Events'],
              //   ['link'=>'', 'title'=>'Reservation'],
              //   ['link'=>'', 'title'=>'Docs'],
              //   ['link'=>'', 'title'=>'Cafe'],
              //   ['link'=>'', 'title'=>'Inbox'],
              //   ['link'=>'', 'title'=>'Me'],
              //   ['link'=>'', 'title'=>'Logout']
              // ];

            if(loggedin()): 

              ?>
              <li class="nav-item">
                <a class="nav-link" href="<?php print $appurl; ?>/news">
                 <i class="material-icons">local_library</i>
                 <p>Newsfeeds</p>
               </a>
             </li>
             <li class="nav-item">
              <a class="nav-link" href="<?php print $appurl; ?>/jobs">
               <i class="material-icons">people</i>
               <p>Jobs</p>
             </a>
           </li>
           <li class="nav-item">
            <a class="nav-link" href="<?php print $appurl; ?>/events">
             <i class="material-icons">date_range</i>
             <p>Events</p>
           </a>
         </li>
         <li class="nav-item">
          <a class="nav-link" href="<?php print $appurl; ?>/booking">
           <i class="material-icons">book</i>
           <p>Reservation</p>
         </a>
       </li>
     </li>
     <li class="nav-item">
      <a class="nav-link" href="<?php print $appurl; ?>/documents">
       <i class="material-icons">folder_open</i>
       <p>Docs</p>
     </a>
   </li>
   <li class="nav-item">
    <a class="nav-link" href="<?php print $appurl; ?>/cafe">
     <i class="material-icons">local_cafe</i>
     <p>Cafe</p>
   </a>
 <?php endif; ?>
</ul>
<ul class="navbar-nav">
  <?php if(!loggedin()): ?>
    <li class="nav-item">
      <a class="nav-link btn btn-neutral" href="<?php print $appurl; ?>/login/login">
        <i class="now-ui-icons arrows-1_share-66"></i>
        <p>Login</p>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php print $appurl; ?>/login/signup">
        <i class="now-ui-icons files_paper"></i>
        <p>Register</p>
      </a>
    </li>
    <?php else: ?>
      <?php $count = inboxCount(); ?>
      <li class="nav-item">
        <a class="nav-link <?php if($count) print "animated flash has-item"; ?>" href='inbox' style="cursor:pointer">
        <i class="material-icons">markunread_mailbox</i>
        <p>inbox <?php if($count) print "($count)"; ?></p>
      </a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" data-toggle="dropdown" id='meDropdown' href='profile' style="cursor:pointer">
        <i class="material-icons">account_circle</i>
        <p>me</p>
      </a>
        <div class="dropdown-menu" aria-labelledby="meDropdown">
            <a class="dropdown-item" href="profile">Profile</a>
            <hr>
            <a class="dropdown-item" href="job_applications">Job Applications</a>
            <a class="dropdown-item" href="my_bookings">Reservations</a>
        </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="<?php print $appurl; ?>/login/logout" style="cursor:pointer">
        <i class="material-icons">exit_to_app</i>
        <p>logout</p>
      </a>
    </li>
            <!-- <li>
                <div class='dropdown'>
                  <a class='dropdown-toggle pointer' data-toggle='dropdown' id='link$key'>
                      <i class='material-icons'>more_vert</i>
                  </a>
                  <ul class='dropdown-menu' aria-labelledby='link$key'>
                      <a class='dropdown-item' href='documents_view/$doc->id'>View</a>
                      <a class='dropdown-item' href='documents_download/$doc->id'>Download</a>
                  </ul>
              </div>  
            </li> -->
            <?php
            if(isset($_SESSION['order'])){
              ?>
              <li class="nav-item">
                <a class="nav-link" href='cart' style="cursor:pointer">
                  <i class="material-icons">shopping_cart</i>
                  <p>(<?php print isset($_SESSION['items'])?$_SESSION['items']:0; ?>)</p>
                </a>
              </li>                        
              <?php
            }
            ?>
          <?php endif; ?>

          
        </ul>
      </div>
    </div>
  </nav>
    <!-- End Navbar -->

<script type="text/javascript">
  if($(".has-item").length>0){
    setInterval(function(){
      if($(".has-item").hasClass("animated")){
        $(".has-item").removeClass("animated flash");
      } else{
        $(".has-item").addClass("animated flash");
      }
    }, 5000);
  }
</script>