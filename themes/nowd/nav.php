<ul class="nav">
                    <li class="active">
                        <a href="<?php print $appurl; ?>/home">
                            <i class="now-ui-icons design_app"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print $appurl; ?>/members/view">
                            <i class="now-ui-icons education_atom"></i>
                            <p>Members</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print $appurl; ?>/merchants/view">
                            <i class="now-ui-icons location_map-big"></i>
                            <p>Merchants</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print $appurl; ?>/user/user/view">
                            <i class="now-ui-icons users_single-02"></i>
                            <p>Admins</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print $appurl; ?>/wallets/view">
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p>Wallets</p>
                        </a>
                    </li>
                    <li>
                        <a href="<?php print $appurl; ?>/setup">
                            <i class="now-ui-icons text_caps-small"></i>
                            <p>Setup</p>
                        </a>
                    </li>
                    <li class="active-pro">
                        <a href="<?php print $appurl; ?>/">
                            <i class="now-ui-icons arrows-1_cloud-download-93"></i>
                            <p>My Account</p>
                        </a>
                    </li>
                </ul>