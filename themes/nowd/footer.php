
<!--   Core JS Files   -->
<script src="<?php print $appurl; ?>/framework/themes/nowd/assets/js/core/jquery.min.js"></script>
<script src="<?php print $appurl; ?>/framework/themes/nowd/assets/js/core/popper.min.js"></script>
<script src="<?php print $appurl; ?>/framework/themes/nowd/assets/js/core/bootstrap.min.js"></script>
<script src="<?php print $appurl; ?>/framework/themes/nowd/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chart JS -->
<script src="<?php print $appurl; ?>/framework/themes/nowd/assets/js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php print $appurl; ?>/framework/themes/nowd/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?php print $appurl; ?>/framework/themes/nowd/assets/js/now-ui-dashboard.js?v=1.0.1"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<?php
if($controller == 'home'){
	print "<script src='$appurl/framework/themes/nowd/assets/demo/demo.home.js'></script>";
} else{
	print "<script src='$appurl/framework/themes/nowd/assets/demo/demo.js'></script>";
}
?>

<script>
    $(document).ready(function() {
        <?php if($controller = 'home'): ?>
        demo.initDashboardPageCharts();
      	<?php endif; ?>
    });
</script>

</html>
