<?php 
if(loggedin()){
    require 'header.php';
    print "<body class='page-$controller'>
        <div class='wrapper '>
            <div class='sidebar' data-color='orange'>
                <!-- Tip 1: You can change the color of the sidebar using: data-color='blue | green | orange | red | yellow' -->
                <div class='logo'>
                    <a href='$appurl' class='simple-text logo-mini'>
                        <!-- Moolah -->
                    </a>
                    <a href='$appurl' class='simple-text logo-normal'>
                        Moolah
                    </a>
                </div>
                <div class='sidebar-wrapper'>";
                    require 'nav.php';
                print "</div>
            </div>
            <div class='main-panel'>";
                require 'content.header.php';
                print "<div class='content'>";
                    require 'content.php';
                print "</div>";
                require 'content.footer.php';
            print "</div>
        </div>
    </body>";
    require 'footer.php';
} else{
    $module = "login";
    require 'header.php';
    print "<body class='page-$controller'>";
    if(file_exists("modules/login")){
        require "modules/login/index.php";
    } else{
        require "framework/modules/login/index.php";
    }
    print "</body>";
    require 'footer.php';
}
