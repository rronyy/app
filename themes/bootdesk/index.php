<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php print APPTITLE; ?></title>
    <!-- Bootstrap Core CSS -->
    <?php
        $cssfiles = ["core/vendor/bootstrap/css/bootstrap.min.css",
                    "themes/sbadmin/bower_components/metisMenu/dist/metisMenu.min.css",
                    "themes/sbadmin/bower_components/font-awesome/css/font-awesome.min.css",
                    "core/vendor/bootstrap/css/bootstrap-select.min.css",
                    "core/vendor/jqueryplugins/jquery.qtip.min.css",
                    "themes/bootdesk/assets/styles.css",
                    "core/vendor/panacea/css/styles.css"];
        foreach ($cssfiles as $cssfile) {
            print "<link href='".BASEURL."/framework/$cssfile' rel='stylesheet'>\n";
        }

        if(file_exists("assets/css/".theme().".css")){
            print "<link href='".BASEURL."/framework/assets/css/".theme().".css' rel='stylesheet'>";
        }
    ?>
    <?php
        $jsfiles = ["core/vendor/jquery/jquery.min.js",
            "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
            "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
            "core/vendor/jqueryplugins/jquery.qtip.min.js",
            "core/vendor/jqueryplugins/jquery.cookie.js",
            "core/vendor/jqueryplugins/jquery.validate.min.js",
            "core/vendor/panacea/js/scripts.js",
            "themes/bootdesk/assets/scripts.js",
            "core/vendor/bootstrap/js/bootstrap-select.min.js",
            "core/vendor/bootstrap/js/bootstrap-typeahead.js"];
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
        }
    ?>
  </head>
  <body>
    <?php if(loggedin()): ?>
    <?php require 'nav.php'; ?>
    <div id="page-wrapper">
    <?php require 'themes/content.php';?>
    </div>
    <?php elseif (isset($module)): require "modules/$module/index.php"; ?>
    <?php else: require 'modules/login/login.php'; ?>
    <?php endif; ?>
    <!-- Javascripts -->
    <?php
      $jsfiles = ["core/vendor/bootstrap/js/bootstrap.min.js",
                "themes/sbadmin/bower_components/metisMenu/dist/metisMenu.min.js"];
      foreach ($jsfiles as $jsfile) {
          print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
      }
      if($controller=='home'){
          $jsfiles = ["themes/sbadmin/bower_components/raphael/raphael-min.js",
              "themes/sbadmin/bower_components/morrisjs/morris.min.js",
              //"themes/sbadmin/js/morris-data.js",
              ];
          foreach ($jsfiles as $jsfile) {
              print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
          }
      }
      $jsfiles = [];
      foreach ($jsfiles as $jsfile) {
          print "<script src='".BASEURL."/framework/$jsfile' type='text/javascript'></script>\n";
      }
    ?>
    <script type="text/javascript">

    </script>
    <!-- END Javascripts -->
  </body>
</html>
