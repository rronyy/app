
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><img class="" src="<?php print BASEURL."framework/assets/logo.png"; ?>" height='32px' alt="" /></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <!--li><a href="home">Home</a></li-->
          <?php //var_dump($config);
            $menu_file = "temps/menus/menu.".APP.".".uid();
            if(file_exists($menu_file)){
              include $menu_file;
            } else{
              $menu_content = "";
              $root_menu = select("*", "sys_privilege", "root=0 AND hidden=0 AND id<>84", "ORDER BY position");
              $permissions = permissions();
              while ($root = mysqli_fetch_object($root_menu)) {
                  if(uid()==1){
                      $menus = select("*", "sys_privilege", "root=$root->id AND hidden=0", "ORDER BY  `position`");
                  } else{
                      //console_log("$permissions AND root=$root->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                      $menus = select("$permissions AND root=$root->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                  }
                  if($menus->num_rows){
                      $menu_content .= "<li class='dropdown'>
                        <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>".ucfirst($root->title)."<span class='caret'></span></a>
                          <ul class='dropdown-menu'>";
                      while ($menu = mysqli_fetch_object($menus)) {
                          if(uid()==1){
                              $menus3 = select("*", "sys_privilege", "root=$menu->id AND hidden=0", "ORDER BY  `position`");
                          } else{
                              //console_log("$permissions AND root=$menu->id AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                              $menus3 = select("$permissions AND root=$menu->pid AND hidden=0 AND user=".uid()." ORDER BY  `position`");
                          }
                          $menu_content .= "<li><a href='#' class='dropdown-menu-item' data='".BASEDIR.(nn($menu->module)?"/$menu->module":"")."/$menu->link/$menu->option'><i class='$menu->glyphicon'></i> $menu->title".($menus3->num_rows?"<span class='fa arrow'></span>":"")."</a>";
                          if($menus3->num_rows){
                              $menu_content .= "<ul class='nav nav-third-level'>";
                                  while ($menu3 = mysqli_fetch_object($menus3)) {
                                      $menu_content .= "<li><a href='".BASEDIR.(nn($menu3->module)?"/$menu3->module":"")."/$menu3->link/$menu3->option'><i class='$root->glyphicon fa-fw'></i>$menu3->title</a></li>";
                                  }
                              $menu_content .= "</ul>";
                          }
                          $menu_content .= "</li>";
                      }
                      $menu_content .= "</ul>
                          </li>";
                  }
              }
              if(uid()==1){
                $menus = select("*", "sys_privilege", "root=84 AND hidden=0", "ORDER BY  `position`");
              } else{
                $menus = select("$permissions AND root=84 AND hidden=0 AND user=".uid()." ORDER BY  `position`");
              }
              while ($menu = mysqli_fetch_object($menus)) {
                $menu_content .= "<li><a href='$menu->link'><i class='$menu->glyphicon'></i>".ucfirst($menu->title)."</a></li>";
              }
              print $menu_content;
              //file_put_contents($menu_file, $menu_content);
            }
            ?>
          </ul>
          <form class="navbar-form navbar-left">
            <div class="form-group">
              <input type="text" class="form-control w150" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
          <!--ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Help <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Online Help</a></li>
                <li><a href="#">About</a></li>
              </ul>
            </li>
          </ul-->
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <script type="text/javascript">
      $(".dropdown-menu-item").click(function(){
        location.href = $(this).attr("data");
      });
      function closeWindow(){
      	location.href = "<?php print BASEURL."framework/home"; ?>";
      }
    </script>
