<?php
//session_start();
//require 'config/gcf.php';
//require 'system/f.php';
//require 'system/rb.php';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="product" content="Metro UI CSS Framework">
    <meta name="description" content="Simple responsive css framework">
    <meta name="author" content="Sergey S. Pimenov, Ukraine, Kiev">

    <link href="css/metro-bootstrap.css" rel="stylesheet">
    <link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
    <link href="css/iconFont.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="css/jquery-ui.theme.min.css" rel="stylesheet">
    <link href="js/prettify/prettify.css" rel="stylesheet">

    <!-- Load JavaScript Libraries -->
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/jquery/jquery-ui.min.js"></script>
    <script src="js/jquery/jquery.widget.min.js"></script>
    <script src="js/jquery/jquery.mousewheel.js"></script>
    <script src="js/prettify/prettify.js"></script>

    <!-- Metro UI CSS JavaScript plugins -->
    <script src="js/load-metro.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
    

    <title>PanAcc</title>
    <style type="text/css">
        .ui-autocomplete-loading {
            background: white url("css/images/ui-anim_basic_16x16.gif") right center no-repeat;
          }
    </style>
</head>
<body class="metro">
    <?php require "menu.php"; ?>
    <div class="container">
    <?php 
    R::setup('mysql:host='.$host.';dbname='.$database,$user,$password); R::freeze(true); 
    if($_GET['q']=='invoice'){
        require 'sales_invoice.php';
    }
    ?>
    </div>

    <script src="js/scripts.js"></script>
</body>
</html>
