<?php require 'includes.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php print APPTITLE; ?></title>
    <style media="screen">
        #canvas{
            display: none;
        }
        .container-fluid {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
    </style>
    <!-- Bootstrap Core CSS -->
    <?php
        $cssfiles = array("core/vendor/jquery/jquery-ui/jquery-ui.min.css",
            "core/vendor/bootstrap/css/bootstrap.min.css",
            "themes/sbadmin/bower_components/metisMenu/dist/metisMenu.min.css",
            "themes/sbadmin/dist/css/timeline.css",
            "themes/materialize_ori/css/materialize.min.css",
            "themes/sbadmin/dist/css/sb-admin-2.css",
            "themes/sbadmin/bower_components/morrisjs/morris.css",
            "themes/sbadmin/bower_components/font-awesome/css/font-awesome.min.css",
            "core/vendor/bootstrap/css/bootstrap-select.min.css",
            "core/vendor/jqueryplugins/jquery.qtip.min.css",
            "core/vendor/jqueryplugins/jquery.modal/css/jquery.modal.css",
            "core/vendor/fontawesome/font-awesome-animation.min.css",
            "core/vendor/panacea/css/styles.css");
        foreach ($cssfiles as $cssfile) {
            print "<link href='".BASEURL."framework/$cssfile' rel='stylesheet'>\n";
        }

        if(file_exists("assets/css/".theme().".css")){
            print "<link href='assets/css/".theme().".css' rel='stylesheet'>";
        }
    ?>
    <?php
        $jsfiles = array(
            "core/vendor/jquery/jquery.min.js",
            "core/vendor/jquery/jquery-ui.min.js",
            "themes/sbadmin/bower_components/datatables/media/js/jquery.dataTables.min.js",
            "themes/sbadmin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js",
            "core/vendor/jqueryplugins/jquery.qtip.min.js",
            "core/vendor/jqueryplugins/jquery.cookie.js",
            "core/vendor/jqueryplugins/jquery.validate.min.js",
            "core/vendor/jqueryplugins/jquery.jqprint-0.3.js",
            "core/vendor/panacea/js/scripts.js",
            "core/vendor/bootstrap/js/bootstrap-select.min.js",
            "core/vendor/bootstrap/js/bootstrap-typeahead.js",
            "core/vendor/jqueryplugins/jquery.modal/js/jquery.modal.min.js",
            "core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js","core/vendor/bootstrap_plugins/bootstrap-toggle/bootstrap-toggle.min.js",
            "core/vendor/highcharts/highcharts.js",
            "core/vendor/highcharts/exporting.js"
            );
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."framework/$jsfile' type='text/javascript'></script>\n";
        }
    ?>
</head>

<body>
<canvas id="canvas"></canvas>
<div id="wrapper">
    <?php if(loggedin()): ?>
		<?php require 'nav.php'; ?>
		<div id="page-wrapper">
		<?php require 'content.php';?>
		</div>
    <?php elseif (isset($module)): require ROOT."/framework/modules/$module/index.php"; ?>
    <?php else: require ROOT.'/framework/modules/login/login.php'; ?>
    <?php endif; ?>
<!-- /#page-wrapper -->
</div>
	<!-- /#wrapper -->

	<?php
        $jsfiles = array("core/vendor/bootstrap/js/bootstrap.min.js",
            "themes/sbadmin/bower_components/metisMenu/dist/metisMenu.min.js");
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."framework/$jsfile' type='text/javascript'></script>\n";
        }
        if($controller=='home'){
            $jsfiles = array("themes/sbadmin/bower_components/raphael/raphael-min.js",
                "themes/sbadmin/bower_components/morrisjs/morris.min.js",
                //"themes/sbadmin/js/morris-data.js",
                "themes/sbadmin/dist/js/sb-admin-2.js");
            foreach ($jsfiles as $jsfile) {
                print "<script src='".BASEURL."framework/$jsfile' type='text/javascript'></script>\n";
            }
        }
        $jsfiles = array("themes/sbadmin/dist/js/sb-admin-2.js");
        foreach ($jsfiles as $jsfile) {
            print "<script src='".BASEURL."framework/$jsfile' type='text/javascript'></script>\n";
        }

    ?>


    <script>
        $(document).ready(function() {
            $('.dataTables').DataTable({
                responsive: true
            });
            $(".pdf").height($(".content-wrapper").height()+15);
            $(".pdf").width($(".content-wrapper").width()+15);
        });
    </script>
</body>
</html>
