<!--table class="table table-responsive table-striped table-bordered"-->
<table class="table table-bordered table-hover table-condensed dataTable no-footer" role="grid">
<tr><th>Application</th><th>Description</th><th></th></tr>
<?php
if($controller){
	if($rc->exists($controller)){
		$_SESSION['app'] = $controller;
		file_put_contents(".app", $_SESSION['app']);
		redir("../");
	} else{
		print("<div class='alert alert-danger' role='alert'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>The specified Application does not exists!</div>");
	}
}

$dir = "applications";
if ($dh = opendir($dir)) {
	while (($file = readdir($dh)) !== false) {
		if (filetype($dir."/".$file)=="dir" && $file!="." && $file!="..") {
			print "<tr><td><a href='".BASEURL."/framework/$module/$file'>".strtoupper($file)."</a></td><td>";
			if(file_exists("$dir/$file/description.md")){
				include("$dir/$file/description.md");
			}
			print "</td><td><a href='".BASEURL."/framework/$module/$file'><i class='glyphicon glyphicon-upload'></i> Load</td></tr>";
		}
	}
	closedir($dh);
}

?>
</table>
