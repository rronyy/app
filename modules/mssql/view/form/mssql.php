<?php
$conn = odbc_connect("Driver={".MS_DRIVER."};Server=".MS_SERVER.";Database=".MS_DATABASE.";", MS_USER, MS_PASSWORD);
$tables = odbc_exec($conn, "SELECT * FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE'");

print "<table class='table w100p'>";
	print "<tr>";
		print "<td class='w50p'>";
			print "<select id='mssql_tables' class='form-control required' size=10>";
			while($table = odbc_fetch_object($tables)){
				print "<option>$table->TABLE_NAME</option>";
			}
			print "</select>";
		print "</td>";
		print "<td class='w50p'>";
			print selectTables("name='schemaname' id='mysql_tables' class='form-control required' size=10'");
			print "</select>";
		print "</td>";
	print "</tr>";
	print "<tr>";
		print "<td id='mssql_table_fields'>";
		print "</td>";
		print "<td id='mysql_table_fields'>";
		print "</td>";
	print "</tr>";
print "</table>";

function selectTables($attr){
	$tables = tables();
	$content = "<select $attr>";
	foreach($tables as $table){
		$content .= "<option>$table</option>";	
	}
	return $content.="</select>";
}
?>
<script type="text/javascript">
	$("#mssql_tables").change(mssql_table_changed);
	$("#mysql_tables").change(mysql_table_changed);

	function mssql_table_changed(){
		$("#mssql_table_fields").text($("#mssql_tables option:selected").val());
	}

	function mysql_table_changed(){
		$.post("<?php print BASEDIR; ?>/common/ajax/getFields.php", { 'schema': $(this).val(), 'type': 'form' }, function (data) {
            $("#mysql_table_fields")(data);
        });
	}
</script>