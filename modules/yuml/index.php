<style type="text/css">
  textarea.diagram-code {
    width: 100%;
    border-radius: 4px;
    padding: 4px;
  }

  .diagram-container {
    position: relative;
    background-color: rgba(255,255,255,.45);
    border-radius: 4px;
    text-align: center;
    min-height: 300px;
  }

  button {
    pointer: cursor;
  }
   
  .spinner.ng-hide-add,
  .spinner.ng-hide-remove {
    -webkit-transition:all linear 0.5s;
    -moz-transition:all linear 0.5s;
    -o-transition:all linear 0.5s;
    transition:all linear 0.5s;
    display:block!important;
  }
   
  .spinner.ng-hide-add.ng-hide-add-active,
  .spinner.ng-hide-remove {
    opacity:0;
  }
   
  .spinner.ng-hide-add,
  .spinner.ng-hide-remove.ng-hide-remove-active {
    opacity:1;
  }


  /* From Spinkit by Tobias Ahlin: http://tobiasahlin.com/spinkit/ */
  .spinner {
    width: 40px;
    height: 40px;
    position: absolute;
    top: 12px;
    left: 12px;
    margin: auto;
  }

  .double-bounce1, .double-bounce2 {
    width: 100%;
    height: 100%;
    border-radius: 50%;
    background-color: #FFF;
    opacity: 0.6;
    position: absolute;
    top: 0;
    left: 0;
    
    -webkit-animation: bounce 2.0s infinite ease-in-out;
    animation: bounce 2.0s infinite ease-in-out;
  }

  .double-bounce2 {
    -webkit-animation-delay: -1.0s;
    animation-delay: -1.0s;
  }

  @-webkit-keyframes bounce {
    0%, 100% { -webkit-transform: scale(0.0) }
    50% { -webkit-transform: scale(1.0) }
  }

  @keyframes bounce {
    0%, 100% { 
      transform: scale(0.0);
      -webkit-transform: scale(0.0);
    } 50% { 
      transform: scale(1.0);
      -webkit-transform: scale(1.0);
    }
  }
</style>
<div ng-app="myApp">
  <div ng-controller="MainController">    
    <div>
      <textarea class="diagram-code"
                ng-model="diagramCode"
                placeholder="Type your diagram code here"
      ></textarea>
    </div>
    
    <div class="diagram-container">
      <div ng-show="hasChanges"
           class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
      <img ng-src="{{ imageURL }}">
    </div>
    
  </div>
</div>
<div><select id='type'><option>class</option><option>activity</option><option>usecase</option></select>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.20/angular.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.20/angular-animate.min.js"></script>
<script type="text/javascript">
  angular.module('myApp', [
    'ngAnimate'
  ])
  .service('yumlService', [function() {
    
    var scope;
    this.baseURL = 'http://yuml.me/diagram/scruffy/';

  }])
  .controller('MainController',
  ['$scope', 'yumlService', function($scope, yumlService) {
    $scope.hasChanges = false;
    
    $scope.updateURL = _.debounce(function(newVal) {
      $scope.$apply(function() {
        console.log('hello');
        if(!(newVal === undefined)) {
          $scope.imageURL = yumlService.baseURL + $("#type").val() + '/' + newVal; 
        }
        $scope.hasChanges = false;
      });
    }, 1250);
    
    $scope.$watch('diagramCode', function(newVal, oldVal) {
      $scope.hasChanges=true;
      console.log('hello');
      $scope.updateURL(newVal);
    });

  }]);
</script>