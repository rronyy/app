<?php
if(isset($post->email) && isset($post->login)){
  //$url = BASEURL."/framework/home";
	$url = BASEURL.APP; 
  if(isset($post->url)){$url .= $post->url;} else{ $url = "home"; }
  //die($url);
	if (_checkLogin($post->email,md5($post->password), false)) {
		global $url;
		global $c;
		$_SESSION[APP.'_loggedin'] = true;
		$_SESSION[APP.'_username'] = $post->email;
		update("sys_user", "u_last_login_time = NOW(), u_loggedin = 1, u_last_ip='{$_SERVER['REMOTE_ADDR']}'", " id = '".uid()."'");

    if(isset($get->q)){
      if($get->q == 'pos'){
		    redir(BASEURL.APP."/pos");  
      } else{
        redir(BASEURL.APP."/".$get->q);  
      }
    } else{
      redir($url);
    }
	} else {
		$msg = 'Failed to login! Please retry with right credentials...';
		//redir($url);
	}
} elseif(isset($post->email) && isset($post->recover)){
	extract($_POST);
	$user = R::findOne("sys_user", "u_username=?", array($email));
	if($user){
		$code = acode(12, "sys_user", "u_recover");
		$user->u_recover = $code;
		R::store($user);
		$ecode = base64_encode($code);
		$body = "Thank you for the request. Kindly click  <a href='".BASEURL."/framework/register?recover=$ecode'>here</a> to recover your password.";
		email($email, "CKRI: Password Recovery", $body, $attachment = '');
		print "<div class='row row-login'>
			<div class='col-md-3'>
			</div>
			<div class='col-md-5'>
				<div class='well login-reg-form'>
					<p>Thank you for the request.</p><div class='well login-reg-form'>
					<p align='center'>Kindly check your email ($email) to recorver your password.</p>
			</div>
			</div>
			</div>";
	} else{
		print "<div class='row row-login'>
			<div class='col-md-3'>
			</div>
			<div class='col-md-5'>
				<div class='well login-reg-form'>
					<p>Invalid</p><div class='well login-reg-form'>
					<p align='center'>Sorry there is no user associated with $email.</p>
			</div>
			</div>
			</div>";
	}
}
/*
if(!loggedin() && !isset($post->recover)){
?>
	<div class="container">
    <div class="row login-block-wrapper">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                  <div class='center sm-only'><img src="assets/logo.png" width='64px' /></div>
                    <h3 class="panel-title"><?php print APP=='flightingale'?"Your Travel Assistant...":"Please log in"; ?></h3>
                </div>
                <div class="panel-body">
                	<p style='color:red'><?php print $msg; ?></p>
                    <form role="form" method="post" action="<?php if(isset($_GET['register'])) print "register?pre"; ?>" autofill="off" autocomplete="off">
                    	<input type='hidden' name='url' value='<?php print url(); ?>' >
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Username" name="email" type="text" autofocus >
                            </div>
                            <?php if(!isset($post->forgotpassword)): ?>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <?php endif; ?>
                            <?php if(!isset($_GET['register'])): ?>
																<?php if(isset($post->forgotpassword)): ?>
                                <button type="submit" name='recover' class="btn btn-primary">Reset Password</button>
                              <?php else: ?>
                              <button type="submit" name='login' class="btn btn-lg btn-success btn-block login">Login</button>
                              <button type="submit" name='forgotpassword' class="btn btn-lg btn-danger btn-block forgotpassword">Forgot Password?</button>
                              <?php endif; ?>
                            <?php else: ?>
                            <button type="submit" name='register' class="btn btn-success">Register</button>
                            <?php endif; ?>
                        </fieldset>
                    </form>
                    <div class='pull-right'><small><?php print APPTITLE . ' ' . APPVERSION; ?></small>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
}
*/

function _checkLogin($username, $password, $remember) {
	$user = select("u.id AS id, r.id as role, r_name as role_name, u_fullname, u_pin", "sys_role r, sys_user_role ur, `sys_user` u", "u_username = '{$username}' AND u_password = '{$password}' AND r.id=ur_role_id AND u.id=ur_user_id AND u_status = 1 AND r_active = 1 ");
	if ($user->num_rows>0){
		//setcookie("show", 1);
		$row = mysqli_fetch_object($user);
		$_SESSION[APP.'_id'] = $row->id;
		$_SESSION[APP.'_role'] = $row->role;
		$_SESSION[APP.'_role_name'] = $row->role_name;
    $_SESSION[APP.'_fullname'] = $row->u_fullname;
    $_SESSION[APP.'_pin'] = $row->u_pin;
    update("sys_user", "u_failed_attempt = 0", "`id` = '$row->id'");
		return true;
	} else {
		$user = select("u_failed_attempt, u_status", "`sys_user_role` ur, `sys_user` u", "u.id=ur_user_id AND u_username = '{$username}' AND u_status = 1");
		if($user->num_rows){
			$u = mysqli_fetch_object($user);
			if($u->u_failed_attempt >= 3 && $u->u_status == 1){
				update("sys_user", "u_status = 0", "`u_username` = '{$username}'");
			} else{
				$failed_attempt = $u->u_failed_attempt + 1;
				update("sys_user", "u_failed_attempt=$failed_attempt", "`u_username` = '{$username}'");
			}
		} else{
			$attempts = isset($_SESSION[APP.'_attempts_count'])?$_SESSION[APP.'_attempts_count']:0;
			$attempts++;
			insert("sys_fraud_user", "`f_username`, `f_password`, `f_ip`, `f_attempts`, `f_date`", "'{$username}', '{$password}', '{$_SERVER['REMOTE_ADDR']}', $attempts, NOW()");
		}
		return false;
	}
}
?>
