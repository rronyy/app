<?php
if(file_exists("framework/modules/$module/$controller.php")){
	include("framework/modules/$module/$controller.php");
} elseif(file_exists("framework/modules/$module/$module.php")){
	$controller = $module;
	include("framework/modules/$module/$module.php");
} else{
	include '404.php';
}