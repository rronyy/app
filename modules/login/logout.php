<?php
	if (session_status() == PHP_SESSION_NONE) {
	    session_start();
	}
	$_SESSION[APP.'_loggedin'] = false;
	unset($_SESSION[APP.'_loggedin']);
	unset($_SESSION);
	//session_destroy();
	if(isset($get->pos)){
		redir(BASEURL.APP."/pos");	
	}
	redir(BASEURL.APP);