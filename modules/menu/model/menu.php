<?php
$prev_root = $object->root;
$object->name = $post->name;
if(nn($post->title)) $object->title = $post->title;
$object->root = $post->parent;
if(nn($post->controller)) $object->link = $post->controller;
if(nn($post->function)) $object->option = $post->function;
$object->glyphicon = $post->glyphicon;
$object->position = $post->position;
$object->active = isset($post->active)?1:0;
$object->hidden = isset($post->hidden)?1:0;
$object->show_in_frontpage = 0;
R::store($object);
if(isset($post->change_siblings_parent)){
	update("sys_privilege", "root=$post->parent", "root=$prev_root AND link='$object->link'");
}
menuUpdated();