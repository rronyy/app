<?php
if($id){
	$root = R::load("sys_privilege",$id);
	$root_id = $root->root;
} else{
	$root = R::load("sys_privilege",0);
	$root_id = 0;
}

if(uid()==1){
	$menus = select("p.*, r.title AS rtitle", "(sys_privilege p, sys_privilege r)", "r.id=p.root AND p.root=$root->id", "ORDER BY link, p.position");
} elseif($root==0){
	$menus = select("*", "sys_privilege", " id IN (SELECT DISTINCT root FROM sys_permission WHERE `user`=".uid().")", "ORDER BY link, `position`"); 
} else{
	$menus = select("p.*", "(sys_privilege p, sys_permission a)", "p.id = a.privilege AND p.root=$root->id AND `user`=".uid(), "ORDER BY link, `position`"); 
	//$menus = select("p.*", "(sys_privilege p, sys_permission a)", "p.id = a.privilege AND `user`=".uid()." AND depends_on IS NULL", "ORDER BY `position`, link");
}
$root_menus = toArray("id,title", "sys_privilege", "root=$root_id", "id", "title");
$root_menus[0] = 'ROOT';
?>
<table class="grid" align="center" width="100%">
<tr class="head"><th width="20px"><input type="checkbox" /></th><th>No.</th><th>Name</th><th>Title</th><th>Parent</th><th>Object</th><th>Function</th><th>Icon</th><th>Position</th><th>Options</th></tr>
<?php
	$i = 1;
	while($menu = mysqli_fetch_object($menus)){
		echo "<tr><form method='post' action='menu/update/$root'>";
		echo "<td><input type='checkbox' /></td>";
		echo "<td>$i</td>";		
		if($function=='edit' && $menu->id==$_GET['id']){	
			echo "<td>
				<input type='hidden' name='id' value='$menu->id' />
				<input type='hidden' name='url' value='menu?f=menu/view/$root' />
				<input type='text' name='name' value='$menu->name' size='12' /></td>
				<td><input type='text' name='title' value='$menu->title' size='12' /></td>
				<td><select name='root'><option value='0'>Root</option>";
				if(uid()==1){
					$roots = select("*", "sys_privilege", "root=0", "ORDER BY position");
				} else{ 
					$roots = select("p.*", "sys_privilege p, sys_permission a", "p.id = a.privilege AND root=46 AND `user`=".uid()." AND depends_on IS NULL", "ORDER BY `position`");
				}
				while($r=mysqli_fetch_object($roots)){
					echo "<option value='$r->id' ";
					if($r->id==$menu->root) echo " selected ";
					echo ">$r->title</option>";
				}
				echo "</select></td>
				<td><input type='text' name='link' value='$menu->link' size='12' /></td>
				<td><input type='text' name='option' value='$menu->option' size='12' /></td>
				<td><input type='text' name='icon' value='$menu->icon' size='12' /></td>";
		} else{
			echo "<td><a href='".makeUri("$module/$controller", $function, $menu->id)."'>$menu->name</a></td>";			
			echo "<td>$menu->title</td>";
			echo "<td>".$root_menus[$menu->root]."</td>";
			echo "<td>$menu->link</td>";
			echo "<td>$menu->option</td>";
			echo "<td><i class='$menu->glyphicon'></i></td>";
		}
		echo "<td><input type='hidden' id='id{$menu->id}' value='{$menu->id}' />
			<select id='position{$menu->id}' onchange='updatepostion($menu->id)'>";
		for($p=-99;$p<=99; $p++){
			echo "<option ";
			if($p==$menu->position) {echo " selected ";}			
			echo ">$p</option>";
		}
		echo "</select></td>";
		if($function=='edit' && $menu->id==$_GET['id']){
			echo "<td><input type='submit' value='Update' /></td>";
		} else{
			echo "<td>".options2("", $menu->id, array("edit", ($menu->hidden?"show":"hide"),
				($menu->show_in_frontpage?"remove_front":"front"),
				($menu->active?"deactivate":"activate"),
				($menu->controller?"not_controller":"set_controller"),
				'remove'))."</td></form></tr>";
		}
		$i++;
	}
	echo "<tr class='head'><th colspan='11'>".(hasAccess('menu', 'add')?options2("", $id, array("add")):"")."</th></tr>";//options($object = '', $id = false, $not = array())
	echo "</table>";
if($id) {
	$parent = R::load("sys_privilege", $root);
	echo "<a href='".makeUri("$module/$controller", $function, $root_id)."'><i class='glyphicon glyphicon-chevron-left'></i> Back</a>";
} 

?>
<script type="text/javascript">
function updatepostion(id){
	$.post("<?php print BASEURL."framework/modules/$module"; ?>/ajax/menu_update_position.php", 
		{ 
			id: $("#id"+id).val(), 
			position: $("#position"+id).val() , 
			app: "<?php print APP; ?>" 
		},
		function(data){
			location.href="<?php print makeUri("$module/$controller", "view", $id); ?>";
	});
}
function msubmit(){
	$("#m-site").submit();	
}
</script>