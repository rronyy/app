<?php
//formWrapperStart('Add Menu', 'Menu Details');
openForm();
print "<br /><table align='center'>";
print "<tr><td colspan='5'><b>Menu Details</b></td></tr>
  <tr>
    <td>Name </td><td><input type='text' required name='name' class='form-control' value='".($object->name?$object->name:(isset($get->link)?title("$get->option $get->link"):''))."' onkeyup='\$(\"#title\").val(this.value)' /></td><td>".space(5)."</td>
    <td>Title </td><td><input type='text' name='title' id='title' class='form-control' value='".($object->title?$object->title:(isset($get->link)?title("$get->option $get->link"):''))."'/></td>
  </tr>
  <tr>
    <td>Controller </td><td><input type='text' name='controller' class='form-control' value='".($object->link?$object->link:(isset($get->link)?$get->link:''))."'/></td><td>".space(5)."</td>
    <td>Function</td><td><input type='text' name='function' class='form-control' value='".($object->option?$object->option:(isset($get->option)?$get->option:''))."'/></td><td>".space(5)."</td>
  </tr>
  <tr>
    <td>Parent</td><td>".selectOption("name='parent' class='form-control selectpicker' data-live-search='true'","sys_privilege", "title", "id", ($function=='edit'?$object->root:$id), "", '', true)."</td><td>".space(5)."</td>
    <td></td><td></td>
  </tr>
  <tr>
    <td>Active</td><td><input type='checkbox' name='active' class='form-control' ".($object->active?'checked':'')." /></td><td>".space(5)."</td>
    <td>Hidden?</td><td><input type='checkbox' name='hidden' class='form-control' ".($object->hidden?'checked':'')." /></td>
  </tr>
  <tr>
      <td>Glyphicon </td><td>";
      print "<select class='form-control selectpicker' name='glyphicon' data-live-search='true'>";
      $icons = select("*", "_icons", '', 'ORDER BY icon');
      while ($icon=mysqli_fetch_object($icons)) {
        print "<option data-icon='$icon->icon'";
        if($icon->icon == $object->glyphicon){
          print " selected";
        }
        print ">$icon->icon</option>";
      }
      print "</select>";

print "</td><td>".space(5)."</td>
      <td>Position </td><td>".createSelectOption("name='position' class='form-control'", -99, 99, $object->position)."</td>
    </tr>
    <tr><td colspan='2'>Change Parent of Siblings?</td><td colspan='2'><input type='checkbox' name='change_siblings_parent' class='form-control' /></td></tr>
    </table>";
closeForm();
?> 