<?php 
if(!loggedin()) die("You don't have access to this feature!");
?>
<?php
$object = R::dispense('sys_privilege');
$function = $function?$function:"view";
switch ($function){
	case 'front':{
		$object = R::load('sys_privilege', $id);
		$object->show_in_frontpage = 1;
		R::store($object);
		deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'remove_front':{		
		$object = R::load('sys_privilege', $id);
		$object->show_in_frontpage = 0;
		R::store($object);
		deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'hide':{
		$object = R::load('sys_privilege', $id);
		$object->hidden = 1;
		R::store($object);
		deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'show':{		
		$object = R::load('sys_privilege', $id);
		$object->hidden = 0;
		R::store($object);
		deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'activate':{		
		$object = R::load('sys_privilege', $id);
		$object->active = 1;
		R::store($object);
		deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'deactivate':{		
		$object = R::load('sys_privilege', $id);
		$object->active = 0;
		R::store($object);
		deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'set_controller':{		
		$object = R::load('sys_privilege', $id);
		$object->controller = 1;
		R::store($object);
		//deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'not_controller':{		
		$object = R::load('sys_privilege', $id);
		$object->controller = 0;
		R::store($object);
		//deleteAllFiles('temps/menus/');
		redir(makeUri($controller, 'view', $object->root));
	} break;
	case 'remove':{
		if(isset($get->conf)){
			$object = R::load('sys_privilege', $id);
			del("sys_privilege", "id=$id");	
			del("sys_privilege", "root=$id");	
			deleteAllFiles('temps/menus/');
			redir(makeUri("$module/$controller", 'view', $object->root));
		} else{
		?>
        <script type="text/javascript">
			if(confirm("Are you sure you want to remove this menu and sub-menus?")){
				location.href = "?<?php echo uri();?>&conf";
			} else{
				location.href = "?f=view<?php echo isset($get->menu)?"&menu=".$get->menu:"";?>";	
			}
		</script>
        <?php
		}
	} break;
	case 'edit':{
		$object = R::load("sys_privilege", $id);
	}
	case 'add':{
		if(isset($post->save)){
			if(isset($get->menu)){
				require("framework/modules/$module/model/menu_item.php");
				redir(makeUri("$module/$controller", 'view', $object->root));
			} else{
				require("framework/modules/$module/model/menu.php");
				redir(makeUri("$module/$controller", 'view', $object->root));
			}
		}
		if(isset($get->menu)){
			require("framework/modules/$module/view/form/menu_item.php");
		} else{
			require("framework/modules/$module/view/form/menu.php");
		}
	} break;
	/*case 'edit_menu_item':
	case 'add_menu_item':{
		if(isset($post->save)){
			require("pages/model/menu_item.php");
			redir("?f=view&menu=".$post->root);
		}
		require("view/form/menu_item.php");
	} break;*/
	case 'update':{
		$object = R::load("sys_privilege", $post->id);
		$object->root = $post->root;
		$object->name = $post->name;
		$object->title = $post->title;
		if(nn($post->link)){
			$object->link = $post->link;
		}
		$object->option = $post->option;
		$object->icon = $post->icon;
		R::store($object);
		deleteAllFiles('temps/menus/');
		redir("menu?f=view&menu={$get->menu}");
	} break;
	case 'view':{
		require("framework/modules/$module/view/menu.php");
  } break;
}
?>