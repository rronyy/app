<style type="text/css">
	td, th{
		padding: 10px 30px;
	}
</style>
<?php
	if($function=='profile_own'){
		$user = R::load("sys_user", uid());
	} else{
		$user = R::load("sys_user", $id);
	}
	if(file_exists("uploads/user/avatar/$user->u_avatar") && nn($user->u_avatar)){
		print "<div class='center'><img src='$appurl/uploads/user/avatar/$user->u_avatar' class='mw300'></div>";
	}
	print "
		<table class='table-striped' align='center'>
			<tr><th>Name</th><td>:</td><td>$user->u_fullname</td></tr>
			<tr><th>Username</th><td>:</td><td>$user->u_username</td></tr>
			<tr><th>Email</th><td>:</td><td>$user->u_email</td></tr>
			<tr><td colspan='3'><hr /></td></tr>
			<tr><th>Password</th><td>:</td><td>";
	if($function=='profile_own'){
		print "<a href='reset_own_password'>Change Password</td></tr>";
	} else{
		print "<a href='../reset_password/$id'>Change Password</td></tr>";
	}
	print "<tr><td colspan='3'><hr /></td></tr>
			<tr><th>Last Login Time</th><td>:</td><td>$user->u_last_login_time</td></tr>
			<tr><th>Last Login From</th><td>:</td><td>$user->u_last_ip</td></tr>
		</table>
	";