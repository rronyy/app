<?php
$user = isset($get->user)?$get->user:'';
$role = isset($get->role)?$get->role:'';
//$filter = "AND u_status = 1";
openFilterForm('get');
print "Username <input type='text' name='user' value='$user' class='form-control-fluid' > Role ".sop2('role', $role, ['optional'=>true, "dataField"=>'r_name', 'filter'=>'id>0'], 'sys_role');
closeFilterForm();
$filter = "u.id>1 and u.id=ur_user_id AND r.id=ur_role_id";
if(uid()!=1){
	joinFilter($filter, "u_created_by=".uid());
}

if($user){
	joinFilter($filter, "(u_username LIKE '%$user%' OR u_email LIKE '%$user%')");
}
if($role){
	joinFilter($filter, "r.id=$role");
}

$users = select("u.*, GROUP_CONCAT(r_name SEPARATOR ', ') AS roles", "sys_user u, sys_user_role ur, sys_role r", $filter, "GROUP BY u.id");

print "<table class='grid' width='100%'>";
print "<thead>
	<th>Sl.</th><th>Name</th><th>Username</th><th>Avatar</th><th>Roles</th><th>Email</th><th>Status</th><th>Last Login</th><th>Password</th><th>Actions</th>
	</thead>";
print "<tbody>";
$i = 1;
$owners[uid()] = username();
while($user = mysqli_fetch_object($users)){
	$avatar = "";
	if(file_exists("uploads/user/avatar/$user->u_avatar") && nn($user->u_avatar)){
		$avatar = "<img src='$appurl/uploads/user/avatar/$user->u_avatar' class='w30'>";
	} else{
		$avatar = "";
	}
	$owners[$user->id] = $user->u_fullname;
	print "<tr class='quick-filter-item'>";
	print "<td>$i</td>";
	print "<td title='$user->u_date_created'><a href='profile/$user->id'>$user->u_fullname</a></td>";
	print "<td>$user->u_username</td>";
	print "<td>$avatar</td>";
	print "<td>$user->roles</td>";
	print "<td>$user->u_email</td>";
	print "<td>".($user->u_status?"Active":"Inactive")."</td>";
	// print "<td>{$owners[$user->u_created_by]}</td>";
	print "<td><small>$user->u_last_ip<br>$user->u_last_login_time</small></td>";
	print "<td>".options2('', $user->id, array(($user->u_status?'deactivate':'activate'),'edit','remove','reset_password'))."</td>";
	print "</tr>";
	$i++;
}
print "</tbody>";
print "<tfoot><tr><th colspan='13'>".options2('', '', array('add'))."</th></tr></tfoot>";
print "</table><br /><br />";
?>

<div class="center">
	<a class='btn btn-success' href="<?php print APPURL; ?>/user/role/view">Roles</a>
	<a class='btn btn-primary' href="<?php print APPURL; ?>/user/user/permission">Permission</a>
</div>