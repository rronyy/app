<?php
function appDir($app, $sub_dir = '/'){
	$return = "";
	if(strpos($_SERVER['SCRIPT_FILENAME'], 'framework') !== FALSE){
		$return = substr($_SERVER['SCRIPT_FILENAME'], 0, strpos($_SERVER['SCRIPT_FILENAME'], 'framework'));
	}	elseif (strpos($_SERVER['SCRIPT_FILENAME'], $app) !== FALSE) {
		$return = substr($_SERVER['SCRIPT_FILENAME'], 0, strpos($_SERVER['SCRIPT_FILENAME'], $app));
	}
	return "$return$sub_dir/";
}