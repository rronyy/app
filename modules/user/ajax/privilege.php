<?php
if(isset($_POST['type'])){
	$type = $_POST['type'];
	require_once("../../../safeboot.php");
	require_once("../config.php");
	require_once("../../../core/vendor/panacea/f.inc.php");
	if($_POST['type']=="u"){
		$rq = select("DISTINCT p.link, pid, ptitle, IF(a.access=1, 'checked', '') AS checked", "`sys_privileges` p LEFT JOIN sys_permission a ON(p.pid=a.privilege AND a.`user` = {$_POST['appliesto']})", "gid<>1", "ORDER BY p.link");
	} elseif($_POST['type']=="r"){
		$rq = select("DISTINCT p.link, pid, ptitle, IF(a.access=1, 'checked', '') AS checked", "`sys_privileges` p LEFT JOIN sys_acl a ON(p.pid=a.privilege AND a.utype='r' AND appliesto = {$_POST['appliesto']})", "gid<>1", "ORDER BY p.link");
	}
?>
<?php 
	$link = "";
	$c = 0;
	$notfirst = false;
	while($r=mysqli_fetch_object($rq)){
		if($link != $r->link){
			$link = $r->link;
			if($notfirst){
				echo "<tr><td colspan='4'>&nbsp;</td></tr>";
			}			
			echo "<tr><td colspan='4'><font size='+1'><b>".ucfirst(title($link))."</b></font><font size='-1'> [<a href='javascript:selectall(\"{$r->link}\")' style='text-decoration:none;color:#000'> Select All </a>] [<a href='javascript:clearall(\"{$r->link}\")' style='text-decoration:none;color:#000'> Clear All </a>]<font></td></tr>";
			$c = 0;
		}		
		if($c%3 == 0){
			echo "<tr><td width='20px'></td>";
		}
		echo "<td><input type='checkbox' class='{$r->link}' name='privilege[]' value='{$r->pid}' $r->checked /> ".$r->ptitle."</td>";
		if($c%3 == 2){
			echo "</tr>";
		}
		$c++;
		$notfirst = true;
	}
?>
<tr><td colspan="4" align="right"><hr /><input type="submit" name="permission" value="Save" /></td></tr>
<?php } ?>