
<?php
if(isset($_POST['type'])){
	$type = $_POST['type'];
	$appliesto = $_POST['appliesto'];
	if($appliesto=='') die("<h3>No Records Found!</h3>");
	require_once("../config.php");
	require_once("../../../core/vendor/panacea/f.inc.php");
	$acls = acls();
	$privileges = "SELECT pr_g.id gid, pr_g.title gtitle, pr_p.id pid, pr_p.position `position`, pr_p.title ptitle, pr_p.icon icon FROM (sys_privilege pr_g LEFT JOIN sys_privilege pr_p ON ((pr_g.id = pr_p.root))) WHERE ((pr_g.root = 0) AND (pr_g.active = 1) AND (pr_p.active = 1)) ORDER BY pr_g.position,pr_g.title,pr_p.link,pr_p.position";
	$permission = "SELECT pe_p.id pid, pe_a.privilege privilege, pe_p.link link, pe_p.icon icon, pe_p.title title, pe_p.position position, pe_p.option `option`, pe_p.root root, pe_p.active active, pe_p.hidden hidden, pe_a.access access, pe_a.appliesto user, pe_p.controller controller, pe_p.show_in_frontpage show_in_frontpage FROM ($acls) pe_a JOIN sys_privilege pe_p WHERE (pe_a.privilege = pe_p.id)";

	if($type=="u"){
		$rq = select("DISTINCT p.*, a.link, a.option, IF(a.access=1, 'checked', '') checked",
			"(($privileges) p LEFT JOIN ($permission) a ON(p.pid=a.privilege AND a.`user` = $appliesto))", "gid<>1");
	} elseif($type=="r"){
		$rq = select("DISTINCT p.*, s.link, s.option, IF(a.access=1, 'checked', '') checked",
			"(($privileges) p LEFT JOIN sys_acl a ON(p.pid=a.privilege AND a.utype='r' AND appliesto = '{$appliesto}')), `sys_privilege` s",
			"s.id=pid AND gid<>1");
	}

	$groupname = "";
	$subgroupname = "";

	while($r=mysqli_fetch_object($rq)){
		//var_dump($r);
		//die();
		if($groupname != $r->gtitle){
			$groupname = $r->gtitle;
			print "<hr /><font size='+1'><b>".title($groupname)."</b></font><font size='-1'> [<a href='javascript:selectall(\"{$r->gid}\")' style='text-decoration:none;color:#000'> Select All </a>] [<a href='javascript:clearall(\"{$r->gid}\")' style='text-decoration:none;color:#000'> Clear All </a>]<font>";
		}
		if($subgroupname != $r->link){
			$subgroupname = $r->link;
			print "<br /><font><b><u>".ucwords(title2($subgroupname))."</u></b></font><font size='-1'> [<a href='javascript:selectall(\"{$subgroupname}\")' style='text-decoration:none;color:#000'> Select All </a>] [<a href='javascript:clearall(\"{$subgroupname}\")' style='text-decoration:none;color:#000'> Clear All </a>]<font><br />";
		}
		$link = "<div class='w250 in'><input type='checkbox' class='$r->gid $subgroupname' name='privilege[]' value='{$r->pid}' $r->checked /> ".$r->ptitle."<br><sub>($r->link/$r->option)</sub></div>";

		print $link;
	}

	print "<div align='center'><button type='submit' class='btn btn-success' name='permission'>Save</button></td></tr></div>";

}
