<?php 
if(!loggedin()) die("You don't have access to this feature!");
?>
<div align="center">
<?php
$role = R::dispense('sys_role');
$width = "700px";
$width1 = "100px";
switch ($function){
	case 'activate':{		
		$role = R::load('sys_role', $id);
		$role->r_active = 1;
		R::store($role);
		redir(makeUri("$module/$controller", 'view'));
	} break;
	case 'deactivate':{		
		$role = R::load('sys_role', $id);
		$role->r_active = 0;
		R::store($role);
		redir(makeUri("$module/$controller", 'view'));
	} break;
	case 'remove':{
		if(isset($get->conf)){			
			$role = R::load('sys_role', $id);
			del("sys_acl", "utype='r' AND appliesto=$role->id");
			R::trash($role);
			redir(makeUri("$module/$controller", 'view'));
		} else{
		?>
        <script type="text/javascript">
			if(confirm("Are you sure you want to remove this User?")){
				location.href = "<?php echo makeUri("$module/$controller", $function, $id);?>?conf";
			} else{
				location.href = "<?php print makeUri("$module/$controller", 'view'); ?>";	
			}
		</script>
        <?php
		}
	} break;
	case 'view':{
		require("framework/modules/$module/view/role.php");
	} break;
	case 'edit':{
		$role = R::load('sys_role', $id);
	}
	case 'add':{
		if(isset($post->save)){
			require_once("framework/modules/$module/model/role.php");
			redir(makeUri("$module/$controller", 'view'));
		}
		echo "<form method='post'>";
		print "<div><h2>Role Details</h2>";
		require_once("framework/modules/$module/view/form/role.php");
		print "</div>";
		echo "<br /><br /><div align='center' id='form-buttons'><input type='submit' name='save' value='Save' class='btn btn-success' />  <input type='reset' value='Clear All' class='btn btn-warning' /></div>";
		echo "</form>";
	} break;
 }
?>
</div>