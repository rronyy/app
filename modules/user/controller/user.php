<?php 
if(!loggedin()) die("You don't have access to this feature!");
?>
<?php
$object = R::dispense('sys_user');
switch ($function){
	case 'remove':{
		if(isset($get->conf)){			
			$object = R::load('sys_user', $id);
			del("sys_user_role", "ur_user_id=$object->id");
			del("sys_acl", "utype='u' AND appliesto=$object->id");
			R::trash($object);
			redir(makeUri("$module/$controller", 'view'));
		} else{
		?>
        <script type="text/javascript">
			if(confirm("Are you sure you want to remove this User?")){
				location.href = "<?php echo makeUri("$module/$controller", $function, $id);?>?conf";
			} else{
				location.href = "<?php print makeUri("$module/$controller", 'view'); ?>";	
			}
		</script>
        <?php
		}
	} break;
	case 'reset_password': {
		$object = R::load('sys_user', $id);
		if(isset($post->save)){
			require_once("framework/modules/$module/model/reset_password.php");			
		}
		require_once("framework/modules/$module/view/form/reset_password.php");
	} break;
	case 'reset_own_password': {
		$object = R::load('sys_user', uid());
		if(isset($post->save)){
			require_once("framework/modules/$module/model/reset_password.php");			
		}
		require_once("framework/modules/$module/view/form/reset_password.php");
	} break;
	case 'activate': {
		if(isset($post->id)){
			$object = R::load("sys_user", $id);
			$object->active = 1;
			$id = R::store($object);
		} elseif(isset($post->ids)){
			foreach($post->ids as $id){
				$object = R::load("user", $id);
				$object->active = 1;
				$id = R::store($object);
			}
		}
		redir(makeUri("$module/$controller", 'view'));
	} break;
	case 'deactivate': {
		if(isset($post->id)){
			$object = R::load("sys_user", $id);
			$object->active = 0;
			$id = R::store($object);
		} elseif(isset($post->ids)){
			foreach($post->ids as $id){
				$object = R::load("user", $id);
				$object->active = 0;
				$id = R::store($object);
			}
		}
		redir(makeUri("$module/$controller", 'view'));
	} break;
	case 'edit': {
		$object = R::load("sys_user", $id);
	}
	case 'add':{
		if(isset($post->save)){
			require_once("framework/modules/$module/model/user.php");
			redir(makeUri("$module/$controller", 'view'));
		}
		require_once("framework/modules/$module/view/form/user.php");
	} break;
	case 'view':{
		require_once("framework/modules/$module/view/user.php");
	} break;
	case 'profile':{
		require_once("framework/modules/$module/view/user_profile.php");
	} break;
	case 'profile_own':{
		require_once("framework/modules/$module/view/user_profile.php");
	} break;
	case 'permission':{
		require_once("framework/modules/$module/view/form/permission.php");
	} break;
}
?>
