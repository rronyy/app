<?php
$valid = true;
$msg = "";
if(strlen($post->fullname)<2){
    $msg = "Fullname must contain at least two characters. ";
    $valid = false;
}
if(strlen($post->username)<1){
    $msg = "User must contain at least one character. ";
    $valid = false;
}
if(isset($post->password)){
    if(strlen($post->password)<4){
        $msg = "Password must contain at least four characters. ";
        $valid = false;
    }
}
if($valid){
    $object->u_fullname = $post->fullname;
    $object->u_username = $post->username;
    if(isset($post->password)){
        $object->u_password = md5($post->password);
    }
    $object->u_email = $post->email;
    if(isset($post->active)){
        $object->u_status = $post->active;
    } else{
        $object->u_status = 0;
    }
    if(isset($post->pin)) $object->u_pin = md5($post->pin);
    $object->u_remarks = $post->remarks;
    $object->u_date_created = now();
    $object->u_created_by = uid();
    $object->u_last_modified_by = uid();
    $id = R::store($object);

    if ($_FILES['file']['size'] > 0){
        $filename = upload($_FILES, $object->id , "uploads/user/avatar");
        $object->u_avatar = $filename;
    }
    R::store($object);

	if(isset($post->type_id)){
		switch (strtolower(rolename($post->role))){
			case 'customer': {
			  $customer = R::load("sales_customer", $post->type_id);
			  $customer->c_user_id = $id;
			  R::store($customer);
			} break;
			case 'supplier': {
			  $supplier = R::load("purchase_supplier", $post->type_id);
			  $supplier->s_user_id = $id;
			  R::store($supplier);
			} break;
			case 'agent': {
			  $agent = R::load("sales_agent", $post->type_id);
			  $agent->a_user_id = $id;
			  R::store($agent);
			} break;
			default: {
			  $employee = R::load("employee_details", $post->type_id);
			  $employee->pd_user_id = $id;
			  R::store($employee);
			} break;
		 }
	}
    replace("sys_user_role", "ur_user_id, ur_role_id", "$id, {$post->role}");
} else{
    global $back;
    die($msg."<br />".$back);	
}