<?php
	$role->r_name = $post->name; 
	$role->r_active = isset($post->active)?$post->active:0;
	$role->r_date_created = today();
	if(isset($post->role)) {
		$role->r_owner = $post->role;
	} else{
		$role->r_owner = uid();
	}
	$role->r_created_by = uid();
	R::store($role);
