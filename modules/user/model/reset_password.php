<?php
if($function == 'reset_own_password'){
	$user = R::findOne("sys_user", "id=? AND u_password=?", array(uid(), md5($post->old_pass)));
} elseif(isset($id)){
	//$user = R::findOne("sys_user", "id=? AND u_password=?", array($id, md5($post->old_pass)));
	$user = R::findOne("sys_user", "id=?", array($id));
}

if($user){
	if($post->old_pass == $post->new_pass && $function == 'reset_own_password'){
		print "<div class='alert alert-danger' role='alert'>Current Password and New Password are the same!</div>";
	} elseif($post->new_pass == $post->confirm_pass){
		$user->u_password = md5($post->new_pass);
		R::store($user);
		alert("Password reset successful!");
		if($function == 'reset_own_password'){
			redir(makeUri("$module/$controller", 'profile'));
		} else{
			redir(makeUri("$module/$controller", 'view'));
		}
	} else{
		print "<div class='alert alert-danger' role='alert'>New Password and Confirm Password did not match!</div>";
	}	
} else{
	print "<div class='alert alert-danger' role='alert'>Current Password provided is wrong!</div>";
}