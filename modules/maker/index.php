<?php
if(file_exists("framework/modules/$module/controller/$controller.php")){
	include("framework/modules/$module/controller/$controller.php");
} elseif(file_exists("framework/modules/$module/controller/$module.php")){
	$controller = $module;
	include("framework/modules/$module/controller/$module.php");
} else{
	include '404.php';
}
