<?php
if(isset($post->schemaname)){
	menuUpdated();
	if(isset($post->overwrite)){
		del("frm_controller", "c_schema='$post->schemaname'");
	}
	$controllername = name(str_replace($post->schemaprefix, "", $post->schemaname));
	$controllertitle= title(str_replace($post->schemaprefix, "", $post->schemaname));
	$tfname = getTrashField($post->schemaname);
	if(nn($post->nmenu)){
		$privilege = R::dispense("sys_privilege");
		$privilege->name = $post->nmenu;
		$privilege->title = $post->nmenu;
		$privilege->root = 0;
		R::store($privilege);
		$post->menu = $privilege->id;
	}
	$object_controller = R::dispense("frm_controller");
	$object_controller->c_schema = $post->schemaname;
    $object_controller->c_schema_prefix = $post->schemaprefix;
    $object_controller->c_name = $controllername;
    $object_controller->c_title = $controllertitle;
    R::store($object_controller);

	$i = 0;
	if(!isset($post->overwrite) || isset($post->menuonly)){
		foreach (array('view','add','edit','remove','erase') as $item) {
			if($item=='remove' && !nn($tfname)){
				continue;
			}
			$privilege_item = R::dispense("sys_privilege");
			$privilege_item->name = title("{$item}_$post->schemaname");
			if($item=='view'){
				$privilege_item->name = $controllertitle;
			}
			$privilege_item->link = $controllername;
			$privilege_item->title = $privilege_item->name;
			$privilege_item->option = $item;
			$privilege_item->position = $i++;
			$privilege_item->hidden = $item=='view'?0:1;
			$privilege_item->controller = $item=='view'?0:1;
			$privilege_item->root = $post->menu;
			if($item=='edit'){
				$privilege_item->glyphicon = "glyphicon glyphicon-edit";
			} elseif($item=='add'){
				$privilege_item->glyphicon = "glyphicon glyphicon-file";
			} elseif($item=='remove'){
				$privilege_item->glyphicon = "glyphicon glyphicon-trash";
			} elseif($item=='view'){
				$privilege_item->glyphicon = "glyphicon glyphicon-list";
			} elseif($item=='erase'){
				$privilege_item->glyphicon = "glyphicon glyphicon-remove";
			}
			R::store($privilege_item);
		}
	}
	
	$form = R::dispense("frm_form");
    $form->f_controller = $object_controller->id;
	$form->f_name = "add_$controllername";
    $form->f_title = "Add $controllertitle";
    $form->f_action_after = 'view';
    R::store($form);

    $i = 0;
	foreach ($post->form_fields as $field) {
		$att = R::dispense("frm_attributes");
		$att->att_form = $form->id;
		$field_name = $post->{"field_$field"};
		$att->att_name = name($field_name);
		$att->att_field = $field_name;
		$att->att_title = title($field_name);
		$att->att_required = 1;
		$att->att_position = $i++;
		R::store($att);
	}

	$view = R::dispense("frm_view");
	$view->v_controller = $object_controller->id;
	$view->v_name = $controllername;
	R::store($view);

	foreach ($post->view_fields as $field) {
		$view_field = R::dispense("frm_view_item");
		$view_field->vi_view = $view->id;
		$view_field->vi_field = $field;
		$view_field->vi_title = title($field);
		$view_field->vi_position = $i++;
		
		R::store($view_field);
	}

	foreach ($post->filter_fields as $field) {
		$filter = R::dispense("frm_view_filter"); 
		$filter->vf_view = $view->id;
	    $filter->vf_field = $field;
		$filter->vf_position = $i++;
	    
	    R::store($filter);
	}

	makeMVC($post);

	//redir("maker");
}


$con = "<form method='post'>
	<table>
    <tr><th>Choose Schema / Schema Prefix / Field Prefix</th><th colspan='3'>Menu & Site</th></tr>
	<tr><td>".selectTables("name='schemaname' id='schemaname' class='required selectpicker' data-live-search='true'")." / <input name='schemaprefix' class='w50 form-control-fluid' /> / <input name='fieldprefix' class='w50 form-control-fluid' /><br>
		 Overwrite?
		 <input type='checkbox' name='overwrite' vlaue='1' class='form-control-fluid w50' />
		 Menu Only?
		 <input type='checkbox' name='menuonly' vlaue='1' class='form-control-fluid w50' />
		 Mobile?
		 <input type='checkbox' name='mobile' vlaue='1' class='form-control-fluid w50' />
		 Dual?
		 <input type='checkbox' name='dual' vlaue='1' class='form-control-fluid w50' /></td>
    <td colspan='2'>Menu <br />".selectOption("name='menu' id='menu' class='form-control selectpicker' data-live-search='true'", "sys_privilege", "title", "id", "", "root=0", "", TRUE)."<br />OR </br>
    New Menu <br /><input type='text' name='nmenu' id='nmenu' class='form-control-fluid' /></td></tr>
    <tr><th>Form</th><th>View</th><th>Filter</th></tr>
    <tr>
    <td id='td-form' class='top'></td>
    <td id='td-view' class='top'></td>
    <td id='td-filter' class='top'></td></tr>";
$con .= '<tr><td></td><td><input type="submit" name="create" class="btn btn-success" /></td></tr>
</table>
</form>';	
print $con;

function selectTables($attr){
	$tables = tables();
	$content = "<select $attr>";
	foreach($tables as $table){
		$content .= "<option>$table</option>";	
	}
	return $content.="</select>";
}


?>


<script type="text/javascript">
	window.onload = function(){
    check();
    $("#menu").change(check);
    function check() {
        if ($("#menu").val() == "") {
            $("#nmenu").addClass("required");
        } else {
            $("#nmenu").removeClass("required");
        }
    }
    $("#schemaname").change(function () {
        $.post("<?php print $appurl; ?>/framework/common/ajax/getFields.php", { 'schema': $(this).val(), 'type': 'form', 'app': '<?php print APP; ?>' }, function (data) {
          $("#td-form").html(data);
        });
        $.post("<?php print $appurl; ?>/framework/common/ajax/getFields.php", { 'schema': $(this).val(), 'type': 'view', 'app': '<?php print APP; ?>' }, function (data) {
          $("#td-view").html(data);
        });
        $.post("<?php print $appurl; ?>/framework/common/ajax/getFields.php", { 'schema': $(this).val(), 'type': 'filter', 'app': '<?php print APP; ?>' }, function (data) {
          $("#td-filter").html(data);
        });
    });
  }
</script>

<?php
function makeMVC($post){
	$schemaname = $post->schemaname;
	$controller = name($post->schemaname);
	$controllertitle = title($post->schemaname);
  $allfields = tableFieldsArray($schemaname);
	$tfname = getTrashField($schemaname); //Trash Field
	$afname = getActiveField($schemaname); //Active Field
	$userlist = false;
	$overwrite = isset($post->overwrite)?TRUE:FALSE;
	//controller, schemaname, controllertitle
	$funtions = f();

	$content = '<?php 
$object = R::dispense("schemaname");
if(isset($id)){
	$object = R::load("schemaname", $id);
}
switch ($function){
	case "view":{
		'.$funtions[0].'
	} break;
';
	if(nn($tfname)){
		$content .= '	case "remove":{
		'.($funtions[1]).'
	} break;
';
	}
	$content .= '	case "erase":{
		'.($funtions[4]).'
	} break;
	case "edit":
	case "add":{
		'.$funtions[3].'
	} break;
}';
	$content = str_replace("schemaname", $schemaname, $content);
	//$content = str_replace("controller", $controller , $content);
	$content = str_replace("controllertitle", $controllertitle , $content);
	$content = str_replace("tfname", $tfname , $content);

    //if($overwrite || !file_exists("controller/$controller.php")){
	if(file_exists("controller/$controller.php")){
		rename("controller/$controller.php", "controller/$controller.php.".time());
	}
  file_put_contents("controller/$controller.php", stripslashes($content));
    //}
    	
	
//**************************** FORM
  $form_content = '';
  if(isset($post->mobile)){ //Mobile Friendly
  	$form_content .= '<?php
print "<div class=\'mobile-screen-only\'>";
openForm();
print "<h2>".str("'.$controllertitle.' Details")."</h2>';
	$i = 1;
    $fc = 1; //Input box counter
    foreach($allfields as $field){
        if(!in_array($i++, $post->form_fields) || contains($field->Field, '_entry_') || contains($field->Field, '_modify_') || contains($field->Field, '_approve_')){
            continue;
        }
		$class = "";	
		if($field->Null=='NO'){
			$class .= "required ";
		}	
		if(contains($field->Field, 'email')){
			$class .= "email ";
		}
		if(contains($field->Type, 'date') || contains($field->Type, 'time')){
			$class .= $field->Type." ";
		} elseif(contains($field->Type, 'int') || contains($field->Type, 'decimal')){
			$class .= "number ";
		}
		$class = trim($class);
		$form_content .= '
		<div class=\'form-group\'>';

		if(contains($field->Field, 'active') || contains($field->Type, 'tinyint')){
			$form_content .= '<label for=\''.name($field->Field).'\'>'.title($field->Field).'?</label>
			<input type=\'checkbox\' name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' ".($object->'.$field->Field.'?"checked":"")." class=\'form-control '.$class.'\' value=\'1\' />';
		} elseif(contains($field->Type, 'enum')){
			$form_content .= '<label for=\''.name($field->Field).'\'>'.title($field->Field).'</label>".selectEnum("name=\''.name($field->Field).'\' id=\''.name($field->Field).'\'  class=\'form-control\'", \''.$schemaname.'\', \''.$field->Field.'\',\$object->'.$field->Field.')."';
		} elseif(contains($field->Type, 'text')){
			$form_content .= '<label for=\''.name($field->Field).'\'>'.title($field->Field).'</label><textarea name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' class=\'form-control '.$class.'\'>$object->'.$field->Field.'</textarea>';
		} elseif($field->Comment!=''){
			$comments = explode(" ", $field->Comment);
			$form_content .= '<label for=\''.name($field->Field).'\'>'.title($field->Field).'</label>".selectOption("name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' class=\'form-control selectpicker\'  data-live-search=\'true\'", \''.$field->RefTable.'\', \''.$comments[0].'\', \'id\',\$'.$object.'->'.$field->Field.')."';
		} elseif($field->RefTable!=''){
			$form_content .= '<label for=\''.name($field->Field).'\'>'.title($field->Field).'</label>".selectOption("name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' class=\'form-control selectpicker\' data-live-search=\'true\'", \''.$field->RefTable.'\', \'name\', \'id\',\$object->'.$field->Field.')."';
		} elseif(contains($field->Type, 'date')){
			$form_content .= '<label for=\''.name($field->Field).'\'>'.title($field->Field).'</label>".dateSelector("'.name($field->Field).'", \$object->'.$field->Field.')."';
		} else{
			$form_content .= '<label for=\''.name($field->Field).'\'>'.title($field->Field).'</label><input type=\'text\' name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' value=\'$object->'.$field->Field.'\' class=\'form-control '.$class.'\' />';
		}

		$form_content .= '</div>';
	}//=========================

$form_content .= '";
closeForm();
print "</div>";
?>';

  	//End Mobile Friendly
  }

  if(!isset($post->mobile) || isset($post->dual)){ // Start Non-Mobile
	$form_content .= '<?php
openForm();
print "<table align=\'center\'>
	<tr><td colspan=\'5\'><b>".str("'.$controllertitle.' Details")."</b></td><tr>';
    $i = 1;
    $fc = 1; //Input box counter
    foreach($allfields as $field){
        if(!in_array($i++, $post->form_fields) || contains($field->Field, 'entry_') || contains($field->Field, 'modify_') || contains($field->Field, 'approve_')){
            continue;
        }
		$class = "";	
		if($field->Null=='NO'){
			$class .= "required ";
		}	
		if(contains($field->Field, 'email')){
			$class .= "email ";
		}
		if(contains($field->Type, 'date') || contains($field->Type, 'time')){
			$class .= $field->Type." ";
		} elseif(contains($field->Type, 'int') || contains($field->Type, 'decimal')){
			$class .= "number ";
		}
		$class = trim($class);
		if($fc==1){
			$form_content .= '
		<tr><td>';
		} elseif($fc=2){
			$form_content .= '<td>';
		}

		if(contains($field->Field, 'active') || contains($field->Type, 'tinyint')){
			$form_content .= '".str("'.title($field->Field).'")."?</td><td><input type=\'checkbox\' name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' ".($object->'.$field->Field.'?"checked":"")." class=\'form-control '.$class.'\' value=\'1\' />';
		} elseif(contains($field->Type, 'enum')){
			$form_content .= '".str("'.title($field->Field).'")."</td><td>".selectEnum("name=\''.name($field->Field).'\' id=\''.name($field->Field).'\'  class=\'form-control\'", \''.$schemaname.'\', \''.$field->Field.'\',\$object->'.$field->Field.')."';
		} elseif(contains($field->Type, 'text')){
			$form_content .= '".str("'.title($field->Field).'")."</td><td><textarea name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' class=\'form-control '.$class.'\'>$object->'.$field->Field.'</textarea>';
		} elseif(contains($field->Type, 'int') && $field->Comment!=''){
			$comments = explode(" ", $field->Comment);
			$form_content .= '".str("'.title($field->Field).'")."</td><td>".sop("'.$comments[0].'", \$object->'.$field->Field.')."';
		} elseif($field->Comment!=''){
			$comments = explode(" ", $field->Comment);
			$form_content .= '".str("'.title($field->Field).'")."</td><td>".selectOption("name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' class=\'form-control selectpicker\' data-live-search=\'true\'", \''.$field->RefTable.'\', \''.$comments[0].'\', \'id\',\$'.$object.'->'.$field->Field.')."';
		} elseif($field->RefTable!=''){
			$form_content .= '".str("'.title($field->Field).'")."</td><td>".selectOption("name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' class=\'form-control selectpicker\' data-live-search=\'true\'", \''.$field->RefTable.'\', \'name\', \'id\',\$object->'.$field->Field.')."';
		} elseif(contains($field->Type, 'date')){
			$form_content .= '".str("'.title($field->Field).'")."</td><td>".dateSelector("'.name($field->Field).'", \$object->'.$field->Field.')."';
		} else{
			$form_content .= '".str("'.title($field->Field).'")."</td><td><input type=\'text\' name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' value=\'$object->'.$field->Field.'\' class=\'form-control '.$class.'\' />';
		}

		if($fc==1){
			$form_content .= '</td><td>".space(5)."</td>';
			$fc = 2;
		} elseif($fc==2){
			$form_content .= '</td></tr>';
			$fc=1;
		}
	}
	$form_content .= '
	</table>";
closeForm();';
    
	} // End Non-Mobile
    
	// if($overwrite || !file_exists("view/form/$controller.php")){
	if(file_exists("view/form/$controller.php")){
		rename("view/form/$controller.php", "view/form/$controller.php.".time());
	}
  file_put_contents("view/form/$controller.php", stripslashes($form_content));
  // }
	//********************* Model
	$pss_content ='<?php 
$fields = [';
	$pss_content_ending = '';
	$fields = '';
	$i = 1;
    foreach($allfields as $field){
        /*if(!in_array($i++, $post->form_fields)){
            continue;
        }*/
		if($field->Field == 'id' || contains($field->Field, 'trash')) {	continue; }
		if(contains($field->Field, 'entry_by')!==FALSE){
			$pss_content_ending .='if($function=="add") $object->'.$field->Field.' = uid();
';
			$userlist = true;
		} elseif(contains($field->Field, 'entry_time')){
			$pss_content_ending .='if($function=="add") $object->'.$field->Field.' = now();
';
		} elseif(contains($field->Field, 'modify_by') || contains($field->Field, 'modified_by')){
			$pss_content_ending .='if($function=="edit") $object->'.$field->Field.' = uid();
';
		} elseif(contains($field->Field, 'modify_time') || contains($field->Field, 'modified_time')){
			$pss_content_ending .='if($function=="edit") $object->'.$field->Field.' = now();
';
		} elseif(contains($field->Type, 'tinyint')){
			$pss_content_ending .='$object->'.$field->Field.' = isset($post->'.name($field->Field).')?$post->'.name($field->Field).':0;
';
			
		} else{
			if(contains($field->Field, "store")){
				$storelist = true;	
			}
			$fields .= ($fields!=''?',':'')."'$field->Field'";
// 			if(strpos($field->Type, 'int')!==false || strpos($field->Type, 'int')!==false){				
// 				$pss_content .='$object->'.$field->Field.' = $post->'.name($field->Field).' + 0;
// ';
// 			} elseif(strpos($field->Type, 'text')!==false || strpos($field->Type, 'varchar')!==false){
// 				$pss_content .='$object->'.$field->Field.' = addslashes($post->'.name($field->Field).');
// ';
// 			}  elseif(strpos($field->Type, 'email')!==false){
// 				$pss_content .='$object->'.$field->Field.' = $post->'.name($field->Field).';
// ';
// 			} else{
// 				$pss_content .='$object->'.$field->Field.' = $post->'.name($field->Field).';
// ';
// 			}
		}
	}
	$pss_content .= $fields.'];

foreach ($fields as $field) {
	if(isset($post->$field) && nn($post->$field)) {
		$object->$field = $post->$field;
	}
}

';
	$pss_content .= $pss_content_ending;
	$pss_content .= 'R::store($object); 
?>';	
	
    // if($overwrite || !file_exists("model/$controller.php")){
		if(file_exists("model/$controller.php")){
			rename("model/$controller.php", "model/$controller.php.".time());
		}
		file_put_contents("model/$controller.php", stripslashes($pss_content));
    // }
	
	//********************* VIEW
	$tfs = "a.*";
	$ts = "$schemaname a";
	$tcs = "";
	$tos = "";
	$talias = 65;
	$alias = 98;
    $view_content = '
<?php
if($id){
	print "<table class=\'table table-responsive table-striped table-bordered table-detailed-view\'>';
	foreach($allfields as $field){ 
		if($field->Field == 'id'){ 
			continue; 
		}
		$view_content .= '
		<tr><th>".str("'.title($field->Field).'")."</th><td> $object->'.$field->Field.'</td></tr>';
	}
    $view_content .= '
  </table>";
  back();
} else{
	//$page = is("page", 1, "", FALSE);
	$page = is("page", 1);
	$offset = 20;
	    ';	
	    	//var_dump($post->view_fields);
	    	foreach($allfields as $field){ 
	    		if(!in_array($field->Field, $post->view_fields)){ 
	    			//var_dump($field);
	    			continue; 
	    		}

				if(contains($field->Field, 'entry_by') || contains($field->Field, 'modify_by') || contains($field->Field, 'modified_by')){
					continue;
				} elseif($field->Comment!=''){
					$comments = explode(" ", $field->Comment);
					for($i=0;$i<count($comments);$i++){
						$tfs .= ", ".chr($alias).".".trim($comments[$i])." ".chr($talias);
					}
					if($field->Null == 'YES'){				
						$ts = "($ts) LEFT JOIN ".$field->RefTable." ".chr($alias)." ON(".chr($alias).".id=a.".$field->Field.")";
					} elseif(isset($field->RefTable)){
						$ts .= ", ".$field->RefTable." ".chr($alias);
						$tcs .= ($tcs!=""?" AND ":"")." a.".$field->Field."=".chr($alias).".id";
					} else{
					//don't know yet
					}
					$alias++; $talias++;
				}	
			}
		if($tfname){ $tcs .= ($tcs!=""?" AND ":"")." $tfname=0"; }
		//Filter 
			//-----------------
	    if(isset($post->filter_fields)){
	    	if(count($post->filter_fields)>0){        
		    $view_content .= ' 
	    $filter = "'.trim($tcs).'";
	    openFilterForm("get");
	    print "<input type=\'hidden\' name=\'page\' value=\'$page\' class=\'form-control-fluid\' />";
	    ';
	        foreach($allfields as $field){ 
	        	if(!in_array($field->Field, $post->filter_fields)){ 
	        		continue; 
	        	}
				if(contains($field->Field, 'active') || contains($field->Type, 'tinyint')){
	            	$view_content .= '$'.name($field->Field).' = is("'.name($field->Field).'", "1");
		if($'.name($field->Field).' != "All"){
			$'.name($field->Field).' = isf("'.(nn($field->Comment)?$field->Comment:$field->Field).'", "'.name($field->Field).'", $filter, $get, "1");
		}';
					$view_content .= '
	    print str("'.title($field->Field).'")." ".makeSelectOption("name=\''.name($field->Field).'\' id=\''.name($field->Field).'\' class=\'form-control-fluid\'", array("All", "Yes", "No"), array("All", "1", "0"), $'.name($field->Field).');
	    ';
				} elseif(contains($field->Type, 'enum')){
	            	$view_content .= '$'.name($field->Field).' = isf("'.$field->Field.'", "'.name($field->Field).'", $filter, $get);';
	            	$view_content .= '
	    print str("'.title($field->Field).'")." ".selectEnum("name=\''.name($field->Field).'\' class=\'form-control-fluid\' id=\''.name($field->Field).'\'", \''.$schemaname.'\', \''.$field->Field.'\',\$'.name($field->Field).', array(), true, false, true)." ";
	    ';
				} elseif($field->Field == 'date'){
	            	$view_content .= '$'.name($field->Field).'From = isset($get->'.name($field->Field).'From)?$get->'.name($field->Field).'From:today();
	    ';
	            	$view_content .= '$'.name($field->Field).'To = isset($get->'.name($field->Field).'To)?$get->'.name($field->Field).'To:today();';
	            	$view_content .= '
	    joinFilter($filter, "`date` BETWEEN \'$'.name($field->Field).'From\' AND \'$'.name($field->Field).'To\'");
	    print str("'.title($field->Field).'")." <input type=\'date\' name=\''.name($field->Field).'From\' value=\'$'.name($field->Field).'From\' class=\'form-control-fluid\' /> to <input type=\'date\' name=\''.name($field->Field).'To\' value=\'$'.name($field->Field).'To\' class=\'form-control-fluid\' /> ";
	    ';
				} elseif(contains($field->Field, 'date')){
	            	$view_content .= '$'.name($field->Field).' = isf("'.(nn($field->Comment)?$field->Comment:$field->Field).'", "'.name($field->Field).'", $filter, $get);';
	            	$view_content .= '
	    print str("'.title($field->Field).'")." <input type=\'date\' name=\''.name($field->Field).'\' value=\'$'.name($field->Field).'\' class=\'form-control-fluid\' /> ";
	    ';
				} elseif($field->RefTable!=''){
					if($field->RefTable=='sys_user'){
						$view_content .= '$'.name($field->Field).' = isf("'.$field->Field.'", "'.name($field->Field).'", $filter, $get, \'\', true);';
    				$view_content .= '
    	print str("'.title($field->Field).'")." ".sop2(\''.$field->Field.'\', $'.name($field->Field).', [\'dataField\'=>\'u_fullname\', \'optional\'=>true], \''.$field->RefTable.'\');		
    	';
					} else{
						$view_content .= '$'.name($field->Field).' = isf("'.$field->Field.'", "'.name($field->Field).'", $filter, $get, \'\', true);';
    				$view_content .= '
    	print str("'.title($field->Field).'")." ".sop2(\''.$field->Field.'\', $'.name($field->Field).', [\'optional\'=>true], \''.$field->RefTable.'\');					
	    ';
	    		}
				} else{
	            	$view_content .= '$'.name($field->Field).' = isf("'.(nn($field->Comment)?$field->Comment:$field->Field).'", "'.name($field->Field).'", $filter, $get);';
	            	$view_content .= '
	    print str("'.title($field->Field).'")." <input type=\'text\' name=\''.name($field->Field).'\' value=\'$'.name($field->Field).'\' class=\'form-control-fluid\' /> ";
	    ';
				}
	        }
	        $view_content .= 'closeFilterForm();
	';        
	    }}
	if($userlist){$view_content .= '$userlist = userList();
	';}
	if(nn($tfname)){
		$view_content .= '
	$nor = num_rows("'.$tfs.'", "'.$ts.'", "'.$tfname.'=0".(nn($filter)?" AND $filter":""));
	$nop = ceil($nor/$offset); if($page > $nop) $page = 1;

	$start = ($page-1)*$offset;
	$'.$controller.'s = select("'.$tfs.'", "'.$ts.'", "'.$tfname.'=0".(nn($filter)?" AND $filter":""), "LIMIT $start, $offset");';
	} else{
		$view_content .= '
	$nor = num_rows("'.$tfs.'", "'.$ts.'", "$filter");
	$nop = ceil($nor/$offset); if($page > $nop) $page = 1;

	$start = ($page-1)*$offset;
	$'.$controller.'s = select("'.$tfs.'", "'.$ts.'", "$filter", "LIMIT $start, $offset");';
	}
	    
	//Referenced column
	foreach($allfields as $field){
    if(!in_array($field->Field, $post->view_fields)){
        continue;
    } elseif($field->RefTable!='' && !(contains($field->Field, 'entry_by') || contains($field->Field, 'modify_by') || contains($field->Field, 'modified_by'))){
    	$view_content .= '
  $'.$field->Field.'List = toA("'.$field->RefTable.'");
    	';
    }
  }

	$view_content .= '
	print "<hr>";
	print "<table align=\'center\' class=\'table table-responsive table-striped\'>';
		$header = '
	<thead><tr><th>#</th>';
		$body = '

	$i = $start + 1;
	while($'.$controller.' = mysqli_fetch_object($'.$controller.'s)){
		print "<tr><td><a href=\'view/$'.$controller.'->id\'>$i</a></td>
			';
		$footer = '
	<tr>';
		$colcount = 0;
		$talias = 65;
		$extra_options = "";
	    foreach($allfields as $field){
	        if(!in_array($field->Field, $post->view_fields)){
	            continue;
	        }
			$header .= '<th>".str("'.title($field->Field).'")."</th>';
			if(contains($field->Field, 'entry_by') || contains($field->Field, 'modify_by') || contains($field->Field, 'modified_by')){
				$body .= '<td>{$userlist[$'.$controller.'->'.$field->Field.']}</td>
			';
			} elseif(contains($field->Field, 'store')){
				$body .= '<td>{".$storelist[$'.$controller.'->'.$field->Field.']."}</td>
			';
			} elseif($field->Comment!=''){
				$comments = count(explode(" ", $field->Comment));
				if($comments>1){
					$body .= '<td>".fs(array(';
					for($i=0;$i<$comments;$i++){ $body .= '$'.$controller.'->'.chr($talias++).','; }
					$body = substr($body,0, -1);
					$body .= '))."</td>
			';	
				} else{
				$body .= '<td>$'.$controller.'->'.chr($talias++).'</td>
			';
				}
			} elseif($field->RefTable!=''){
	    	$body .= '<td>{$'.$field->Field.'List[$'.$controller.'->'.$field->Field.']}</td>
	    ';
	    } elseif(contains($field->Type, 'text')){
			  $body .= '<td>".stripslashes($'.$controller.'->'.$field->Field.')."</td>
			';
			} elseif(contains($field->Field, 'active') ||contains($field->Type, 'tinyint')){
				$body .= '<td>".($'.$controller.'->'.$field->Field.'?"Yes":"No")."</td>
			';
			} else{
			$body .= '<td>$'.$controller.'->'.$field->Field.'</td>
			';
			}
			$colcount++;
		}
		$view_content .= $header.'<th>".options2("", "", array("add"))."</th></tr></thead>
	    <tbody>";';
		$view_content .= $body.'<td>".options2("", $'.$controller.'->id, array("edit", "remove"'.$extra_options.',"erase"))."</td></tr>";
		$i++;
	}';
		$view_content .= '
	print "</tbody>
	<tfoot>";
	print paging('.($colcount+2).', $nop, $nor, $page);';
		$view_content .= '
	print "</tfoot>
	</table>";
}
?>';
    // if($overwrite || !file_exists("view/$controller.php")){
		if(file_exists("view/$controller.php")){
			rename("view/$controller.php", "view/$controller.php.".time());
		}
    file_put_contents("view/$controller.php", stripslashes($view_content));	
    // }
	
}
//END of MakeMVC
function f(){
$f = array(
'require("view/$controller.php");',
'if(isset($get->conf)){		
			$object = R::load("schemaname", $id);
			$object->tfname = 1;
			R::store($object);
			redir("../view");
		} else{
			?>
			<script type="text/javascript">
				if(confirm("Are you sure you want to remove this controllertitle?")){
					location.href = "?conf";
				} else{
					location.href = "../view";	
				}
			</script>
			<?php
		}
',
'$object = R::load("schemaname", $id);',
'if(isset($post->save)){
			require_once("model/$controller.php");
			redir(($function==\'edit\'?\'../\':\'\')."view");
		}
		require_once("view/form/$controller.php");',
'if(isset($get->conf)){		
			$object = R::load("schemaname", $id);
			R::trash($object);
			redir("../view");
		} else{
			?>
			<script type="text/javascript">
				if(confirm("Are you sure you want to completly remove this controllertitle?")){
					location.href = "?conf";
				} else{
					location.href = "../view";	
				}
			</script>
			<?php
		}
');
	return $f;
}
?>