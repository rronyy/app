﻿<?php
/**
 * WebSMS2u SMS SEND INCLUDE
 *
 * This class is meant to send SMS messages via the WebSMS2u gateway
 * and provides support to authenticate to this service and also query
 * for the current account balance. This class use the fopen or CURL module
 * to communicate with the gateway via HTTP/S.
 *
 * For more information about WebSMS2u service visit http://www.websms2u.com
 *
 * @version 1.5 (11 September 2013) - Renamed Files for easier and consistent naming (introduced checkstatus sample codes)
 * @version 1.4 (25 September 2011) - $APIresponse added to allow storage or echo of response from SMS Gateway Server
 * @version 1.3 - Improved Instructions from version 1.2 for easier understanding.
 * @package sms_api
 * @copyright Copyright (2009 - 2011)
 *
 */

/**
 * Usage Instructions
 *
 * Step 1: Configure the following options in the CLASS SMS below:
 *
 *         Mandatory Variables to set:
 *         $user = Your WebSMS2u.com UserID
 *         $pass = Your WebSMS2u.com Password
 *
 *         Optional Variables to set:
 *         $sending_method = Method to use to perform transmission. Enter either 'curl' or 'fopen'.
 *         $curl_use_proxy = Set to False or True depending on whether you wish to use PROXY under curl method
 *         $curl_proxy = Set the proxy address to use
 * 
 * Step 2: Call the class funtion from the included index.php or from your script which should include the following example codes:
 *
 *         Example Code below:
 *	   Make sure you fill in or replace the DESTINATION, SenderID, MESSAGE, TYPE, SMSGATEWAY variables
 *
 *         <?php
 *         require_once ("sms_send_include.php");
 *         $mysms = new sms();
 *         echo $mysms->session;
 *         $APIresponse = $mysms->send ("DESTINATION", "SenderID", "MESSAGE", "TYPE", "SMSGATEWAY");
 *         $APIresponse;
 *         ?>
 *
 * Step 3: Upload the files and run it from the browser. You should be able to receive an SMS from our system after that.
 */


class sms {

    /**
    * WebSMS2u username
    * @var mixed
    */
    var $user = "";

    /**
    * WebSMS2u password
    * @var mixed
    */
    var $pass = "";

    /**
    * Gateway command sending method (curl,fopen)
    * @var mixed
    */
    var $sending_method = "fopen";

    /**
    * Optional CURL Proxy
    * @var bool
    */
    var $curl_use_proxy = false;

    /**
    * Proxy URL and PORT
    * @var mixed
    */
    var $curl_proxy = "http://127.0.0.1:8080";

    /**
    * Proxy username and password
    * @var mixed
    */
    var $curl_proxyuserpwd = "login:secretpass";


    /**
    * Session variable
    * @var mixed
    */
    var $session;

    /**
    * Class constructor
    * Create SMS object and authenticate SMS gateway
    * @return object New SMS object.
    * @access public
    */
    function sms () {
        $this->base   = "http://www.websms2u.com/sms/sendapi.asp";
    }

    /**
    * Send SMS message
    * @param to mixed  The destination address.
    * @param from mixed  The source/sender address
    * @param text mixed  The text content of the message
    * @return mixed  "OK" or script die
    * @access public
    */

    function setUser($u){
        $this->user = $u;
    }

    function setPass($p){
        $this->pass = $p;
    }

    function send($to=null, $from=null, $text=null, $type=null, $smsgateway) {

    	/* Check SMS $text length */
        if (strlen ($text) > 480) {
    	    die ("Your message is to long! (Current lenght=".strlen ($text).")");
    	}

    	/* Check $to, $from, $smsgateway is not empty */
        if (empty ($to)) {
    	    die ("You did not specify destination address (TO)!");
    	}
        if (empty ($from)) {
    	    die ("You did not specify source address (FROM)!");
    	}
        if (empty ($smsgateway)) {
    	    die ("You did not specify smsgateway type (SMSGATEWAY)!");
    	}


    	/* Reformat $to number */
        $cleanup_chr = array ("+", "-", " ", "(", ")", "\r", "\n", "\r\n");
        $to = str_replace($cleanup_chr, "", $to);

    	/* Send SMS now */
    	$comm = sprintf ("%s?user=%s&pass=%s&to=%s&from=%s&text=%s&type=%s&smsgateway=%s",
            $this->base,
	    $this->user,
	    $this->pass,
            rawurlencode($to),
            rawurlencode($from),
            rawurlencode($text),
            rawurlencode($type),
            rawurlencode($smsgateway),
            $this->session
        );
        return $this->_parse_send ($this->_execgw($comm));
    }

    /**
    * Execute gateway commands
    * @access private
    */
    function _execgw($command) {
        if ($this->sending_method == "curl")
            return $this->_curl($command);
        if ($this->sending_method == "fopen")
            return $this->_fopen($command);
        die ("Unsupported sending method!");
    }

    /**
    * CURL sending method
    * @access private
    */
    function _curl($command) {
        $this->_chk_curl();
        $ch = curl_init ($command);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER,0);
        if ($this->curl_use_proxy) {
            curl_setopt ($ch, CURLOPT_PROXY, $this->curl_proxy);
            curl_setopt ($ch, CURLOPT_PROXYUSERPWD, $this->curl_proxyuserpwd);
        }
        $result=curl_exec ($ch);
        curl_close ($ch);
        return $result;
    }

    /**
    * fopen sending method
    * @access private
    */
    function _fopen($command) {
        $result = '';
        $handle = @fopen($command, "r");
        if ($handle) {
            while ($line = @fgets($handle,1024)) {
                $result .= $line;
            }
            fclose ($handle);
            return $result;
        } else {
            die ("Error while executing fopen sending method!<br>Please check does PHP have OpenSSL support and check does PHP version is greater than 4.3.0.");
        }
    }


    /**
    * Parse send command response text
    * @access private
    */
    function _parse_send ($result) {
    	if (substr($result,-4)!=",200") {
    	    // die ("Error sending SMS! ($result)");
	    $code = $result;
    	} else {
    	    $code = $result;
    	}
        return $result;
    }

    /**
    * Check for CURL PHP module
    * @access private
    */
    function _chk_curl() {
        if (!extension_loaded('curl')) {
            die ("This SMS API class can not work without CURL PHP module! Try using fopen sending method.");
        }
    }
}

?>