<?php
/**
 * Usage Instructions
 *
 * Step 1: Make sure you have set your UserID($user) and Password($pass) in the sms_api.php file.
 * Step 2: Fill in or Replace the line $APIresponse =  $mysms->send ("DESTINATION", "SenderID", "MESSAGE", "TYPE", "SMSGATEWAY")
 *         with the necessary fields: DESTINATION, SenderID, MESSAGE, TYPE, Gateway
 *
 * DESTINATION = Mobile Number to send to (Can support up to 30 number separated by comma ",")
 * SenderID = Name that you want to appear on receiver's phone (Limited to 11 Alphanumeric Characters)
 * MESSAGE = Message that you want to send to receiver
 * TYPE = 0 for Normal Text, 5 for Unicode
 * SMSGATEWAY = normal, dipping (depending on which credit type you wish to use). Failure to set the correct credit type might result in 402 error
 *
 * Use $APIresponse; to execute the sending of the SMS
 * OR echo $APIresponse; to see the response from the Server
 */

require_once ("sms_send_include.php");
$mysms = new sms();
echo $mysms->session;
//$APIresponse =  $mysms->send ("DESTINATION", "SenderID", "MESSAGE", "TYPE", "SMSGATEWAY")
$APIresponse = $mysms->send ("60121234567", "WebSMS2u", "Hi this is a test message.", "0", "dipping");

//The line below will execute the sending of the SMS Above SMS without echo output
//$APIresponse;

// The line below will execute the sending of the SMS with echo output
echo $APIresponse;
?>