﻿<?php
/**
 * WebSMS2u SMS CHECKSTATUS INCLUDE
 *
 * This class is meant to check the status of SMS messages via the WebSMS2u gateway
 * This class use the fopen or CURL module to communicate with the gateway via HTTP/S.
 *
 * For more information about WebSMS2u service visit http://www.websms2u.com
 *
 * @version 1.0 - 11 Sept 2013
 * @package sms_api
 * @copyright Copyright XGEN Technologies Sdn Bhd
 *
 */

/**
 * Usage Instructions
 *
 * Step 1: Configure the following options in the CLASS SMS below:
 *
 *         Mandatory Variables to set:
 *         $user = Your WebSMS2u.com UserID
 *         $pass = Your WebSMS2u.com Password
 *
 *         Optional Variables to set:
 *         $sending_method = Method to use to perform transmission. Enter either 'curl' or 'fopen'.
 *         $curl_use_proxy = Set to False or True depending on whether you wish to use PROXY under curl method
 *         $curl_proxy = Set the proxy address to use
 * 
 * Step 2: Call the class funtion from the included index.php or from your script which should include the following example codes:
 *
 *         Example Code below:
 *	   Make sure you fill in or replace the DESTINATION, SenderID, MESSAGE, TYPE, SMSGATEWAY variables
 *
 *         <?php
 *         require_once ("sms_checkstatus_include.php");
 *         $mysms = new sms();
 *         echo $mysms->session;
 *         $APIresponse = $mysms->send ("ACTION", "PHONENO", "SMSID");
 *         $APIresponse;
 *         ?>
 *
 * Step 3: Upload the files and run it from the browser. You should be able to receive an SMS from our system after that.
 */


class sms {

    /**
    * WebSMS2u username
    * @var mixed
    */
    var $user = "YOUR_WebSMS2u_USERNAME";

    /**
    * WebSMS2u password
    * @var mixed
    */
    var $pass = "YOUR_WebSMS2u_PASSWORD";

    /**
    * Gateway command sending method (curl,fopen)
    * @var mixed
    */
    var $sending_method = "fopen";

    /**
    * Optional CURL Proxy
    * @var bool
    */
    var $curl_use_proxy = false;

    /**
    * Proxy URL and PORT
    * @var mixed
    */
    var $curl_proxy = "http://127.0.0.1:8080";

    /**
    * Proxy username and password
    * @var mixed
    */
    var $curl_proxyuserpwd = "login:secretpass";


    /**
    * Session variable
    * @var mixed
    */
    var $session;

    /**
    * Class constructor
    * Create SMS object and authenticate SMS gateway
    * @return object New SMS object.
    * @access public
    */

    function setUser($u){
        $this->user = $u;
    }

    function setPass($p){
        $this->pass = $p;
    }
    
    function sms () {
        $this->base   = "http://www.websms2u.com/sms/intapi.asp";
    }

    function send($action=null, $phoneno=null, $smsid=null) {

    	/* Check $action, $phoneno, $smsid not empty */
        if (empty ($action)) {
    	    die ("You did not specify an action (ACTION)!");
    	}
        if (empty ($phoneno)) {
    	    die ("You did not specify a Phone Number (PHONENO)!");
    	}
        if (empty ($smsid)) {
    	    die ("You did not specify the SMSID (SMSID)!");
    	}

    	/* Submit Now */
    	$comm = sprintf ("%s?user=%s&pass=%s&action=%s&phoneno=%s&smsid=%s",
            $this->base,
	    $this->user,
	    $this->pass,
            rawurlencode($action),
            rawurlencode($phoneno),
            rawurlencode($smsid),
            $this->session
        );
        return $this->_parse_send ($this->_execgw($comm));
echo $comm;
    }

    /**
    * Execute gateway commands
    * @access private
    */
    function _execgw($command) {
        if ($this->sending_method == "curl")
            return $this->_curl($command);
        if ($this->sending_method == "fopen")
            return $this->_fopen($command);
        die ("Unsupported sending method!");
    }

    /**
    * CURL sending method
    * @access private
    */
    function _curl($command) {
        $this->_chk_curl();
        $ch = curl_init ($command);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER,0);
        if ($this->curl_use_proxy) {
            curl_setopt ($ch, CURLOPT_PROXY, $this->curl_proxy);
            curl_setopt ($ch, CURLOPT_PROXYUSERPWD, $this->curl_proxyuserpwd);
        }
        $result=curl_exec ($ch);
        curl_close ($ch);
        return $result;
    }

    /**
    * fopen sending method
    * @access private
    */
    function _fopen($command) {
        $result = '';
        $handle = @fopen($command, "r");
        if ($handle) {
            while ($line = @fgets($handle,1024)) {
                $result .= $line;
            }
            fclose ($handle);
            return $result;
        } else {
            die ("Error while executing fopen sending method!<br>Please check does PHP have OpenSSL support and check does PHP version is greater than 4.3.0.");
        }
    }


    /**
    * Parse send command response text
    * @access private
    */
    function _parse_send ($result) {
        $code = $result;
        return $result;
    }

    /**
    * Check for CURL PHP module
    * @access private
    */
    function _chk_curl() {
        if (!extension_loaded('curl')) {
            die ("This SMS API class can not work without CURL PHP module! Try using fopen sending method.");
        }
    }
}

?>