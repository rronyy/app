<?php
/**
 * Usage Instructions
 *
 * Step 1: Make sure you have set your UserID($user) and Password($pass) in the sms_checkstatus_api.php file.
 * Step 2: Fill in or Replace the line $APIresponse = $mysms->send ("ACTION", "PHONENO", "SMSID")
 *         with the necessary fields: ACTION, PHONENO, SMSID
 *
 * ACTION = "checkstatus"
 * PHONENO = The Recipient Mobile Number which you wish to check delivery status on
 * SMSID = The SMSID of the Message which you wish to check delivery status on
 *
 * Use $APIresponse; to execute the sending of the SMS
 * OR echo $APIresponse; to see the response from the Server
 */

require_once ("sms_checkstatus_include.php");
$mysms = new sms();
echo $mysms->session;
//$APIresponse = $mysms->send ("ACTION", "PHONENO", "SMSID")
$APIresponse = $mysms->send ("checkstatus", "60121234567", "123456");

//The line below will execute the sending of the SMS Above SMS without echo output
//$APIresponse;

// The line below will execute the sending of the SMS with echo output
echo $APIresponse;
?>