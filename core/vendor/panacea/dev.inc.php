<?php
if($controller && isset($function)){
	if($controller && !exists("sys_privilege", "link='$controller' AND `option`='$function'")){
		$glyphicon = "glyphicon glyphicon-question-sign";
		if($function=='view'){
			$glyphicon = "glyphicon glyphicon-list";
		} elseif($function=='edit'){
			$glyphicon = "glyphicon glyphicon-edit";
		} elseif($function=='remove'){
			$glyphicon = "glyphicon glyphicon-remove";
		} elseif($function=='add'){
			$glyphicon = "glyphicon glyphicon-plus-sign";
		} elseif($function=='print'){
			$glyphicon = "glyphicon glyphicon-print";
		} elseif($function=='activate'){
			$glyphicon = "glyphicon glyphicon-eye-open";
		} elseif($function=='deactivate'){
			$glyphicon = "glyphicon glyphicon-eye-close";
		}
		$root = 2;
		$find_root = R::findOne("sys_privilege", "link=?", array($controller));
		if($find_root){
			$root = $find_root->root;
		}
		$id =insert("sys_privilege", (isset($module)?"module,":"")."`name`,`link`,`title`,`root`,`option`,hidden, glyphicon", 
			 (isset($module)?"'$module',":"")."'".title("$function $controller")."','$controller','".title("$function $controller")."',$root,'$function',".
			(in_array($function, array('view'))?"0":"1").",'$glyphicon'");
		insert("sys_site_privilege", "sp_site, sp_privilege", "1,$id");
	}
}