<?php
class IO
{
	var $dir = "";
	
	function __construct($dir)
	{
		$this->dir = $dir;
	}

	function firstDir(){
		if ($dh = opendir($this->dir)) {
	        while (($file = readdir($dh)) !== false) {
	           	if(in_array($file, array(".", ".."))){
	           		continue;
	           	} else{
	           		if(is_dir($this->dir."/".$file)){
	        			closedir($dh);
	           			return $file;
	           		}
	           	}
	        }
	        closedir($dh);
	    }
	}

	function exists($path){
		return file_exists($this->dir."/".$path);
	}
}