<?php

class DB
{
	var $dbname;
	function __construct($dbname)
	{
		$this->dbname = $dbname;
	}

	function tables(){
	    global $database;
		$arr_field = array();
		$tables=select("table_name",  "information_schema.tables", "table_schema = '".$database."' AND table_name NOT LIKE 'sys_%'");
		while($table=mysqli_fetch_object($tables)){	
			array_push($arr_field,$table->table_name);
		}
		return $arr_field;
	}

	function getTrashField($table){	
		$fields=select("SHOW FULL COLUMNS FROM ".$table." WHERE FIELD LIKE '%trash'");
		if($fields->num_rows>0){
			$field = mysqli_fetch_object($fields);
			return $field->Field;	
		}
		return false;
	}
	function getActiveField($table){
		$fields=select("SHOW FULL COLUMNS FROM ".$table." WHERE FIELD LIKE '%active'");
		if($fields->num_rows>0){
			$field = mysqli_fetch_object($fields);
			return $field->Field;	
		}
		return false;
	}
	function getForeignKeys($table){
	    global $database;
		$fields = select("SELECT * FROM information_schema.`KEY_COLUMN_USAGE` WHERE `CONSTRAINT_SCHEMA`='".$database."' AND `TABLE_NAME`='".$table."'AND `CONSTRAINT_NAME` NOT IN('PRIMARY', 'UNIQUE')");
		if($fields->num_rows>0){
			$field = mysqli_fetch_object($fields);
			return $field->Field;	
		}
		return false;
	}
}