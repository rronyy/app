function jsload(){
	if(typeof $.validate !== "undefined"){
		$.each($("form"), function(i, val) {
			$(this).validate();
	  });
	}
  if (typeof $.DataTable !== "undefined") { 
  	//$('.table-sortable').DataTable();
  }
  //$('.selectpicker').selectpicker();
  
  if (typeof $.qtip !== "undefined") { 
  	// $('[title!=""]').qtip();
  }
  // if (typeof $.pikaday !== "undefined") { 
  	$('.datepicker').pikaday({ firstDay: 1 }).addClass("has-datepicker");
  // }
  // $('.datepicker').pikaday({ firstDay: 1, format: 'D-MM-YYYY' }).addClass("has-datepicker");

  $("#msg-text").hide('pulsate', {}, 750);
  var msg = setInterval("hidemsg()", 7500);
  /*$(".date").datepicker({
	  		dateFormat: "yy-mm-dd",
			minDate: '-50y',
	  		showButtonPanel: true,
			changeMonth: true,
			changeYear: true,
			showAnim: "clip",
			numberOfMonths: 1
	});
*/
}

$(".duplicate").click();
function duplicate(el){
	$(el).parent().parent().parent().append('<tr>' + $(el).parent().parent().html() + '</tr>');
	console.log($(el).parent().parent());
}
function hidemsg(){
	if(exists("#msg")){
		if($("#msg").css("display").toString()!="none"){
			$("#msg").fadeOut("slow");
			clearInterval(msg);
		}
	}
}
$(function(){
  resize();
  jsload();
  $(window).resize(function () {resize(); });
  //******************
  	if(exists("#tabs")) {
		var cookieName = $.getQueryParam("q");
		$("#tabs").tabs({cookie: {expires: 1 }});
	};
});
function cookie(name, value){
	if(typeof(value)=='undefined'){
		return $.cookie(name);
	} else{
		return $.cookie(name,value);
	}
}
function setcookie(name,value){
	return $.cookie(name,value);
}
function zerofill(num, digit){
	num = num.toString();
	for(var i=digit; i>num.length; i--){
		num = "0" + num;
	}
	return num;
}
function resize(){
  $("#login_form").css({"top": (($(window).height()/2 - $("#login_form").height()/2) - 50) + "px", "left": ($(window).width()/2 - $("#login_form").width()/2) + "px"});
}
function jprint(area){
	console.log(area);
	if(typeof(area)!='undefined'){
		console.log(area);
		$('#'+area).jqprint();
	} else{
		if(exists("#printarea")){
			$('#printarea').jqprint();
		} else{
			$('#print-area').jqprint();
		}
	}
}

function pdf(){
	var data = "";
	if(typeof(area)!='undefined'){
		data = $('#'+area).html();
	} else{
		if(exists(".exportable")){
			data = $('.exportable').html();
		} else if(exists("#print-area")){
			data = $('#print-area').html();
		} else{
			data = $('#content').html();
		}
	}
	$(data).find(".nfp").remove();
	$(data).find(".dp").remove();
	$(".exportable").after("<form id='export' method='post'><input type='hidden' id='data' name='data' /></form>");
	$("#data").val(data);
	$("#export").attr("action", "../core/vendor/dompdf/export.php");
	$("#export").submit();
	$("#export").remove();
}

function email(){
	$("#maincontainer").before("<div class='overlay' align='center' style='position:absolute;left:0;right:0;top:0;bottom:0;z-index:999;background:rgba(30,30,30,.8);';margin:50px;><img src='images/loader.gif' /> Sending Email, Please Wait....</div>");
	var data = "";
	if(typeof(area)!='undefined'){
		data = $('#'+area).html();
	} else{
		if(exists(".exportable")){
			oridata = $('.exportable').html();
			data = $('.exportable').html();
			$('.exportable').html(oridata);
		} else if(exists("#print-area")){
			data = $('#print-area').html();
		} else{
			data = $('#content').html();
		}
	}
	$(data).find(".nfp").remove();
	$(data).find(".dp").remove();
	$("#data").val(data);
	$.post("lib/email.php", {'email': prompt("Please key in Email Address"), 'data': data}, function(data){
		$(".overlay").remove();
	});

}

function excel(){
	var data = "";
	if(typeof(area)!='undefined'){
		data = $('#'+area).html();
	} else{
		if(exists(".exportable")){
			data = $('.exportable').html();
		} else if(exists("#print-area")){
			data = $('#print-area').html();
		} else{
			data = $('#content').html();
		}
	}
	$(data).find(".nfp").remove();
	$(data).find(".dp").remove();
	$("#data").val(data);
	$("#export").attr("action", "lib/excel.php");
	$("#export").submit();

}

function exists(elem){
	if($(document).find(elem).length>0){
		return true;
	} else{
		return false;
	}
}
function existsin(container, elem){
	if($(container).find(elem).length>0){
		return true;
	} else{
		return false;
	}
}
function tf(val) {
	temp = ifNan(val);
	return parseFloat(temp).toFixed(2);
}
function ifNull(val){
  if(isNaN(val)){
    return 0;
  }
  return val;
}
function ifNan(val){
	v = parseFloat(val);
  if(isNaN(v) || v == 'Infinity'){
    return 0;
  }
  return v;
}
function redir(url){
	location.href = url;
}
function countryChanged(country_field, state_field){
	typeof(state_field)=='undefined'?state_field='state':'';
    $.post("ajax/loadState.php", {cid: $('#'+country_field).val()},
        function(data){
         $("#"+state_field).html(data);
        });
}

function selectByClass(cl){
  $("." + cl).prop("checked", true);
}

function unselectByClass(cl){
  $("." + cl).prop("checked", false);
}

function conf(msg, url, frm, f, v){
	if(confirm(msg)){
		if(typeof(url)!='undefined' && typeof(frm)=='undefined'){
			location.href = url;
		} else if(typeof(frm)!='undefined'){
			if(typeof(f)!='undefined'){
				if(typeof(v)!='undefined'){
					$('#' + f).val(v);
				} else{
					return false;
				}
			}
			$('#' + frm).submit();
		}
		return true;
	} else{
		return false;
	}
}
function del(id, c){
	if(typeof(c)=='undefined'){ $("#"+id).remove(); }
	else { $("."+id).remove(); }
}
function mysqldate(d){
	return d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
}
function fsub(frm, f, v){
	if(typeof(frm)!='undefined'){
		if(typeof(f)!='undefined'){
			if(typeof(v)!='undefined'){
				$('#' + f).val(v);
			} else{
				return false;
			}
		}
		$('#' + frm).submit();
	}
	return false;
}
function count(ob, msg){
	alert(ob);
	var c = $(ob).length;alert(c);
	if(typeof(msg)!='undefined' && c<1){alert(c);}
	return c>0?c:false;
}
function get2(q) {
	q = q.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var s = "[\\?&]" + q + "=([^&#]*)";
	var x = new RegExp(s);
	var r = x.exec(window.location.search);
	if(r == null){ return ""; }
	else { return decodeURIComponent(r[1].replace(/\+/g, " ")); }
}
function get(q) {
  q = q.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var s = "[\\?&]" + q + "=([^&#]*)";
  var x = new RegExp(s);
  var r = x.exec(window.location.search);
  if(r == null){ return ""; }
  else {return decodeURIComponent(r[1].replace(/\+/g, " "));}
}

jQuery.fn.filterByText = function(textbox, selectSingleMatch) {
  return this.each(function() {
    var select = this;
    var options = [];
    $(select).find('option').each(function() {
      options.push({value: $(this).val(), text: $(this).text()});
    });
    $(select).data('options', options);
    $(textbox).bind('change keyup', function() {
      var options = $(select).empty().data('options');
      var search = $.trim($(this).val());
      var regex = new RegExp(search,'gi');

      $.each(options, function(i) {
        var option = options[i];
        if(option.text.match(regex) !== null) {
          $(select).append(
             $('<option>').text(option.text).val(option.value)
          );
        }
      });
      if (selectSingleMatch === true &&
          $(select).children().length === 1) {
        $(select).children().get(0).selected = true;
      }
    });
  });
};
function equalize(selector){
	var greatestWidth = 0;
	$(selector).each(function() {
		var theWidth = $(this).width();
		if( theWidth > greatestWidth) {
			greatestWidth = theWidth;
		}
	});
	$(selector).width(greatestWidth + 10);
}

function cl(msg){
	console.log(msg);
}

if(jQuery) (function(){

	$.fn.hasAttr = function(name) {  
		return this.attr(name) !== undefined;
	};

	$.extend($.fn, {

		rightClick: function(handler) {
			$(this).each( function() {
				$(this).mousedown( function(e) {
					var evt = e;
					$(this).mouseup( function() {
						$(this).unbind('mouseup');
						if( evt.button == 2 ) {
							handler.call( $(this), evt );
							return false;
						} else {
							return true;
						}
					});
				});
				$(this)[0].oncontextmenu = function() {
					return false;
				}
			});
			return $(this);
		},

		rightMouseDown: function(handler) {
			$(this).each( function() {
				$(this).mousedown( function(e) {
					if( e.button == 2 ) {
						handler.call( $(this), e );
						return false;
					} else {
						return true;
					}
				});
				$(this)[0].oncontextmenu = function() {
					return false;
				}
			});
			return $(this);
		},

		rightMouseUp: function(handler) {
			$(this).each( function() {
				$(this).mouseup( function(e) {
					if( e.button == 2 ) {
						handler.call( $(this), e );
						return false;
					} else {
						return true;
					}
				});
				$(this)[0].oncontextmenu = function() {
					return false;
				}
			});
			return $(this);
		},

		noContext: function() {
			$(this).each( function() {
				$(this)[0].oncontextmenu = function() {
					return false;
				}
			});
			return $(this);
		}

	});

})(jQuery);
