
<?php
//Returns
function div($text, $attr = []){
	return "<div".extractAttr($attr).">$text</div>";
}
function table(){
	return "<table>";
}
function tr($attr = []){ //Wrapper
	return row($attr);
}
function row($attr = []){
	return openTag("tr", $attr);
}
function td($text, $attr = []){ //Wrapper
	return cell($text, $attr);
}
function cell($text, $attr = []){
	return openTag("td", $attr);
}
function openTag($tag, $attr = []){
	return "<$tag".extractAttr($attr).">";
}
function closeTag($tag){
	return "</$tag>";
}
//Prints
function divP($text, $attr = []){
	print div($text, $attr = []);
}
function openTagP($tag, $attr = []){
	print openTag($tag);
}
function closeTagP($tag){
	print closeTag($tag);
}
function tdP($text, $attr = []){ //Wrapper
	print td($text, $attr);
}
function cellP($text, $attr = []){ //Wrapper
	print cell("td", $attr);
}
function trP($attr = []){ //Wrapper
	print row($attr);
}
function rowP($attr = []){ //Wrapper
	print row($attr);
}

//Common
function extractAttr($attr = []){
	$attr_extracted = "";
	foreach ($attr as $key => $value) {
		$attr_extracted .= " $key='$value'";
	}
	return $attr_extracted.addCommonAttr();
}

function addCommonAttr(){
	$lang = isset($_SESSION[APP.'_lang'])?$_SESSION[APP.'_lang']:'en';
	$dir = $lang=='ar'?'rtl':'ltr';
	return "lang='$lang' dir='$dir'";
}