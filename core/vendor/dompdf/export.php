<?php
require_once 'autoload.inc.php';
ini_set('memory_limit', '999M');
ini_set('max_execution_time', '999');
$html = stripslashes($_POST['data']);
$filename = isset($_GET['filename'])?$_GET['filename']:'report.pdf';
$paper = isset($_GET['p'])?$_GET['p']:'A4';
$orientation = isset($_GET['o'])?$_GET['o']:'portrait';
//print $html;
// reference the Dompdf namespace
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', $orientation);

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();