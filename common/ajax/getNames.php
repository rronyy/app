<?php
require_once("../../config/gcf.php"); require_once("../../system/f.php");
if(isset($_POST['type'])){
	$class = isset($_POST['required'])?"required":"";
  switch (strtolower($_POST['type'])){
	case 'customer': {
	  print selectOption("name='type_id' id='type_id' class='$class'", "sales_customer", "c_name", "id", (isset($_POST['userid'])?$_POST['userid']:''),'','',true);
	} break;
	case 'supplier': {
	  print selectOption("name='type_id' id='type_id' class='$class'", "purchase_supplier", "s_name", "id",( isset($_POST['userid'])?$_POST['userid']:''),'','',true);
	} break;
	case 'service_provider': {
	  print selectOption("name='type_id' id='type_id' class='$class'", "service_provider", "h_name", "id", (isset($_POST['userid'])?$_POST['userid']:''),'','',true);
	} break;
	case 'service': {
	  print selectOption("name='type_id' id='type_id' class='$class'", "service_provider_services", "sps_name", "id", (isset($_POST['userid'])?$_POST['userid']:''),'','',true);
	} break;
	case 'agent': {
	  print selectOption("name='type_id' id='type_id' class='$class'", "sales_agent", "a_name", "id", (isset($_POST['userid'])?$_POST['userid']:''),'','',true);
	} break;
	default:{
		print selectOption("name='type_id' id='type_id' class='$class'", "employee_details", "pd_fname", "id", (isset($_POST['userid'])?$_POST['userid']:''),'','',true);
	} break;
  }
}