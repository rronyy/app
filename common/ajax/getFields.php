<?php
if(isset($_POST['schema']) && isset($_POST['type'])){    
    if(!defined("APP")){
        define("APP", $_POST["app"]);
    }
    if(strpos($_SERVER["SCRIPT_FILENAME"], "framework") !== FALSE){
        $base = substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "framework"));
    } if(strpos($_SERVER["SCRIPT_FILENAME"], APP) !== FALSE){
        $base = substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], APP));
    } else{
        $base = substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "framework"));
    }
    $base = "$base".APP;
    require_once "$base/safeboot.php";
    $schema = $_POST['schema'];
    $type = $_POST['type'];
    $fields = tableFieldsRaw($schema);
    $i = 1;
    if($type=='form'){
        print "<table>
            <tr><td>Field</td><td>Span</td><td>Type</td><td>Helper</td><td>Group</td><td>Group Name</td><td>Position</td></tr>";
        while($field=mysqli_fetch_object($fields)){
            $checked = "checked";
            if($field->Field == 'id' || strpos($field->Field, 'entry_by')!==FALSE || strpos($field->Field, 'modify_by')!==FALSE || strpos($field->Field, 'entry_time')!==FALSE || strpos($field->Field, 'modify_time')!==FALSE || contains($field->Field, 'trash')) {
			    $checked = "";
		    }
            $name = $field->Field;
            print "<tr>
                    <td><input type='checkbox' value='$i' name='form_fields[]' $checked class='form-control' />
                        <input type='hidden' name='field_$i' value='$field->Field' /></td><td>$field->Field</td>
                    <td><input type='number' name='span_$i' size='2' value='1' class='w30 form-control-fluid' /></td>
                    <td>".selectEnum("name='type_$i' class='form-control-fluid'", "frm_attributes", "att_type", 'Text')."</td>
                    <td><input type='text' name='helper_$i' size='2' value='' class='w50 form-control-fluid' /></td>
                    <td><input type='number' name='group_$i' size='2' value='1' class='w60 form-control-fluid' /></td>
                    <td><input type='text' name='group_name_$i' size='2' value='1' class='w50 form-control-fluid' /></td>
                    <td><input type='number' name='position_$i' size='2' value='$i' class='w60 form-control-fluid' /></td>
                </tr>";
            $i++;
        }
        print "</table>";
    }
    $i = 1;
    if($type=='filter'){
        print "<select name='filter_fields[]' size='15' class='form-control-fluid h300' multiple>";
        while($field=mysqli_fetch_object($fields)){
            $selected = "";
            if(strpos($field->Field, 'name')!==FALSE || strpos($field->Field, 'date')!==FALSE) {
			    $selected = "selected='selected'";
		    }
            print "<option $selected value='$field->Field'>$field->Field</option>";
            $i++;
        }
        print "</select>";
    }
    $i = 1;
    if($type=='view'){
        print "<select name='view_fields[]' class='form-control-fluid h300' size='15' multiple>";
        while($field=mysqli_fetch_object($fields)){
            $selected = "selected='selected'";
            if($field->Field == 'id' || contains($field->Field, 'trash')) {
			    $selected = "";
		    }
            print "<option $selected value='$field->Field'>$field->Field</option>";
            $i++;
        }
        print "</select>";
    }
}
?>
