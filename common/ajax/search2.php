<?php
	if(isset($_POST['key'])){
		if(!defined("APP")){
        define("APP", $_POST["app"]);
    }
    if(strpos($_SERVER["SCRIPT_FILENAME"], "framework") !== FALSE){
        $base = substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "framework"));
    } if(strpos($_SERVER["SCRIPT_FILENAME"], APP) !== FALSE){
        $base = substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], APP));
    } else{
        $base = substr($_SERVER["SCRIPT_FILENAME"], 0, strpos($_SERVER["SCRIPT_FILENAME"], "framework"));
    }
    $base = "$base".APP;
    require_once "$base/safeboot.php";
		extract($_POST);
		$os = select("*", "sys_search_options", "`object`='booking'");
		$data = "";
		while($o = mysqli_fetch_object($os)){
			$schema = $o->schema;
			$object = $o->object;
			$display = explode(",", $o->display);
			$keys = explode(",", $o->keys);
			$functions = explode(",", $o->functions);
			$filter = "";
			$count = 1;
			foreach($keys as $k){
				$k = trim($k);
				$filter .= "id = '{$_POST['key']}'";
				if($count++<count($keys)) $filter .= " OR ";
			}
			$sr = select("*", "$o->schema", "$filter");
			while($s = mysqli_fetch_object($sr)){
				$data .= "<tr><td>";
				//$data .= "<a href='".BASEURL.APP."/$object/".trim($functions[0])."/$s->id'>";

				foreach ($display as $column) {
					if(contains(strtolower($s->$column), ['.png', '.jpg', '.jpeg'])){
						//if(file_exists("uploads/$object/$s->id/".$s->$column)){
							$data .= "<a href='$appurl/uploads/$object/$s->id/".$s->$column."' target='_blank'><img src='".$appurl."/uploads/$object/$s->id/".$s->$column."' height='50px'/></a>";
						//}
					} else{
						$data .= "<a href='$appurl/$object/".trim($functions[0])."/$s->id'>".title($column).": ".$s->$column.space(3)."";	
					}					
				}
				
				$data .= " ($o->note)</a><br />";
				$i = 0;
				foreach($functions as $f){
					$f = trim($f);
					//if($i++>1) echo "<tr>";
					$icon = 'fa fa-list';
					switch ($f) {
						case 'edit':
							$icon = 'fa fa-edit';
							break;
						case 'print':
							$icon = 'fa fa-print';
							break;
						case 'view':
							$icon = 'fa fa-search';
							break;
					}
					$data .= "<a href='$appurl/$object/{$f}/$s->id'><i class='$icon'></i> $f</a> &nbsp;&nbsp;";
				}	
				$data .= "</td></tr>";
			}			
		}
		if($data!="")
		echo "<table align='center' width='100%' class='result'>$data</table><hr>";
	}
?>