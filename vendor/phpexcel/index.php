<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;


// $spreadsheet = new Spreadsheet();
// $sheet = $spreadsheet->getActiveSheet();
// $sheet->setCellValue('A1', 'Hello World !');

// $writer = new Xlsx($spreadsheet);
// $writer->save('hello world.xlsx');

$inputFileName = __DIR__ .'\test.xlsx';

$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load($inputFileName);
	// print_r($spreadsheet);

$row = 1;
$col = 65;
$break = false;
print "<table>";
for($row = 1; $row <= 10000; $row++){
	print "<tr>";
	for($col = 65; $col <= 67; $col++){
		print "<td>".$spreadsheet->getActiveSheet()->getCell(chr($col)."$row")->getValue()."</td>";		
	}	
	print "</tr>";
}
print "</table>";